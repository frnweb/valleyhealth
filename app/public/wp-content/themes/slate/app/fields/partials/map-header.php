<?php

namespace App;

use StoutLogic\AcfBuilder\FieldsBuilder;

$mapheader = new FieldsBuilder('map_header', ['position' => 'side']);

$mapheader
  ->setLocation('post_type', '==', 'page');
  
$mapheader
  //Map		
	->addTrueFalse('map_header', [
		'label' => 'Add Map in Header'
		])
		->setInstructions('Checking this will replace the page header image with a map');

return $mapheader;