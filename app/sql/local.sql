-- MySQL dump 10.13  Distrib 8.0.16, for macos10.14 (x86_64)
--
-- Host: localhost    Database: local
-- ------------------------------------------------------
-- Server version	8.0.16

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
 SET NAMES utf8mb4 ;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `wp_commentmeta`
--

DROP TABLE IF EXISTS `wp_commentmeta`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
 SET character_set_client = utf8mb4 ;
CREATE TABLE `wp_commentmeta` (
  `meta_id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `comment_id` bigint(20) unsigned NOT NULL DEFAULT '0',
  `meta_key` varchar(255) COLLATE utf8mb4_unicode_520_ci DEFAULT NULL,
  `meta_value` longtext COLLATE utf8mb4_unicode_520_ci,
  PRIMARY KEY (`meta_id`),
  KEY `comment_id` (`comment_id`),
  KEY `meta_key` (`meta_key`(191))
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_520_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `wp_commentmeta`
--

LOCK TABLES `wp_commentmeta` WRITE;
/*!40000 ALTER TABLE `wp_commentmeta` DISABLE KEYS */;
/*!40000 ALTER TABLE `wp_commentmeta` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `wp_comments`
--

DROP TABLE IF EXISTS `wp_comments`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
 SET character_set_client = utf8mb4 ;
CREATE TABLE `wp_comments` (
  `comment_ID` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `comment_post_ID` bigint(20) unsigned NOT NULL DEFAULT '0',
  `comment_author` tinytext COLLATE utf8mb4_unicode_520_ci NOT NULL,
  `comment_author_email` varchar(100) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `comment_author_url` varchar(200) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `comment_author_IP` varchar(100) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `comment_date` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `comment_date_gmt` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `comment_content` text COLLATE utf8mb4_unicode_520_ci NOT NULL,
  `comment_karma` int(11) NOT NULL DEFAULT '0',
  `comment_approved` varchar(20) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '1',
  `comment_agent` varchar(255) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `comment_type` varchar(20) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT 'comment',
  `comment_parent` bigint(20) unsigned NOT NULL DEFAULT '0',
  `user_id` bigint(20) unsigned NOT NULL DEFAULT '0',
  PRIMARY KEY (`comment_ID`),
  KEY `comment_post_ID` (`comment_post_ID`),
  KEY `comment_approved_date_gmt` (`comment_approved`,`comment_date_gmt`),
  KEY `comment_date_gmt` (`comment_date_gmt`),
  KEY `comment_parent` (`comment_parent`),
  KEY `comment_author_email` (`comment_author_email`(10))
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_520_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `wp_comments`
--

LOCK TABLES `wp_comments` WRITE;
/*!40000 ALTER TABLE `wp_comments` DISABLE KEYS */;
INSERT INTO `wp_comments` VALUES (1,1,'A WordPress Commenter','wapuu@wordpress.example','https://wordpress.org/','','2021-04-19 17:39:35','2021-04-19 17:39:35','Hi, this is a comment.\nTo get started with moderating, editing, and deleting comments, please visit the Comments screen in the dashboard.\nCommenter avatars come from <a href=\"https://gravatar.com\">Gravatar</a>.',0,'1','','comment',0,0);
/*!40000 ALTER TABLE `wp_comments` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `wp_gf_draft_submissions`
--

DROP TABLE IF EXISTS `wp_gf_draft_submissions`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
 SET character_set_client = utf8mb4 ;
CREATE TABLE `wp_gf_draft_submissions` (
  `uuid` char(32) COLLATE utf8mb4_unicode_520_ci NOT NULL,
  `email` varchar(255) COLLATE utf8mb4_unicode_520_ci DEFAULT NULL,
  `form_id` mediumint(8) unsigned NOT NULL,
  `date_created` datetime NOT NULL,
  `ip` varchar(39) COLLATE utf8mb4_unicode_520_ci NOT NULL,
  `source_url` longtext COLLATE utf8mb4_unicode_520_ci NOT NULL,
  `submission` longtext COLLATE utf8mb4_unicode_520_ci NOT NULL,
  PRIMARY KEY (`uuid`),
  KEY `form_id` (`form_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_520_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `wp_gf_draft_submissions`
--

LOCK TABLES `wp_gf_draft_submissions` WRITE;
/*!40000 ALTER TABLE `wp_gf_draft_submissions` DISABLE KEYS */;
/*!40000 ALTER TABLE `wp_gf_draft_submissions` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `wp_gf_entry`
--

DROP TABLE IF EXISTS `wp_gf_entry`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
 SET character_set_client = utf8mb4 ;
CREATE TABLE `wp_gf_entry` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `form_id` mediumint(8) unsigned NOT NULL,
  `post_id` bigint(20) unsigned DEFAULT NULL,
  `date_created` datetime NOT NULL,
  `date_updated` datetime DEFAULT NULL,
  `is_starred` tinyint(1) NOT NULL DEFAULT '0',
  `is_read` tinyint(1) NOT NULL DEFAULT '0',
  `ip` varchar(39) COLLATE utf8mb4_unicode_520_ci NOT NULL,
  `source_url` varchar(200) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `user_agent` varchar(250) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `currency` varchar(5) COLLATE utf8mb4_unicode_520_ci DEFAULT NULL,
  `payment_status` varchar(15) COLLATE utf8mb4_unicode_520_ci DEFAULT NULL,
  `payment_date` datetime DEFAULT NULL,
  `payment_amount` decimal(19,2) DEFAULT NULL,
  `payment_method` varchar(30) COLLATE utf8mb4_unicode_520_ci DEFAULT NULL,
  `transaction_id` varchar(50) COLLATE utf8mb4_unicode_520_ci DEFAULT NULL,
  `is_fulfilled` tinyint(1) DEFAULT NULL,
  `created_by` bigint(20) unsigned DEFAULT NULL,
  `transaction_type` tinyint(1) DEFAULT NULL,
  `status` varchar(20) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT 'active',
  PRIMARY KEY (`id`),
  KEY `form_id` (`form_id`),
  KEY `form_id_status` (`form_id`,`status`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_520_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `wp_gf_entry`
--

LOCK TABLES `wp_gf_entry` WRITE;
/*!40000 ALTER TABLE `wp_gf_entry` DISABLE KEYS */;
/*!40000 ALTER TABLE `wp_gf_entry` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `wp_gf_entry_meta`
--

DROP TABLE IF EXISTS `wp_gf_entry_meta`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
 SET character_set_client = utf8mb4 ;
CREATE TABLE `wp_gf_entry_meta` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `form_id` mediumint(8) unsigned NOT NULL DEFAULT '0',
  `entry_id` bigint(20) unsigned NOT NULL,
  `meta_key` varchar(255) COLLATE utf8mb4_unicode_520_ci DEFAULT NULL,
  `meta_value` longtext COLLATE utf8mb4_unicode_520_ci,
  `item_index` varchar(60) COLLATE utf8mb4_unicode_520_ci DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `meta_key` (`meta_key`(191)),
  KEY `entry_id` (`entry_id`),
  KEY `meta_value` (`meta_value`(191))
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_520_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `wp_gf_entry_meta`
--

LOCK TABLES `wp_gf_entry_meta` WRITE;
/*!40000 ALTER TABLE `wp_gf_entry_meta` DISABLE KEYS */;
/*!40000 ALTER TABLE `wp_gf_entry_meta` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `wp_gf_entry_notes`
--

DROP TABLE IF EXISTS `wp_gf_entry_notes`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
 SET character_set_client = utf8mb4 ;
CREATE TABLE `wp_gf_entry_notes` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `entry_id` int(10) unsigned NOT NULL,
  `user_name` varchar(250) COLLATE utf8mb4_unicode_520_ci DEFAULT NULL,
  `user_id` bigint(20) DEFAULT NULL,
  `date_created` datetime NOT NULL,
  `value` longtext COLLATE utf8mb4_unicode_520_ci,
  `note_type` varchar(50) COLLATE utf8mb4_unicode_520_ci DEFAULT NULL,
  `sub_type` varchar(50) COLLATE utf8mb4_unicode_520_ci DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `entry_id` (`entry_id`),
  KEY `entry_user_key` (`entry_id`,`user_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_520_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `wp_gf_entry_notes`
--

LOCK TABLES `wp_gf_entry_notes` WRITE;
/*!40000 ALTER TABLE `wp_gf_entry_notes` DISABLE KEYS */;
/*!40000 ALTER TABLE `wp_gf_entry_notes` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `wp_gf_form`
--

DROP TABLE IF EXISTS `wp_gf_form`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
 SET character_set_client = utf8mb4 ;
CREATE TABLE `wp_gf_form` (
  `id` mediumint(8) unsigned NOT NULL AUTO_INCREMENT,
  `title` varchar(150) COLLATE utf8mb4_unicode_520_ci NOT NULL,
  `date_created` datetime NOT NULL,
  `date_updated` datetime DEFAULT NULL,
  `is_active` tinyint(1) NOT NULL DEFAULT '1',
  `is_trash` tinyint(1) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_520_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `wp_gf_form`
--

LOCK TABLES `wp_gf_form` WRITE;
/*!40000 ALTER TABLE `wp_gf_form` DISABLE KEYS */;
INSERT INTO `wp_gf_form` VALUES (1,'Contact Us','2021-05-14 18:39:06',NULL,1,0);
/*!40000 ALTER TABLE `wp_gf_form` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `wp_gf_form_meta`
--

DROP TABLE IF EXISTS `wp_gf_form_meta`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
 SET character_set_client = utf8mb4 ;
CREATE TABLE `wp_gf_form_meta` (
  `form_id` mediumint(8) unsigned NOT NULL,
  `display_meta` longtext COLLATE utf8mb4_unicode_520_ci,
  `entries_grid_meta` longtext COLLATE utf8mb4_unicode_520_ci,
  `confirmations` longtext COLLATE utf8mb4_unicode_520_ci,
  `notifications` longtext COLLATE utf8mb4_unicode_520_ci,
  PRIMARY KEY (`form_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_520_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `wp_gf_form_meta`
--

LOCK TABLES `wp_gf_form_meta` WRITE;
/*!40000 ALTER TABLE `wp_gf_form_meta` DISABLE KEYS */;
INSERT INTO `wp_gf_form_meta` VALUES (1,'{\"title\":\"Contact Us\",\"description\":\"\",\"labelPlacement\":\"top_label\",\"descriptionPlacement\":\"below\",\"button\":{\"type\":\"text\",\"text\":\"Submit\",\"imageUrl\":\"\"},\"fields\":[{\"type\":\"text\",\"id\":1,\"label\":\"First Name\",\"adminLabel\":\"\",\"isRequired\":true,\"size\":\"medium\",\"errorMessage\":\"\",\"visibility\":\"visible\",\"inputs\":null,\"formId\":1,\"description\":\"\",\"allowsPrepopulate\":false,\"inputMask\":false,\"inputMaskValue\":\"\",\"inputMaskIsCustom\":false,\"maxLength\":\"\",\"inputType\":\"\",\"labelPlacement\":\"\",\"descriptionPlacement\":\"\",\"subLabelPlacement\":\"\",\"placeholder\":\"\",\"cssClass\":\"\",\"inputName\":\"\",\"noDuplicates\":false,\"defaultValue\":\"\",\"choices\":\"\",\"conditionalLogic\":\"\",\"productField\":\"\",\"enablePasswordInput\":\"\",\"multipleFiles\":false,\"maxFiles\":\"\",\"calculationFormula\":\"\",\"calculationRounding\":\"\",\"enableCalculation\":\"\",\"disableQuantity\":false,\"displayAllCategories\":false,\"useRichTextEditor\":false,\"pageNumber\":1,\"fields\":\"\"},{\"type\":\"text\",\"id\":2,\"label\":\"Last Name\",\"adminLabel\":\"\",\"isRequired\":true,\"size\":\"medium\",\"errorMessage\":\"\",\"visibility\":\"visible\",\"inputs\":null,\"formId\":1,\"description\":\"\",\"allowsPrepopulate\":false,\"inputMask\":false,\"inputMaskValue\":\"\",\"inputMaskIsCustom\":false,\"maxLength\":\"\",\"inputType\":\"\",\"labelPlacement\":\"\",\"descriptionPlacement\":\"\",\"subLabelPlacement\":\"\",\"placeholder\":\"\",\"cssClass\":\"\",\"inputName\":\"\",\"noDuplicates\":false,\"defaultValue\":\"\",\"choices\":\"\",\"conditionalLogic\":\"\",\"productField\":\"\",\"enablePasswordInput\":\"\",\"multipleFiles\":false,\"maxFiles\":\"\",\"calculationFormula\":\"\",\"calculationRounding\":\"\",\"enableCalculation\":\"\",\"disableQuantity\":false,\"displayAllCategories\":false,\"useRichTextEditor\":false,\"pageNumber\":1,\"fields\":\"\"},{\"type\":\"text\",\"id\":3,\"label\":\"Phone Number\",\"adminLabel\":\"\",\"isRequired\":true,\"size\":\"medium\",\"errorMessage\":\"\",\"visibility\":\"visible\",\"inputs\":null,\"formId\":1,\"description\":\"\",\"allowsPrepopulate\":false,\"inputMask\":false,\"inputMaskValue\":\"\",\"inputMaskIsCustom\":false,\"maxLength\":\"\",\"inputType\":\"\",\"labelPlacement\":\"\",\"descriptionPlacement\":\"\",\"subLabelPlacement\":\"\",\"placeholder\":\"\",\"cssClass\":\"\",\"inputName\":\"\",\"noDuplicates\":false,\"defaultValue\":\"\",\"choices\":\"\",\"conditionalLogic\":\"\",\"productField\":\"\",\"enablePasswordInput\":\"\",\"multipleFiles\":false,\"maxFiles\":\"\",\"calculationFormula\":\"\",\"calculationRounding\":\"\",\"enableCalculation\":\"\",\"disableQuantity\":false,\"displayAllCategories\":false,\"useRichTextEditor\":false,\"pageNumber\":1,\"fields\":\"\"},{\"type\":\"text\",\"id\":4,\"label\":\"Email Address\",\"adminLabel\":\"\",\"isRequired\":true,\"size\":\"medium\",\"errorMessage\":\"\",\"visibility\":\"visible\",\"inputs\":null,\"formId\":1,\"description\":\"\",\"allowsPrepopulate\":false,\"inputMask\":false,\"inputMaskValue\":\"\",\"inputMaskIsCustom\":false,\"maxLength\":\"\",\"inputType\":\"\",\"labelPlacement\":\"\",\"descriptionPlacement\":\"\",\"subLabelPlacement\":\"\",\"placeholder\":\"\",\"cssClass\":\"\",\"inputName\":\"\",\"noDuplicates\":false,\"defaultValue\":\"\",\"choices\":\"\",\"conditionalLogic\":\"\",\"productField\":\"\",\"enablePasswordInput\":\"\",\"multipleFiles\":false,\"maxFiles\":\"\",\"calculationFormula\":\"\",\"calculationRounding\":\"\",\"enableCalculation\":\"\",\"disableQuantity\":false,\"displayAllCategories\":false,\"useRichTextEditor\":false,\"pageNumber\":1,\"fields\":\"\"},{\"type\":\"textarea\",\"id\":5,\"label\":\"Comments\",\"adminLabel\":\"\",\"isRequired\":true,\"size\":\"medium\",\"errorMessage\":\"\",\"visibility\":\"visible\",\"inputs\":null,\"formId\":1,\"description\":\"\",\"allowsPrepopulate\":false,\"inputMask\":false,\"inputMaskValue\":\"\",\"inputMaskIsCustom\":false,\"maxLength\":\"\",\"inputType\":\"\",\"labelPlacement\":\"\",\"descriptionPlacement\":\"\",\"subLabelPlacement\":\"\",\"placeholder\":\"\",\"cssClass\":\"\",\"inputName\":\"\",\"noDuplicates\":false,\"defaultValue\":\"\",\"choices\":\"\",\"conditionalLogic\":\"\",\"productField\":\"\",\"form_id\":\"\",\"useRichTextEditor\":false,\"multipleFiles\":false,\"maxFiles\":\"\",\"calculationFormula\":\"\",\"calculationRounding\":\"\",\"enableCalculation\":\"\",\"disableQuantity\":false,\"displayAllCategories\":false,\"pageNumber\":1,\"fields\":\"\"}],\"version\":\"2.4.22.5\",\"id\":1,\"nextFieldId\":6,\"useCurrentUserAsAuthor\":true,\"postContentTemplateEnabled\":false,\"postTitleTemplateEnabled\":false,\"postTitleTemplate\":\"\",\"postContentTemplate\":\"\",\"lastPageButton\":null,\"pagination\":null,\"firstPageCssClass\":null}',NULL,'{\"609ec3cae7c08\":{\"id\":\"609ec3cae7c08\",\"name\":\"Default Confirmation\",\"isDefault\":true,\"type\":\"message\",\"message\":\"Thanks for contacting us! We will get in touch with you shortly.\",\"url\":\"\",\"pageId\":\"\",\"queryString\":\"\"}}','{\"609ec3cad9e59\":{\"id\":\"609ec3cad9e59\",\"isActive\":true,\"to\":\"{admin_email}\",\"name\":\"Admin Notification\",\"event\":\"form_submission\",\"toType\":\"email\",\"subject\":\"New submission from {form_title}\",\"message\":\"{all_fields}\"}}');
/*!40000 ALTER TABLE `wp_gf_form_meta` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `wp_gf_form_revisions`
--

DROP TABLE IF EXISTS `wp_gf_form_revisions`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
 SET character_set_client = utf8mb4 ;
CREATE TABLE `wp_gf_form_revisions` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `form_id` mediumint(8) unsigned NOT NULL,
  `display_meta` longtext COLLATE utf8mb4_unicode_520_ci,
  `date_created` datetime NOT NULL,
  PRIMARY KEY (`id`),
  KEY `date_created` (`date_created`),
  KEY `form_id` (`form_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_520_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `wp_gf_form_revisions`
--

LOCK TABLES `wp_gf_form_revisions` WRITE;
/*!40000 ALTER TABLE `wp_gf_form_revisions` DISABLE KEYS */;
/*!40000 ALTER TABLE `wp_gf_form_revisions` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `wp_gf_form_view`
--

DROP TABLE IF EXISTS `wp_gf_form_view`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
 SET character_set_client = utf8mb4 ;
CREATE TABLE `wp_gf_form_view` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `form_id` mediumint(8) unsigned NOT NULL,
  `date_created` datetime NOT NULL,
  `ip` char(15) COLLATE utf8mb4_unicode_520_ci DEFAULT NULL,
  `count` mediumint(8) unsigned NOT NULL DEFAULT '1',
  PRIMARY KEY (`id`),
  KEY `date_created` (`date_created`),
  KEY `form_id` (`form_id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_520_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `wp_gf_form_view`
--

LOCK TABLES `wp_gf_form_view` WRITE;
/*!40000 ALTER TABLE `wp_gf_form_view` DISABLE KEYS */;
INSERT INTO `wp_gf_form_view` VALUES (1,1,'2021-05-14 18:40:33','',1);
INSERT INTO `wp_gf_form_view` VALUES (2,1,'2021-10-22 15:52:58','',3);
/*!40000 ALTER TABLE `wp_gf_form_view` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `wp_links`
--

DROP TABLE IF EXISTS `wp_links`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
 SET character_set_client = utf8mb4 ;
CREATE TABLE `wp_links` (
  `link_id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `link_url` varchar(255) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `link_name` varchar(255) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `link_image` varchar(255) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `link_target` varchar(25) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `link_description` varchar(255) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `link_visible` varchar(20) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT 'Y',
  `link_owner` bigint(20) unsigned NOT NULL DEFAULT '1',
  `link_rating` int(11) NOT NULL DEFAULT '0',
  `link_updated` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `link_rel` varchar(255) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `link_notes` mediumtext COLLATE utf8mb4_unicode_520_ci NOT NULL,
  `link_rss` varchar(255) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  PRIMARY KEY (`link_id`),
  KEY `link_visible` (`link_visible`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_520_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `wp_links`
--

LOCK TABLES `wp_links` WRITE;
/*!40000 ALTER TABLE `wp_links` DISABLE KEYS */;
/*!40000 ALTER TABLE `wp_links` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `wp_options`
--

DROP TABLE IF EXISTS `wp_options`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
 SET character_set_client = utf8mb4 ;
CREATE TABLE `wp_options` (
  `option_id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `option_name` varchar(191) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `option_value` longtext COLLATE utf8mb4_unicode_520_ci NOT NULL,
  `autoload` varchar(20) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT 'yes',
  PRIMARY KEY (`option_id`),
  UNIQUE KEY `option_name` (`option_name`),
  KEY `autoload` (`autoload`)
) ENGINE=InnoDB AUTO_INCREMENT=781 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_520_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `wp_options`
--

LOCK TABLES `wp_options` WRITE;
/*!40000 ALTER TABLE `wp_options` DISABLE KEYS */;
INSERT INTO `wp_options` VALUES (1,'siteurl','http://valleyhealth.local','yes');
INSERT INTO `wp_options` VALUES (2,'home','http://valleyhealth.local','yes');
INSERT INTO `wp_options` VALUES (3,'blogname','Valley Health Specialty Hospital','yes');
INSERT INTO `wp_options` VALUES (4,'blogdescription','','yes');
INSERT INTO `wp_options` VALUES (5,'users_can_register','0','yes');
INSERT INTO `wp_options` VALUES (6,'admin_email','kauland.buchanan@uhsinc.com','yes');
INSERT INTO `wp_options` VALUES (7,'start_of_week','1','yes');
INSERT INTO `wp_options` VALUES (8,'use_balanceTags','0','yes');
INSERT INTO `wp_options` VALUES (9,'use_smilies','1','yes');
INSERT INTO `wp_options` VALUES (10,'require_name_email','1','yes');
INSERT INTO `wp_options` VALUES (11,'comments_notify','1','yes');
INSERT INTO `wp_options` VALUES (12,'posts_per_rss','10','yes');
INSERT INTO `wp_options` VALUES (13,'rss_use_excerpt','0','yes');
INSERT INTO `wp_options` VALUES (14,'mailserver_url','mail.example.com','yes');
INSERT INTO `wp_options` VALUES (15,'mailserver_login','login@example.com','yes');
INSERT INTO `wp_options` VALUES (16,'mailserver_pass','password','yes');
INSERT INTO `wp_options` VALUES (17,'mailserver_port','110','yes');
INSERT INTO `wp_options` VALUES (18,'default_category','1','yes');
INSERT INTO `wp_options` VALUES (19,'default_comment_status','open','yes');
INSERT INTO `wp_options` VALUES (20,'default_ping_status','open','yes');
INSERT INTO `wp_options` VALUES (21,'default_pingback_flag','1','yes');
INSERT INTO `wp_options` VALUES (22,'posts_per_page','10','yes');
INSERT INTO `wp_options` VALUES (23,'date_format','F j, Y','yes');
INSERT INTO `wp_options` VALUES (24,'time_format','g:i a','yes');
INSERT INTO `wp_options` VALUES (25,'links_updated_date_format','F j, Y g:i a','yes');
INSERT INTO `wp_options` VALUES (26,'comment_moderation','0','yes');
INSERT INTO `wp_options` VALUES (27,'moderation_notify','1','yes');
INSERT INTO `wp_options` VALUES (28,'permalink_structure','/%postname%/','yes');
INSERT INTO `wp_options` VALUES (29,'rewrite_rules','a:161:{s:15:\"testimonials/?$\";s:39:\"index.php?post_type=sl_testimonial_cpts\";s:45:\"testimonials/feed/(feed|rdf|rss|rss2|atom)/?$\";s:56:\"index.php?post_type=sl_testimonial_cpts&feed=$matches[1]\";s:40:\"testimonials/(feed|rdf|rss|rss2|atom)/?$\";s:56:\"index.php?post_type=sl_testimonial_cpts&feed=$matches[1]\";s:32:\"testimonials/page/([0-9]{1,})/?$\";s:57:\"index.php?post_type=sl_testimonial_cpts&paged=$matches[1]\";s:7:\"faqs/?$\";s:32:\"index.php?post_type=sl_faqs_cpts\";s:37:\"faqs/feed/(feed|rdf|rss|rss2|atom)/?$\";s:49:\"index.php?post_type=sl_faqs_cpts&feed=$matches[1]\";s:32:\"faqs/(feed|rdf|rss|rss2|atom)/?$\";s:49:\"index.php?post_type=sl_faqs_cpts&feed=$matches[1]\";s:24:\"faqs/page/([0-9]{1,})/?$\";s:50:\"index.php?post_type=sl_faqs_cpts&paged=$matches[1]\";s:7:\"news/?$\";s:32:\"index.php?post_type=sl_news_cpts\";s:37:\"news/feed/(feed|rdf|rss|rss2|atom)/?$\";s:49:\"index.php?post_type=sl_news_cpts&feed=$matches[1]\";s:32:\"news/(feed|rdf|rss|rss2|atom)/?$\";s:49:\"index.php?post_type=sl_news_cpts&feed=$matches[1]\";s:24:\"news/page/([0-9]{1,})/?$\";s:50:\"index.php?post_type=sl_news_cpts&paged=$matches[1]\";s:11:\"^wp-json/?$\";s:22:\"index.php?rest_route=/\";s:14:\"^wp-json/(.*)?\";s:33:\"index.php?rest_route=/$matches[1]\";s:21:\"^index.php/wp-json/?$\";s:22:\"index.php?rest_route=/\";s:24:\"^index.php/wp-json/(.*)?\";s:33:\"index.php?rest_route=/$matches[1]\";s:17:\"^wp-sitemap\\.xml$\";s:23:\"index.php?sitemap=index\";s:17:\"^wp-sitemap\\.xsl$\";s:36:\"index.php?sitemap-stylesheet=sitemap\";s:23:\"^wp-sitemap-index\\.xsl$\";s:34:\"index.php?sitemap-stylesheet=index\";s:48:\"^wp-sitemap-([a-z]+?)-([a-z\\d_-]+?)-(\\d+?)\\.xml$\";s:75:\"index.php?sitemap=$matches[1]&sitemap-subtype=$matches[2]&paged=$matches[3]\";s:34:\"^wp-sitemap-([a-z]+?)-(\\d+?)\\.xml$\";s:47:\"index.php?sitemap=$matches[1]&paged=$matches[2]\";s:47:\"category/(.+?)/feed/(feed|rdf|rss|rss2|atom)/?$\";s:52:\"index.php?category_name=$matches[1]&feed=$matches[2]\";s:42:\"category/(.+?)/(feed|rdf|rss|rss2|atom)/?$\";s:52:\"index.php?category_name=$matches[1]&feed=$matches[2]\";s:23:\"category/(.+?)/embed/?$\";s:46:\"index.php?category_name=$matches[1]&embed=true\";s:35:\"category/(.+?)/page/?([0-9]{1,})/?$\";s:53:\"index.php?category_name=$matches[1]&paged=$matches[2]\";s:17:\"category/(.+?)/?$\";s:35:\"index.php?category_name=$matches[1]\";s:44:\"tag/([^/]+)/feed/(feed|rdf|rss|rss2|atom)/?$\";s:42:\"index.php?tag=$matches[1]&feed=$matches[2]\";s:39:\"tag/([^/]+)/(feed|rdf|rss|rss2|atom)/?$\";s:42:\"index.php?tag=$matches[1]&feed=$matches[2]\";s:20:\"tag/([^/]+)/embed/?$\";s:36:\"index.php?tag=$matches[1]&embed=true\";s:32:\"tag/([^/]+)/page/?([0-9]{1,})/?$\";s:43:\"index.php?tag=$matches[1]&paged=$matches[2]\";s:14:\"tag/([^/]+)/?$\";s:25:\"index.php?tag=$matches[1]\";s:45:\"type/([^/]+)/feed/(feed|rdf|rss|rss2|atom)/?$\";s:50:\"index.php?post_format=$matches[1]&feed=$matches[2]\";s:40:\"type/([^/]+)/(feed|rdf|rss|rss2|atom)/?$\";s:50:\"index.php?post_format=$matches[1]&feed=$matches[2]\";s:21:\"type/([^/]+)/embed/?$\";s:44:\"index.php?post_format=$matches[1]&embed=true\";s:33:\"type/([^/]+)/page/?([0-9]{1,})/?$\";s:51:\"index.php?post_format=$matches[1]&paged=$matches[2]\";s:15:\"type/([^/]+)/?$\";s:33:\"index.php?post_format=$matches[1]\";s:40:\"about-us/staff/.+?/attachment/([^/]+)/?$\";s:32:\"index.php?attachment=$matches[1]\";s:50:\"about-us/staff/.+?/attachment/([^/]+)/trackback/?$\";s:37:\"index.php?attachment=$matches[1]&tb=1\";s:70:\"about-us/staff/.+?/attachment/([^/]+)/feed/(feed|rdf|rss|rss2|atom)/?$\";s:49:\"index.php?attachment=$matches[1]&feed=$matches[2]\";s:65:\"about-us/staff/.+?/attachment/([^/]+)/(feed|rdf|rss|rss2|atom)/?$\";s:49:\"index.php?attachment=$matches[1]&feed=$matches[2]\";s:65:\"about-us/staff/.+?/attachment/([^/]+)/comment-page-([0-9]{1,})/?$\";s:50:\"index.php?attachment=$matches[1]&cpage=$matches[2]\";s:46:\"about-us/staff/.+?/attachment/([^/]+)/embed/?$\";s:43:\"index.php?attachment=$matches[1]&embed=true\";s:29:\"about-us/staff/(.+?)/embed/?$\";s:46:\"index.php?sl_staff_cpts=$matches[1]&embed=true\";s:33:\"about-us/staff/(.+?)/trackback/?$\";s:40:\"index.php?sl_staff_cpts=$matches[1]&tb=1\";s:41:\"about-us/staff/(.+?)/page/?([0-9]{1,})/?$\";s:53:\"index.php?sl_staff_cpts=$matches[1]&paged=$matches[2]\";s:48:\"about-us/staff/(.+?)/comment-page-([0-9]{1,})/?$\";s:53:\"index.php?sl_staff_cpts=$matches[1]&cpage=$matches[2]\";s:37:\"about-us/staff/(.+?)(?:/([0-9]+))?/?$\";s:52:\"index.php?sl_staff_cpts=$matches[1]&page=$matches[2]\";s:38:\"testimonials/.+?/attachment/([^/]+)/?$\";s:32:\"index.php?attachment=$matches[1]\";s:48:\"testimonials/.+?/attachment/([^/]+)/trackback/?$\";s:37:\"index.php?attachment=$matches[1]&tb=1\";s:68:\"testimonials/.+?/attachment/([^/]+)/feed/(feed|rdf|rss|rss2|atom)/?$\";s:49:\"index.php?attachment=$matches[1]&feed=$matches[2]\";s:63:\"testimonials/.+?/attachment/([^/]+)/(feed|rdf|rss|rss2|atom)/?$\";s:49:\"index.php?attachment=$matches[1]&feed=$matches[2]\";s:63:\"testimonials/.+?/attachment/([^/]+)/comment-page-([0-9]{1,})/?$\";s:50:\"index.php?attachment=$matches[1]&cpage=$matches[2]\";s:44:\"testimonials/.+?/attachment/([^/]+)/embed/?$\";s:43:\"index.php?attachment=$matches[1]&embed=true\";s:27:\"testimonials/(.+?)/embed/?$\";s:52:\"index.php?sl_testimonial_cpts=$matches[1]&embed=true\";s:31:\"testimonials/(.+?)/trackback/?$\";s:46:\"index.php?sl_testimonial_cpts=$matches[1]&tb=1\";s:51:\"testimonials/(.+?)/feed/(feed|rdf|rss|rss2|atom)/?$\";s:58:\"index.php?sl_testimonial_cpts=$matches[1]&feed=$matches[2]\";s:46:\"testimonials/(.+?)/(feed|rdf|rss|rss2|atom)/?$\";s:58:\"index.php?sl_testimonial_cpts=$matches[1]&feed=$matches[2]\";s:39:\"testimonials/(.+?)/page/?([0-9]{1,})/?$\";s:59:\"index.php?sl_testimonial_cpts=$matches[1]&paged=$matches[2]\";s:46:\"testimonials/(.+?)/comment-page-([0-9]{1,})/?$\";s:59:\"index.php?sl_testimonial_cpts=$matches[1]&cpage=$matches[2]\";s:35:\"testimonials/(.+?)(?:/([0-9]+))?/?$\";s:58:\"index.php?sl_testimonial_cpts=$matches[1]&page=$matches[2]\";s:30:\"faqs/.+?/attachment/([^/]+)/?$\";s:32:\"index.php?attachment=$matches[1]\";s:40:\"faqs/.+?/attachment/([^/]+)/trackback/?$\";s:37:\"index.php?attachment=$matches[1]&tb=1\";s:60:\"faqs/.+?/attachment/([^/]+)/feed/(feed|rdf|rss|rss2|atom)/?$\";s:49:\"index.php?attachment=$matches[1]&feed=$matches[2]\";s:55:\"faqs/.+?/attachment/([^/]+)/(feed|rdf|rss|rss2|atom)/?$\";s:49:\"index.php?attachment=$matches[1]&feed=$matches[2]\";s:55:\"faqs/.+?/attachment/([^/]+)/comment-page-([0-9]{1,})/?$\";s:50:\"index.php?attachment=$matches[1]&cpage=$matches[2]\";s:36:\"faqs/.+?/attachment/([^/]+)/embed/?$\";s:43:\"index.php?attachment=$matches[1]&embed=true\";s:19:\"faqs/(.+?)/embed/?$\";s:45:\"index.php?sl_faqs_cpts=$matches[1]&embed=true\";s:23:\"faqs/(.+?)/trackback/?$\";s:39:\"index.php?sl_faqs_cpts=$matches[1]&tb=1\";s:43:\"faqs/(.+?)/feed/(feed|rdf|rss|rss2|atom)/?$\";s:51:\"index.php?sl_faqs_cpts=$matches[1]&feed=$matches[2]\";s:38:\"faqs/(.+?)/(feed|rdf|rss|rss2|atom)/?$\";s:51:\"index.php?sl_faqs_cpts=$matches[1]&feed=$matches[2]\";s:31:\"faqs/(.+?)/page/?([0-9]{1,})/?$\";s:52:\"index.php?sl_faqs_cpts=$matches[1]&paged=$matches[2]\";s:38:\"faqs/(.+?)/comment-page-([0-9]{1,})/?$\";s:52:\"index.php?sl_faqs_cpts=$matches[1]&cpage=$matches[2]\";s:27:\"faqs/(.+?)(?:/([0-9]+))?/?$\";s:51:\"index.php?sl_faqs_cpts=$matches[1]&page=$matches[2]\";s:49:\"category/([^/]+)/feed/(feed|rdf|rss|rss2|atom)/?$\";s:53:\"index.php?faq_categories=$matches[1]&feed=$matches[2]\";s:44:\"category/([^/]+)/(feed|rdf|rss|rss2|atom)/?$\";s:53:\"index.php?faq_categories=$matches[1]&feed=$matches[2]\";s:25:\"category/([^/]+)/embed/?$\";s:47:\"index.php?faq_categories=$matches[1]&embed=true\";s:37:\"category/([^/]+)/page/?([0-9]{1,})/?$\";s:54:\"index.php?faq_categories=$matches[1]&paged=$matches[2]\";s:19:\"category/([^/]+)/?$\";s:36:\"index.php?faq_categories=$matches[1]\";s:30:\"news/.+?/attachment/([^/]+)/?$\";s:32:\"index.php?attachment=$matches[1]\";s:40:\"news/.+?/attachment/([^/]+)/trackback/?$\";s:37:\"index.php?attachment=$matches[1]&tb=1\";s:60:\"news/.+?/attachment/([^/]+)/feed/(feed|rdf|rss|rss2|atom)/?$\";s:49:\"index.php?attachment=$matches[1]&feed=$matches[2]\";s:55:\"news/.+?/attachment/([^/]+)/(feed|rdf|rss|rss2|atom)/?$\";s:49:\"index.php?attachment=$matches[1]&feed=$matches[2]\";s:55:\"news/.+?/attachment/([^/]+)/comment-page-([0-9]{1,})/?$\";s:50:\"index.php?attachment=$matches[1]&cpage=$matches[2]\";s:36:\"news/.+?/attachment/([^/]+)/embed/?$\";s:43:\"index.php?attachment=$matches[1]&embed=true\";s:19:\"news/(.+?)/embed/?$\";s:45:\"index.php?sl_news_cpts=$matches[1]&embed=true\";s:23:\"news/(.+?)/trackback/?$\";s:39:\"index.php?sl_news_cpts=$matches[1]&tb=1\";s:43:\"news/(.+?)/feed/(feed|rdf|rss|rss2|atom)/?$\";s:51:\"index.php?sl_news_cpts=$matches[1]&feed=$matches[2]\";s:38:\"news/(.+?)/(feed|rdf|rss|rss2|atom)/?$\";s:51:\"index.php?sl_news_cpts=$matches[1]&feed=$matches[2]\";s:31:\"news/(.+?)/page/?([0-9]{1,})/?$\";s:52:\"index.php?sl_news_cpts=$matches[1]&paged=$matches[2]\";s:38:\"news/(.+?)/comment-page-([0-9]{1,})/?$\";s:52:\"index.php?sl_news_cpts=$matches[1]&cpage=$matches[2]\";s:27:\"news/(.+?)(?:/([0-9]+))?/?$\";s:51:\"index.php?sl_news_cpts=$matches[1]&page=$matches[2]\";s:12:\"robots\\.txt$\";s:18:\"index.php?robots=1\";s:13:\"favicon\\.ico$\";s:19:\"index.php?favicon=1\";s:48:\".*wp-(atom|rdf|rss|rss2|feed|commentsrss2)\\.php$\";s:18:\"index.php?feed=old\";s:20:\".*wp-app\\.php(/.*)?$\";s:19:\"index.php?error=403\";s:18:\".*wp-register.php$\";s:23:\"index.php?register=true\";s:32:\"feed/(feed|rdf|rss|rss2|atom)/?$\";s:27:\"index.php?&feed=$matches[1]\";s:27:\"(feed|rdf|rss|rss2|atom)/?$\";s:27:\"index.php?&feed=$matches[1]\";s:8:\"embed/?$\";s:21:\"index.php?&embed=true\";s:20:\"page/?([0-9]{1,})/?$\";s:28:\"index.php?&paged=$matches[1]\";s:27:\"comment-page-([0-9]{1,})/?$\";s:38:\"index.php?&page_id=9&cpage=$matches[1]\";s:41:\"comments/feed/(feed|rdf|rss|rss2|atom)/?$\";s:42:\"index.php?&feed=$matches[1]&withcomments=1\";s:36:\"comments/(feed|rdf|rss|rss2|atom)/?$\";s:42:\"index.php?&feed=$matches[1]&withcomments=1\";s:17:\"comments/embed/?$\";s:21:\"index.php?&embed=true\";s:44:\"search/(.+)/feed/(feed|rdf|rss|rss2|atom)/?$\";s:40:\"index.php?s=$matches[1]&feed=$matches[2]\";s:39:\"search/(.+)/(feed|rdf|rss|rss2|atom)/?$\";s:40:\"index.php?s=$matches[1]&feed=$matches[2]\";s:20:\"search/(.+)/embed/?$\";s:34:\"index.php?s=$matches[1]&embed=true\";s:32:\"search/(.+)/page/?([0-9]{1,})/?$\";s:41:\"index.php?s=$matches[1]&paged=$matches[2]\";s:14:\"search/(.+)/?$\";s:23:\"index.php?s=$matches[1]\";s:47:\"author/([^/]+)/feed/(feed|rdf|rss|rss2|atom)/?$\";s:50:\"index.php?author_name=$matches[1]&feed=$matches[2]\";s:42:\"author/([^/]+)/(feed|rdf|rss|rss2|atom)/?$\";s:50:\"index.php?author_name=$matches[1]&feed=$matches[2]\";s:23:\"author/([^/]+)/embed/?$\";s:44:\"index.php?author_name=$matches[1]&embed=true\";s:35:\"author/([^/]+)/page/?([0-9]{1,})/?$\";s:51:\"index.php?author_name=$matches[1]&paged=$matches[2]\";s:17:\"author/([^/]+)/?$\";s:33:\"index.php?author_name=$matches[1]\";s:69:\"([0-9]{4})/([0-9]{1,2})/([0-9]{1,2})/feed/(feed|rdf|rss|rss2|atom)/?$\";s:80:\"index.php?year=$matches[1]&monthnum=$matches[2]&day=$matches[3]&feed=$matches[4]\";s:64:\"([0-9]{4})/([0-9]{1,2})/([0-9]{1,2})/(feed|rdf|rss|rss2|atom)/?$\";s:80:\"index.php?year=$matches[1]&monthnum=$matches[2]&day=$matches[3]&feed=$matches[4]\";s:45:\"([0-9]{4})/([0-9]{1,2})/([0-9]{1,2})/embed/?$\";s:74:\"index.php?year=$matches[1]&monthnum=$matches[2]&day=$matches[3]&embed=true\";s:57:\"([0-9]{4})/([0-9]{1,2})/([0-9]{1,2})/page/?([0-9]{1,})/?$\";s:81:\"index.php?year=$matches[1]&monthnum=$matches[2]&day=$matches[3]&paged=$matches[4]\";s:39:\"([0-9]{4})/([0-9]{1,2})/([0-9]{1,2})/?$\";s:63:\"index.php?year=$matches[1]&monthnum=$matches[2]&day=$matches[3]\";s:56:\"([0-9]{4})/([0-9]{1,2})/feed/(feed|rdf|rss|rss2|atom)/?$\";s:64:\"index.php?year=$matches[1]&monthnum=$matches[2]&feed=$matches[3]\";s:51:\"([0-9]{4})/([0-9]{1,2})/(feed|rdf|rss|rss2|atom)/?$\";s:64:\"index.php?year=$matches[1]&monthnum=$matches[2]&feed=$matches[3]\";s:32:\"([0-9]{4})/([0-9]{1,2})/embed/?$\";s:58:\"index.php?year=$matches[1]&monthnum=$matches[2]&embed=true\";s:44:\"([0-9]{4})/([0-9]{1,2})/page/?([0-9]{1,})/?$\";s:65:\"index.php?year=$matches[1]&monthnum=$matches[2]&paged=$matches[3]\";s:26:\"([0-9]{4})/([0-9]{1,2})/?$\";s:47:\"index.php?year=$matches[1]&monthnum=$matches[2]\";s:43:\"([0-9]{4})/feed/(feed|rdf|rss|rss2|atom)/?$\";s:43:\"index.php?year=$matches[1]&feed=$matches[2]\";s:38:\"([0-9]{4})/(feed|rdf|rss|rss2|atom)/?$\";s:43:\"index.php?year=$matches[1]&feed=$matches[2]\";s:19:\"([0-9]{4})/embed/?$\";s:37:\"index.php?year=$matches[1]&embed=true\";s:31:\"([0-9]{4})/page/?([0-9]{1,})/?$\";s:44:\"index.php?year=$matches[1]&paged=$matches[2]\";s:13:\"([0-9]{4})/?$\";s:26:\"index.php?year=$matches[1]\";s:27:\".?.+?/attachment/([^/]+)/?$\";s:32:\"index.php?attachment=$matches[1]\";s:37:\".?.+?/attachment/([^/]+)/trackback/?$\";s:37:\"index.php?attachment=$matches[1]&tb=1\";s:57:\".?.+?/attachment/([^/]+)/feed/(feed|rdf|rss|rss2|atom)/?$\";s:49:\"index.php?attachment=$matches[1]&feed=$matches[2]\";s:52:\".?.+?/attachment/([^/]+)/(feed|rdf|rss|rss2|atom)/?$\";s:49:\"index.php?attachment=$matches[1]&feed=$matches[2]\";s:52:\".?.+?/attachment/([^/]+)/comment-page-([0-9]{1,})/?$\";s:50:\"index.php?attachment=$matches[1]&cpage=$matches[2]\";s:33:\".?.+?/attachment/([^/]+)/embed/?$\";s:43:\"index.php?attachment=$matches[1]&embed=true\";s:16:\"(.?.+?)/embed/?$\";s:41:\"index.php?pagename=$matches[1]&embed=true\";s:20:\"(.?.+?)/trackback/?$\";s:35:\"index.php?pagename=$matches[1]&tb=1\";s:40:\"(.?.+?)/feed/(feed|rdf|rss|rss2|atom)/?$\";s:47:\"index.php?pagename=$matches[1]&feed=$matches[2]\";s:35:\"(.?.+?)/(feed|rdf|rss|rss2|atom)/?$\";s:47:\"index.php?pagename=$matches[1]&feed=$matches[2]\";s:28:\"(.?.+?)/page/?([0-9]{1,})/?$\";s:48:\"index.php?pagename=$matches[1]&paged=$matches[2]\";s:35:\"(.?.+?)/comment-page-([0-9]{1,})/?$\";s:48:\"index.php?pagename=$matches[1]&cpage=$matches[2]\";s:24:\"(.?.+?)(?:/([0-9]+))?/?$\";s:47:\"index.php?pagename=$matches[1]&page=$matches[2]\";s:27:\"[^/]+/attachment/([^/]+)/?$\";s:32:\"index.php?attachment=$matches[1]\";s:37:\"[^/]+/attachment/([^/]+)/trackback/?$\";s:37:\"index.php?attachment=$matches[1]&tb=1\";s:57:\"[^/]+/attachment/([^/]+)/feed/(feed|rdf|rss|rss2|atom)/?$\";s:49:\"index.php?attachment=$matches[1]&feed=$matches[2]\";s:52:\"[^/]+/attachment/([^/]+)/(feed|rdf|rss|rss2|atom)/?$\";s:49:\"index.php?attachment=$matches[1]&feed=$matches[2]\";s:52:\"[^/]+/attachment/([^/]+)/comment-page-([0-9]{1,})/?$\";s:50:\"index.php?attachment=$matches[1]&cpage=$matches[2]\";s:33:\"[^/]+/attachment/([^/]+)/embed/?$\";s:43:\"index.php?attachment=$matches[1]&embed=true\";s:16:\"([^/]+)/embed/?$\";s:37:\"index.php?name=$matches[1]&embed=true\";s:20:\"([^/]+)/trackback/?$\";s:31:\"index.php?name=$matches[1]&tb=1\";s:40:\"([^/]+)/feed/(feed|rdf|rss|rss2|atom)/?$\";s:43:\"index.php?name=$matches[1]&feed=$matches[2]\";s:35:\"([^/]+)/(feed|rdf|rss|rss2|atom)/?$\";s:43:\"index.php?name=$matches[1]&feed=$matches[2]\";s:28:\"([^/]+)/page/?([0-9]{1,})/?$\";s:44:\"index.php?name=$matches[1]&paged=$matches[2]\";s:35:\"([^/]+)/comment-page-([0-9]{1,})/?$\";s:44:\"index.php?name=$matches[1]&cpage=$matches[2]\";s:24:\"([^/]+)(?:/([0-9]+))?/?$\";s:43:\"index.php?name=$matches[1]&page=$matches[2]\";s:16:\"[^/]+/([^/]+)/?$\";s:32:\"index.php?attachment=$matches[1]\";s:26:\"[^/]+/([^/]+)/trackback/?$\";s:37:\"index.php?attachment=$matches[1]&tb=1\";s:46:\"[^/]+/([^/]+)/feed/(feed|rdf|rss|rss2|atom)/?$\";s:49:\"index.php?attachment=$matches[1]&feed=$matches[2]\";s:41:\"[^/]+/([^/]+)/(feed|rdf|rss|rss2|atom)/?$\";s:49:\"index.php?attachment=$matches[1]&feed=$matches[2]\";s:41:\"[^/]+/([^/]+)/comment-page-([0-9]{1,})/?$\";s:50:\"index.php?attachment=$matches[1]&cpage=$matches[2]\";s:22:\"[^/]+/([^/]+)/embed/?$\";s:43:\"index.php?attachment=$matches[1]&embed=true\";}','yes');
INSERT INTO `wp_options` VALUES (30,'hack_file','0','yes');
INSERT INTO `wp_options` VALUES (31,'blog_charset','UTF-8','yes');
INSERT INTO `wp_options` VALUES (32,'moderation_keys','','no');
INSERT INTO `wp_options` VALUES (33,'active_plugins','a:4:{i:0;s:29:\"gravityforms/gravityforms.php\";i:1;s:33:\"classic-editor/classic-editor.php\";i:2;s:27:\"redirection/redirection.php\";i:3;s:25:\"relevanssi/relevanssi.php\";}','yes');
INSERT INTO `wp_options` VALUES (34,'category_base','','yes');
INSERT INTO `wp_options` VALUES (35,'ping_sites','http://rpc.pingomatic.com/','yes');
INSERT INTO `wp_options` VALUES (36,'comment_max_links','2','yes');
INSERT INTO `wp_options` VALUES (37,'gmt_offset','0','yes');
INSERT INTO `wp_options` VALUES (38,'default_email_category','1','yes');
INSERT INTO `wp_options` VALUES (39,'recently_edited','','no');
INSERT INTO `wp_options` VALUES (40,'template','slate','yes');
INSERT INTO `wp_options` VALUES (41,'stylesheet','slate','yes');
INSERT INTO `wp_options` VALUES (42,'comment_registration','0','yes');
INSERT INTO `wp_options` VALUES (43,'html_type','text/html','yes');
INSERT INTO `wp_options` VALUES (44,'use_trackback','0','yes');
INSERT INTO `wp_options` VALUES (45,'default_role','subscriber','yes');
INSERT INTO `wp_options` VALUES (46,'db_version','49752','yes');
INSERT INTO `wp_options` VALUES (47,'uploads_use_yearmonth_folders','1','yes');
INSERT INTO `wp_options` VALUES (48,'upload_path','','yes');
INSERT INTO `wp_options` VALUES (49,'blog_public','1','yes');
INSERT INTO `wp_options` VALUES (50,'default_link_category','2','yes');
INSERT INTO `wp_options` VALUES (51,'show_on_front','page','yes');
INSERT INTO `wp_options` VALUES (52,'tag_base','','yes');
INSERT INTO `wp_options` VALUES (53,'show_avatars','1','yes');
INSERT INTO `wp_options` VALUES (54,'avatar_rating','G','yes');
INSERT INTO `wp_options` VALUES (55,'upload_url_path','','yes');
INSERT INTO `wp_options` VALUES (56,'thumbnail_size_w','150','yes');
INSERT INTO `wp_options` VALUES (57,'thumbnail_size_h','150','yes');
INSERT INTO `wp_options` VALUES (58,'thumbnail_crop','1','yes');
INSERT INTO `wp_options` VALUES (59,'medium_size_w','300','yes');
INSERT INTO `wp_options` VALUES (60,'medium_size_h','300','yes');
INSERT INTO `wp_options` VALUES (61,'avatar_default','mystery','yes');
INSERT INTO `wp_options` VALUES (62,'large_size_w','1024','yes');
INSERT INTO `wp_options` VALUES (63,'large_size_h','1024','yes');
INSERT INTO `wp_options` VALUES (64,'image_default_link_type','none','yes');
INSERT INTO `wp_options` VALUES (65,'image_default_size','','yes');
INSERT INTO `wp_options` VALUES (66,'image_default_align','','yes');
INSERT INTO `wp_options` VALUES (67,'close_comments_for_old_posts','0','yes');
INSERT INTO `wp_options` VALUES (68,'close_comments_days_old','14','yes');
INSERT INTO `wp_options` VALUES (69,'thread_comments','1','yes');
INSERT INTO `wp_options` VALUES (70,'thread_comments_depth','5','yes');
INSERT INTO `wp_options` VALUES (71,'page_comments','0','yes');
INSERT INTO `wp_options` VALUES (72,'comments_per_page','50','yes');
INSERT INTO `wp_options` VALUES (73,'default_comments_page','newest','yes');
INSERT INTO `wp_options` VALUES (74,'comment_order','asc','yes');
INSERT INTO `wp_options` VALUES (75,'sticky_posts','a:0:{}','yes');
INSERT INTO `wp_options` VALUES (76,'widget_categories','a:2:{i:2;a:4:{s:5:\"title\";s:0:\"\";s:5:\"count\";i:0;s:12:\"hierarchical\";i:0;s:8:\"dropdown\";i:0;}s:12:\"_multiwidget\";i:1;}','yes');
INSERT INTO `wp_options` VALUES (77,'widget_text','a:2:{i:1;a:0:{}s:12:\"_multiwidget\";i:1;}','yes');
INSERT INTO `wp_options` VALUES (78,'widget_rss','a:2:{i:1;a:0:{}s:12:\"_multiwidget\";i:1;}','yes');
INSERT INTO `wp_options` VALUES (79,'uninstall_plugins','a:1:{s:27:\"redirection/redirection.php\";a:2:{i:0;s:17:\"Redirection_Admin\";i:1;s:16:\"plugin_uninstall\";}}','no');
INSERT INTO `wp_options` VALUES (80,'timezone_string','','yes');
INSERT INTO `wp_options` VALUES (81,'page_for_posts','0','yes');
INSERT INTO `wp_options` VALUES (82,'page_on_front','9','yes');
INSERT INTO `wp_options` VALUES (83,'default_post_format','0','yes');
INSERT INTO `wp_options` VALUES (84,'link_manager_enabled','0','yes');
INSERT INTO `wp_options` VALUES (85,'finished_splitting_shared_terms','1','yes');
INSERT INTO `wp_options` VALUES (86,'site_icon','0','yes');
INSERT INTO `wp_options` VALUES (87,'medium_large_size_w','768','yes');
INSERT INTO `wp_options` VALUES (88,'medium_large_size_h','0','yes');
INSERT INTO `wp_options` VALUES (89,'wp_page_for_privacy_policy','3','yes');
INSERT INTO `wp_options` VALUES (90,'show_comments_cookies_opt_in','1','yes');
INSERT INTO `wp_options` VALUES (91,'admin_email_lifespan','1635521330','yes');
INSERT INTO `wp_options` VALUES (92,'disallowed_keys','','no');
INSERT INTO `wp_options` VALUES (93,'comment_previously_approved','1','yes');
INSERT INTO `wp_options` VALUES (94,'auto_plugin_theme_update_emails','a:0:{}','no');
INSERT INTO `wp_options` VALUES (95,'auto_update_core_dev','enabled','yes');
INSERT INTO `wp_options` VALUES (96,'auto_update_core_minor','enabled','yes');
INSERT INTO `wp_options` VALUES (97,'auto_update_core_major','enabled','yes');
INSERT INTO `wp_options` VALUES (98,'initial_db_version','49752','yes');
INSERT INTO `wp_options` VALUES (99,'wp_user_roles','a:5:{s:13:\"administrator\";a:2:{s:4:\"name\";s:13:\"Administrator\";s:12:\"capabilities\";a:61:{s:13:\"switch_themes\";b:1;s:11:\"edit_themes\";b:1;s:16:\"activate_plugins\";b:1;s:12:\"edit_plugins\";b:1;s:10:\"edit_users\";b:1;s:10:\"edit_files\";b:1;s:14:\"manage_options\";b:1;s:17:\"moderate_comments\";b:1;s:17:\"manage_categories\";b:1;s:12:\"manage_links\";b:1;s:12:\"upload_files\";b:1;s:6:\"import\";b:1;s:15:\"unfiltered_html\";b:1;s:10:\"edit_posts\";b:1;s:17:\"edit_others_posts\";b:1;s:20:\"edit_published_posts\";b:1;s:13:\"publish_posts\";b:1;s:10:\"edit_pages\";b:1;s:4:\"read\";b:1;s:8:\"level_10\";b:1;s:7:\"level_9\";b:1;s:7:\"level_8\";b:1;s:7:\"level_7\";b:1;s:7:\"level_6\";b:1;s:7:\"level_5\";b:1;s:7:\"level_4\";b:1;s:7:\"level_3\";b:1;s:7:\"level_2\";b:1;s:7:\"level_1\";b:1;s:7:\"level_0\";b:1;s:17:\"edit_others_pages\";b:1;s:20:\"edit_published_pages\";b:1;s:13:\"publish_pages\";b:1;s:12:\"delete_pages\";b:1;s:19:\"delete_others_pages\";b:1;s:22:\"delete_published_pages\";b:1;s:12:\"delete_posts\";b:1;s:19:\"delete_others_posts\";b:1;s:22:\"delete_published_posts\";b:1;s:20:\"delete_private_posts\";b:1;s:18:\"edit_private_posts\";b:1;s:18:\"read_private_posts\";b:1;s:20:\"delete_private_pages\";b:1;s:18:\"edit_private_pages\";b:1;s:18:\"read_private_pages\";b:1;s:12:\"delete_users\";b:1;s:12:\"create_users\";b:1;s:17:\"unfiltered_upload\";b:1;s:14:\"edit_dashboard\";b:1;s:14:\"update_plugins\";b:1;s:14:\"delete_plugins\";b:1;s:15:\"install_plugins\";b:1;s:13:\"update_themes\";b:1;s:14:\"install_themes\";b:1;s:11:\"update_core\";b:1;s:10:\"list_users\";b:1;s:12:\"remove_users\";b:1;s:13:\"promote_users\";b:1;s:18:\"edit_theme_options\";b:1;s:13:\"delete_themes\";b:1;s:6:\"export\";b:1;}}s:6:\"editor\";a:2:{s:4:\"name\";s:6:\"Editor\";s:12:\"capabilities\";a:34:{s:17:\"moderate_comments\";b:1;s:17:\"manage_categories\";b:1;s:12:\"manage_links\";b:1;s:12:\"upload_files\";b:1;s:15:\"unfiltered_html\";b:1;s:10:\"edit_posts\";b:1;s:17:\"edit_others_posts\";b:1;s:20:\"edit_published_posts\";b:1;s:13:\"publish_posts\";b:1;s:10:\"edit_pages\";b:1;s:4:\"read\";b:1;s:7:\"level_7\";b:1;s:7:\"level_6\";b:1;s:7:\"level_5\";b:1;s:7:\"level_4\";b:1;s:7:\"level_3\";b:1;s:7:\"level_2\";b:1;s:7:\"level_1\";b:1;s:7:\"level_0\";b:1;s:17:\"edit_others_pages\";b:1;s:20:\"edit_published_pages\";b:1;s:13:\"publish_pages\";b:1;s:12:\"delete_pages\";b:1;s:19:\"delete_others_pages\";b:1;s:22:\"delete_published_pages\";b:1;s:12:\"delete_posts\";b:1;s:19:\"delete_others_posts\";b:1;s:22:\"delete_published_posts\";b:1;s:20:\"delete_private_posts\";b:1;s:18:\"edit_private_posts\";b:1;s:18:\"read_private_posts\";b:1;s:20:\"delete_private_pages\";b:1;s:18:\"edit_private_pages\";b:1;s:18:\"read_private_pages\";b:1;}}s:6:\"author\";a:2:{s:4:\"name\";s:6:\"Author\";s:12:\"capabilities\";a:10:{s:12:\"upload_files\";b:1;s:10:\"edit_posts\";b:1;s:20:\"edit_published_posts\";b:1;s:13:\"publish_posts\";b:1;s:4:\"read\";b:1;s:7:\"level_2\";b:1;s:7:\"level_1\";b:1;s:7:\"level_0\";b:1;s:12:\"delete_posts\";b:1;s:22:\"delete_published_posts\";b:1;}}s:11:\"contributor\";a:2:{s:4:\"name\";s:11:\"Contributor\";s:12:\"capabilities\";a:5:{s:10:\"edit_posts\";b:1;s:4:\"read\";b:1;s:7:\"level_1\";b:1;s:7:\"level_0\";b:1;s:12:\"delete_posts\";b:1;}}s:10:\"subscriber\";a:2:{s:4:\"name\";s:10:\"Subscriber\";s:12:\"capabilities\";a:2:{s:4:\"read\";b:1;s:7:\"level_0\";b:1;}}}','yes');
INSERT INTO `wp_options` VALUES (100,'fresh_site','0','yes');
INSERT INTO `wp_options` VALUES (101,'widget_search','a:2:{i:2;a:1:{s:5:\"title\";s:0:\"\";}s:12:\"_multiwidget\";i:1;}','yes');
INSERT INTO `wp_options` VALUES (102,'widget_recent-posts','a:2:{i:2;a:2:{s:5:\"title\";s:0:\"\";s:6:\"number\";i:5;}s:12:\"_multiwidget\";i:1;}','yes');
INSERT INTO `wp_options` VALUES (103,'widget_recent-comments','a:2:{i:2;a:2:{s:5:\"title\";s:0:\"\";s:6:\"number\";i:5;}s:12:\"_multiwidget\";i:1;}','yes');
INSERT INTO `wp_options` VALUES (104,'widget_archives','a:2:{i:2;a:3:{s:5:\"title\";s:0:\"\";s:5:\"count\";i:0;s:8:\"dropdown\";i:0;}s:12:\"_multiwidget\";i:1;}','yes');
INSERT INTO `wp_options` VALUES (105,'widget_meta','a:2:{i:2;a:1:{s:5:\"title\";s:0:\"\";}s:12:\"_multiwidget\";i:1;}','yes');
INSERT INTO `wp_options` VALUES (106,'sidebars_widgets','a:3:{s:19:\"wp_inactive_widgets\";a:6:{i:0;s:8:\"search-2\";i:1;s:14:\"recent-posts-2\";i:2;s:17:\"recent-comments-2\";i:3;s:10:\"archives-2\";i:4;s:12:\"categories-2\";i:5;s:6:\"meta-2\";}s:6:\"side_1\";a:0:{}s:13:\"array_version\";i:3;}','yes');
INSERT INTO `wp_options` VALUES (107,'cron','a:8:{i:1639427976;a:1:{s:34:\"wp_privacy_delete_old_export_files\";a:1:{s:32:\"40cd750bba9870f18aada2478b24840a\";a:3:{s:8:\"schedule\";s:6:\"hourly\";s:4:\"args\";a:0:{}s:8:\"interval\";i:3600;}}}i:1639460376;a:4:{s:18:\"wp_https_detection\";a:1:{s:32:\"40cd750bba9870f18aada2478b24840a\";a:3:{s:8:\"schedule\";s:10:\"twicedaily\";s:4:\"args\";a:0:{}s:8:\"interval\";i:43200;}}s:16:\"wp_version_check\";a:1:{s:32:\"40cd750bba9870f18aada2478b24840a\";a:3:{s:8:\"schedule\";s:10:\"twicedaily\";s:4:\"args\";a:0:{}s:8:\"interval\";i:43200;}}s:17:\"wp_update_plugins\";a:1:{s:32:\"40cd750bba9870f18aada2478b24840a\";a:3:{s:8:\"schedule\";s:10:\"twicedaily\";s:4:\"args\";a:0:{}s:8:\"interval\";i:43200;}}s:16:\"wp_update_themes\";a:1:{s:32:\"40cd750bba9870f18aada2478b24840a\";a:3:{s:8:\"schedule\";s:10:\"twicedaily\";s:4:\"args\";a:0:{}s:8:\"interval\";i:43200;}}}i:1639460639;a:1:{s:35:\"puc_cron_check_updates-slate-plugin\";a:1:{s:32:\"40cd750bba9870f18aada2478b24840a\";a:3:{s:8:\"schedule\";s:10:\"twicedaily\";s:4:\"args\";a:0:{}s:8:\"interval\";i:43200;}}}i:1639503576;a:2:{s:30:\"wp_site_health_scheduled_check\";a:1:{s:32:\"40cd750bba9870f18aada2478b24840a\";a:3:{s:8:\"schedule\";s:6:\"weekly\";s:4:\"args\";a:0:{}s:8:\"interval\";i:604800;}}s:32:\"recovery_mode_clean_expired_keys\";a:1:{s:32:\"40cd750bba9870f18aada2478b24840a\";a:3:{s:8:\"schedule\";s:5:\"daily\";s:4:\"args\";a:0:{}s:8:\"interval\";i:86400;}}}i:1639503850;a:3:{s:19:\"wp_scheduled_delete\";a:1:{s:32:\"40cd750bba9870f18aada2478b24840a\";a:3:{s:8:\"schedule\";s:5:\"daily\";s:4:\"args\";a:0:{}s:8:\"interval\";i:86400;}}s:25:\"delete_expired_transients\";a:1:{s:32:\"40cd750bba9870f18aada2478b24840a\";a:3:{s:8:\"schedule\";s:5:\"daily\";s:4:\"args\";a:0:{}s:8:\"interval\";i:86400;}}s:30:\"wp_scheduled_auto_draft_delete\";a:1:{s:32:\"40cd750bba9870f18aada2478b24840a\";a:3:{s:8:\"schedule\";s:5:\"daily\";s:4:\"args\";a:0:{}s:8:\"interval\";i:86400;}}}i:1639510153;a:1:{s:17:\"gravityforms_cron\";a:1:{s:32:\"40cd750bba9870f18aada2478b24840a\";a:3:{s:8:\"schedule\";s:5:\"daily\";s:4:\"args\";a:0:{}s:8:\"interval\";i:86400;}}}i:1639510166;a:1:{s:24:\"relevanssi_update_counts\";a:1:{s:32:\"40cd750bba9870f18aada2478b24840a\";a:3:{s:8:\"schedule\";s:6:\"weekly\";s:4:\"args\";a:0:{}s:8:\"interval\";i:604800;}}}s:7:\"version\";i:2;}','yes');
INSERT INTO `wp_options` VALUES (108,'widget_pages','a:1:{s:12:\"_multiwidget\";i:1;}','yes');
INSERT INTO `wp_options` VALUES (109,'widget_calendar','a:1:{s:12:\"_multiwidget\";i:1;}','yes');
INSERT INTO `wp_options` VALUES (110,'widget_media_audio','a:1:{s:12:\"_multiwidget\";i:1;}','yes');
INSERT INTO `wp_options` VALUES (111,'widget_media_image','a:1:{s:12:\"_multiwidget\";i:1;}','yes');
INSERT INTO `wp_options` VALUES (112,'widget_media_gallery','a:1:{s:12:\"_multiwidget\";i:1;}','yes');
INSERT INTO `wp_options` VALUES (113,'widget_media_video','a:1:{s:12:\"_multiwidget\";i:1;}','yes');
INSERT INTO `wp_options` VALUES (114,'nonce_key','RZD5#a}ErhYbb2AtfE>.[E`kx]~,ENs}^%ZHcdEa<Nhv.9[8m #4JNO{QVC}0pI3','no');
INSERT INTO `wp_options` VALUES (115,'nonce_salt','J~QO!wD3R9Q3%UfZat]G,O~GxLHC`/8yU:Yia1{zRZ3u!%A.mFM,wNF7]=mlzxKP','no');
INSERT INTO `wp_options` VALUES (116,'widget_tag_cloud','a:1:{s:12:\"_multiwidget\";i:1;}','yes');
INSERT INTO `wp_options` VALUES (117,'widget_nav_menu','a:1:{s:12:\"_multiwidget\";i:1;}','yes');
INSERT INTO `wp_options` VALUES (118,'widget_custom_html','a:1:{s:12:\"_multiwidget\";i:1;}','yes');
INSERT INTO `wp_options` VALUES (120,'recovery_keys','a:0:{}','yes');
INSERT INTO `wp_options` VALUES (121,'theme_mods_twentytwentyone','a:2:{s:18:\"custom_css_post_id\";i:-1;s:16:\"sidebars_widgets\";a:2:{s:4:\"time\";i:1618854261;s:4:\"data\";a:3:{s:19:\"wp_inactive_widgets\";a:0:{}s:9:\"sidebar-1\";a:3:{i:0;s:8:\"search-2\";i:1;s:14:\"recent-posts-2\";i:2;s:17:\"recent-comments-2\";}s:9:\"sidebar-2\";a:3:{i:0;s:10:\"archives-2\";i:1;s:12:\"categories-2\";i:2;s:6:\"meta-2\";}}}}','yes');
INSERT INTO `wp_options` VALUES (122,'https_detection_errors','a:1:{s:23:\"ssl_verification_failed\";a:1:{i:0;s:24:\"SSL verification failed.\";}}','yes');
INSERT INTO `wp_options` VALUES (123,'_site_transient_update_core','O:8:\"stdClass\":4:{s:7:\"updates\";a:3:{i:0;O:8:\"stdClass\":10:{s:8:\"response\";s:7:\"upgrade\";s:8:\"download\";s:59:\"https://downloads.wordpress.org/release/wordpress-5.8.2.zip\";s:6:\"locale\";s:5:\"en_US\";s:8:\"packages\";O:8:\"stdClass\":5:{s:4:\"full\";s:59:\"https://downloads.wordpress.org/release/wordpress-5.8.2.zip\";s:10:\"no_content\";s:70:\"https://downloads.wordpress.org/release/wordpress-5.8.2-no-content.zip\";s:11:\"new_bundled\";s:71:\"https://downloads.wordpress.org/release/wordpress-5.8.2-new-bundled.zip\";s:7:\"partial\";s:0:\"\";s:8:\"rollback\";s:0:\"\";}s:7:\"current\";s:5:\"5.8.2\";s:7:\"version\";s:5:\"5.8.2\";s:11:\"php_version\";s:6:\"5.6.20\";s:13:\"mysql_version\";s:3:\"5.0\";s:11:\"new_bundled\";s:3:\"5.6\";s:15:\"partial_version\";s:0:\"\";}i:1;O:8:\"stdClass\":11:{s:8:\"response\";s:10:\"autoupdate\";s:8:\"download\";s:59:\"https://downloads.wordpress.org/release/wordpress-5.8.2.zip\";s:6:\"locale\";s:5:\"en_US\";s:8:\"packages\";O:8:\"stdClass\":5:{s:4:\"full\";s:59:\"https://downloads.wordpress.org/release/wordpress-5.8.2.zip\";s:10:\"no_content\";s:70:\"https://downloads.wordpress.org/release/wordpress-5.8.2-no-content.zip\";s:11:\"new_bundled\";s:71:\"https://downloads.wordpress.org/release/wordpress-5.8.2-new-bundled.zip\";s:7:\"partial\";s:0:\"\";s:8:\"rollback\";s:0:\"\";}s:7:\"current\";s:5:\"5.8.2\";s:7:\"version\";s:5:\"5.8.2\";s:11:\"php_version\";s:6:\"5.6.20\";s:13:\"mysql_version\";s:3:\"5.0\";s:11:\"new_bundled\";s:3:\"5.6\";s:15:\"partial_version\";s:0:\"\";s:9:\"new_files\";s:1:\"1\";}i:2;O:8:\"stdClass\":11:{s:8:\"response\";s:10:\"autoupdate\";s:8:\"download\";s:59:\"https://downloads.wordpress.org/release/wordpress-5.7.4.zip\";s:6:\"locale\";s:5:\"en_US\";s:8:\"packages\";O:8:\"stdClass\":5:{s:4:\"full\";s:59:\"https://downloads.wordpress.org/release/wordpress-5.7.4.zip\";s:10:\"no_content\";s:70:\"https://downloads.wordpress.org/release/wordpress-5.7.4-no-content.zip\";s:11:\"new_bundled\";s:71:\"https://downloads.wordpress.org/release/wordpress-5.7.4-new-bundled.zip\";s:7:\"partial\";s:69:\"https://downloads.wordpress.org/release/wordpress-5.7.4-partial-1.zip\";s:8:\"rollback\";s:70:\"https://downloads.wordpress.org/release/wordpress-5.7.4-rollback-1.zip\";}s:7:\"current\";s:5:\"5.7.4\";s:7:\"version\";s:5:\"5.7.4\";s:11:\"php_version\";s:6:\"5.6.20\";s:13:\"mysql_version\";s:3:\"5.0\";s:11:\"new_bundled\";s:3:\"5.6\";s:15:\"partial_version\";s:5:\"5.7.1\";s:9:\"new_files\";s:0:\"\";}}s:12:\"last_checked\";i:1639425511;s:15:\"version_checked\";s:5:\"5.7.1\";s:12:\"translations\";a:0:{}}','no');
INSERT INTO `wp_options` VALUES (130,'external_updates-slate-plugin','O:8:\"stdClass\":2:{s:9:\"lastCheck\";i:1639425513;s:14:\"checkedVersion\";s:6:\"1.0.70\";}','no');
INSERT INTO `wp_options` VALUES (131,'acf_version','5.9.2','yes');
INSERT INTO `wp_options` VALUES (139,'can_compress_scripts','1','no');
INSERT INTO `wp_options` VALUES (150,'current_theme','Slate Starter Theme','yes');
INSERT INTO `wp_options` VALUES (151,'theme_mods_slate','a:3:{i:0;b:0;s:18:\"nav_menu_locations\";a:4:{s:9:\"main-menu\";i:2;s:9:\"user-menu\";i:3;s:11:\"footer-menu\";i:4;s:12:\"footer-links\";i:5;}s:18:\"custom_css_post_id\";i:-1;}','yes');
INSERT INTO `wp_options` VALUES (152,'theme_switched','','yes');
INSERT INTO `wp_options` VALUES (153,'category_children','a:0:{}','yes');
INSERT INTO `wp_options` VALUES (156,'finished_updating_comment_type','1','yes');
INSERT INTO `wp_options` VALUES (164,'_transient_health-check-site-status-result','{\"good\":10,\"recommended\":8,\"critical\":1}','yes');
INSERT INTO `wp_options` VALUES (165,'options_address_0_address_street_address','8656 West Patrick Lane','no');
INSERT INTO `wp_options` VALUES (166,'_options_address_0_address_street_address','field_global_options_address_address_street_address','no');
INSERT INTO `wp_options` VALUES (167,'options_address_0_address_city',' Las Vegas','no');
INSERT INTO `wp_options` VALUES (168,'_options_address_0_address_city','field_global_options_address_address_city','no');
INSERT INTO `wp_options` VALUES (169,'options_address_0_address_state','NV','no');
INSERT INTO `wp_options` VALUES (170,'_options_address_0_address_state','field_global_options_address_address_state','no');
INSERT INTO `wp_options` VALUES (171,'options_address_0_address_zip_code','89148','no');
INSERT INTO `wp_options` VALUES (172,'_options_address_0_address_zip_code','field_global_options_address_address_zip_code','no');
INSERT INTO `wp_options` VALUES (173,'options_address_0_address','','no');
INSERT INTO `wp_options` VALUES (174,'_options_address_0_address','field_global_options_address_address','no');
INSERT INTO `wp_options` VALUES (175,'options_address','1','no');
INSERT INTO `wp_options` VALUES (176,'_options_address','field_global_options_address','no');
INSERT INTO `wp_options` VALUES (177,'options_phone_main','702-777-7100','no');
INSERT INTO `wp_options` VALUES (178,'_options_phone_main','field_global_options_phone_main','no');
INSERT INTO `wp_options` VALUES (179,'options_phone_sms','','no');
INSERT INTO `wp_options` VALUES (180,'_options_phone_sms','field_global_options_phone_sms','no');
INSERT INTO `wp_options` VALUES (181,'options_phone','','no');
INSERT INTO `wp_options` VALUES (182,'_options_phone','field_global_options_phone','no');
INSERT INTO `wp_options` VALUES (183,'options_add_logo','1','no');
INSERT INTO `wp_options` VALUES (184,'_options_add_logo','field_header_options_add_logo','no');
INSERT INTO `wp_options` VALUES (185,'options_logo','6','no');
INSERT INTO `wp_options` VALUES (186,'_options_logo','field_header_options_logo','no');
INSERT INTO `wp_options` VALUES (187,'options_add_menulogo','0','no');
INSERT INTO `wp_options` VALUES (188,'_options_add_menulogo','field_header_options_add_menulogo','no');
INSERT INTO `wp_options` VALUES (190,'options_logos_0_logo','','no');
INSERT INTO `wp_options` VALUES (191,'_options_logos_0_logo','field_footer_options_logos_logo','no');
INSERT INTO `wp_options` VALUES (192,'options_logos','1','no');
INSERT INTO `wp_options` VALUES (193,'_options_logos','field_footer_options_logos','no');
INSERT INTO `wp_options` VALUES (194,'options_socials','a:4:{i:0;s:9:\"instagram\";i:1;s:7:\"twitter\";i:2;s:7:\"youtube\";i:3;s:8:\"linkedin\";}','no');
INSERT INTO `wp_options` VALUES (195,'_options_socials','field_footer_options_socials','no');
INSERT INTO `wp_options` VALUES (196,'options_instagram_url','#','no');
INSERT INTO `wp_options` VALUES (197,'_options_instagram_url','field_footer_options_instagram_url','no');
INSERT INTO `wp_options` VALUES (198,'options_twitter_url','#','no');
INSERT INTO `wp_options` VALUES (199,'_options_twitter_url','field_footer_options_twitter_url','no');
INSERT INTO `wp_options` VALUES (200,'options_youtube_url','#','no');
INSERT INTO `wp_options` VALUES (201,'_options_youtube_url','field_footer_options_youtube_url','no');
INSERT INTO `wp_options` VALUES (202,'options_linkedin_url','#','no');
INSERT INTO `wp_options` VALUES (203,'_options_linkedin_url','field_footer_options_linkedin_url','no');
INSERT INTO `wp_options` VALUES (204,'options_legitscript_legitscript_check','0','no');
INSERT INTO `wp_options` VALUES (205,'_options_legitscript_legitscript_check','field_footer_options_legitscript_legitscript_check','no');
INSERT INTO `wp_options` VALUES (206,'options_legitscript','','no');
INSERT INTO `wp_options` VALUES (207,'_options_legitscript','field_footer_options_legitscript','no');
INSERT INTO `wp_options` VALUES (208,'options_bbbscript_bbbscript_check','0','no');
INSERT INTO `wp_options` VALUES (209,'_options_bbbscript_bbbscript_check','field_footer_options_bbbscript_bbbscript_check','no');
INSERT INTO `wp_options` VALUES (210,'options_bbbscript','','no');
INSERT INTO `wp_options` VALUES (211,'_options_bbbscript','field_footer_options_bbbscript','no');
INSERT INTO `wp_options` VALUES (212,'options_jointcommission_jc_check','0','no');
INSERT INTO `wp_options` VALUES (213,'_options_jointcommission_jc_check','field_footer_options_jointcommission_jc_check','no');
INSERT INTO `wp_options` VALUES (214,'options_jointcommission','','no');
INSERT INTO `wp_options` VALUES (215,'_options_jointcommission','field_footer_options_jointcommission','no');
INSERT INTO `wp_options` VALUES (216,'options_customlogos_customlogos_repeater_0_accreditation_logo','','no');
INSERT INTO `wp_options` VALUES (217,'_options_customlogos_customlogos_repeater_0_accreditation_logo','field_footer_options_customlogos_customlogos_repeater_accreditation_logo','no');
INSERT INTO `wp_options` VALUES (218,'options_customlogos_customlogos_repeater_0_accreditation_logo_alt','','no');
INSERT INTO `wp_options` VALUES (219,'_options_customlogos_customlogos_repeater_0_accreditation_logo_alt','field_footer_options_customlogos_customlogos_repeater_accreditation_logo_alt','no');
INSERT INTO `wp_options` VALUES (220,'options_customlogos_customlogos_repeater','1','no');
INSERT INTO `wp_options` VALUES (221,'_options_customlogos_customlogos_repeater','field_footer_options_customlogos_customlogos_repeater','no');
INSERT INTO `wp_options` VALUES (222,'options_customlogos','','no');
INSERT INTO `wp_options` VALUES (223,'_options_customlogos','field_footer_options_customlogos','no');
INSERT INTO `wp_options` VALUES (226,'recently_activated','a:0:{}','yes');
INSERT INTO `wp_options` VALUES (227,'gf_db_version','2.4.22.5','no');
INSERT INTO `wp_options` VALUES (228,'rg_form_version','2.4.22.5','no');
INSERT INTO `wp_options` VALUES (229,'gform_enable_background_updates','1','yes');
INSERT INTO `wp_options` VALUES (230,'gform_pending_installation','','yes');
INSERT INTO `wp_options` VALUES (231,'widget_gform_widget','a:1:{s:12:\"_multiwidget\";i:1;}','yes');
INSERT INTO `wp_options` VALUES (232,'gravityformsaddon_gravityformswebapi_version','1.0','yes');
INSERT INTO `wp_options` VALUES (234,'gform_version_info','a:8:{s:12:\"is_valid_key\";b:0;s:6:\"reason\";s:7:\"invalid\";s:15:\"expiration_time\";i:0;s:7:\"version\";s:6:\"2.5.15\";s:3:\"url\";s:0:\"\";s:9:\"offerings\";a:66:{s:12:\"gravityforms\";a:3:{s:12:\"is_available\";b:0;s:7:\"version\";s:6:\"2.5.15\";s:14:\"version_latest\";s:8:\"2.5.15.2\";}s:17:\"gravityforms-beta\";a:3:{s:12:\"is_available\";b:0;s:7:\"version\";s:8:\"2.5-rc-3\";s:14:\"version_latest\";s:8:\"2.5-rc-3\";}s:21:\"gravityforms2checkout\";a:3:{s:12:\"is_available\";b:0;s:7:\"version\";s:3:\"2.0\";s:14:\"version_latest\";s:5:\"2.0.1\";}s:26:\"gravityformsactivecampaign\";a:3:{s:12:\"is_available\";b:0;s:7:\"version\";s:3:\"1.9\";s:14:\"version_latest\";s:5:\"1.9.1\";}s:32:\"gravityformsadvancedpostcreation\";a:3:{s:12:\"is_available\";b:0;s:7:\"version\";s:3:\"1.0\";s:14:\"version_latest\";s:3:\"1.1\";}s:20:\"gravityformsagilecrm\";a:3:{s:12:\"is_available\";b:0;s:7:\"version\";s:3:\"1.4\";s:14:\"version_latest\";s:3:\"1.4\";}s:19:\"gravityformsakismet\";a:3:{s:12:\"is_available\";b:0;s:7:\"version\";s:3:\"1.0\";s:14:\"version_latest\";s:3:\"1.0\";}s:24:\"gravityformsauthorizenet\";a:3:{s:12:\"is_available\";b:0;s:7:\"version\";s:4:\"2.11\";s:14:\"version_latest\";s:4:\"2.11\";}s:18:\"gravityformsaweber\";a:3:{s:12:\"is_available\";b:0;s:7:\"version\";s:4:\"2.11\";s:14:\"version_latest\";s:4:\"2.11\";}s:21:\"gravityformsbatchbook\";a:3:{s:12:\"is_available\";b:0;s:7:\"version\";s:3:\"1.3\";s:14:\"version_latest\";s:3:\"1.3\";}s:18:\"gravityformsbreeze\";a:3:{s:12:\"is_available\";b:0;s:7:\"version\";s:3:\"1.5\";s:14:\"version_latest\";s:3:\"1.5\";}s:27:\"gravityformscampaignmonitor\";a:3:{s:12:\"is_available\";b:0;s:7:\"version\";s:3:\"3.9\";s:14:\"version_latest\";s:3:\"3.9\";}s:20:\"gravityformscampfire\";a:3:{s:12:\"is_available\";b:0;s:7:\"version\";s:3:\"1.1\";s:14:\"version_latest\";s:5:\"1.2.2\";}s:22:\"gravityformscapsulecrm\";a:3:{s:12:\"is_available\";b:0;s:7:\"version\";s:3:\"1.6\";s:14:\"version_latest\";s:3:\"1.6\";}s:26:\"gravityformschainedselects\";a:3:{s:12:\"is_available\";b:0;s:7:\"version\";s:3:\"1.5\";s:14:\"version_latest\";s:3:\"1.5\";}s:31:\"gravityformschainedselects-beta\";a:3:{s:12:\"is_available\";b:0;s:7:\"version\";s:3:\"1.0\";s:14:\"version_latest\";s:12:\"1.0-beta-1.3\";}s:23:\"gravityformscleverreach\";a:3:{s:12:\"is_available\";b:0;s:7:\"version\";s:3:\"1.7\";s:14:\"version_latest\";s:3:\"1.7\";}s:15:\"gravityformscli\";a:3:{s:12:\"is_available\";b:0;s:7:\"version\";s:0:\"\";s:14:\"version_latest\";s:3:\"1.4\";}s:27:\"gravityformsconstantcontact\";a:3:{s:12:\"is_available\";b:0;s:7:\"version\";s:3:\"1.5\";s:14:\"version_latest\";s:3:\"1.5\";}s:19:\"gravityformscoupons\";a:3:{s:12:\"is_available\";b:0;s:7:\"version\";s:3:\"3.0\";s:14:\"version_latest\";s:3:\"3.0\";}s:17:\"gravityformsdebug\";a:3:{s:12:\"is_available\";b:0;s:7:\"version\";s:0:\"\";s:14:\"version_latest\";s:10:\"1.0.beta12\";}s:19:\"gravityformsdropbox\";a:3:{s:12:\"is_available\";b:0;s:7:\"version\";s:3:\"3.0\";s:14:\"version_latest\";s:3:\"3.0\";}s:24:\"gravityformsdropbox-beta\";a:3:{s:12:\"is_available\";b:0;s:7:\"version\";s:3:\"2.0\";s:14:\"version_latest\";s:12:\"2.0-beta-1.1\";}s:24:\"gravityformsemailoctopus\";a:3:{s:12:\"is_available\";b:0;s:7:\"version\";s:3:\"1.2\";s:14:\"version_latest\";s:3:\"1.2\";}s:16:\"gravityformsemma\";a:3:{s:12:\"is_available\";b:0;s:7:\"version\";s:3:\"1.5\";s:14:\"version_latest\";s:3:\"1.5\";}s:22:\"gravityformsfreshbooks\";a:3:{s:12:\"is_available\";b:0;s:7:\"version\";s:3:\"2.8\";s:14:\"version_latest\";s:3:\"2.8\";}s:23:\"gravityformsgetresponse\";a:3:{s:12:\"is_available\";b:0;s:7:\"version\";s:3:\"1.6\";s:14:\"version_latest\";s:3:\"1.6\";}s:21:\"gravityformsgutenberg\";a:3:{s:12:\"is_available\";b:0;s:7:\"version\";s:10:\"1.0-rc-1.4\";s:14:\"version_latest\";s:10:\"1.0-rc-1.5\";}s:21:\"gravityformshelpscout\";a:3:{s:12:\"is_available\";b:0;s:7:\"version\";s:3:\"2.0\";s:14:\"version_latest\";s:3:\"2.0\";}s:20:\"gravityformshighrise\";a:3:{s:12:\"is_available\";b:0;s:7:\"version\";s:3:\"1.3\";s:14:\"version_latest\";s:3:\"1.3\";}s:19:\"gravityformshipchat\";a:3:{s:12:\"is_available\";b:0;s:7:\"version\";s:3:\"1.2\";s:14:\"version_latest\";s:3:\"1.2\";}s:19:\"gravityformshubspot\";a:3:{s:12:\"is_available\";b:0;s:7:\"version\";s:3:\"1.7\";s:14:\"version_latest\";s:3:\"1.7\";}s:20:\"gravityformsicontact\";a:3:{s:12:\"is_available\";b:0;s:7:\"version\";s:3:\"1.5\";s:14:\"version_latest\";s:3:\"1.5\";}s:19:\"gravityformslogging\";a:3:{s:12:\"is_available\";b:0;s:7:\"version\";s:3:\"1.3\";s:14:\"version_latest\";s:5:\"1.3.1\";}s:19:\"gravityformsmadmimi\";a:3:{s:12:\"is_available\";b:0;s:7:\"version\";s:3:\"1.4\";s:14:\"version_latest\";s:3:\"1.4\";}s:21:\"gravityformsmailchimp\";a:3:{s:12:\"is_available\";b:0;s:7:\"version\";s:3:\"5.0\";s:14:\"version_latest\";s:3:\"5.0\";}s:19:\"gravityformsmailgun\";a:3:{s:12:\"is_available\";b:0;s:7:\"version\";s:3:\"1.3\";s:14:\"version_latest\";s:5:\"1.3.1\";}s:18:\"gravityformsmollie\";a:3:{s:12:\"is_available\";b:0;s:7:\"version\";s:3:\"1.2\";s:14:\"version_latest\";s:3:\"1.2\";}s:26:\"gravityformspartialentries\";a:3:{s:12:\"is_available\";b:0;s:7:\"version\";s:3:\"1.6\";s:14:\"version_latest\";s:5:\"1.6.1\";}s:18:\"gravityformspaypal\";a:3:{s:12:\"is_available\";b:0;s:7:\"version\";s:3:\"3.5\";s:14:\"version_latest\";s:3:\"3.5\";}s:33:\"gravityformspaypalexpresscheckout\";a:3:{s:12:\"is_available\";b:0;s:7:\"version\";s:0:\"\";s:14:\"version_latest\";N;}s:29:\"gravityformspaypalpaymentspro\";a:3:{s:12:\"is_available\";b:0;s:7:\"version\";s:3:\"2.7\";s:14:\"version_latest\";s:3:\"2.7\";}s:21:\"gravityformspaypalpro\";a:3:{s:12:\"is_available\";b:0;s:7:\"version\";s:5:\"1.8.1\";s:14:\"version_latest\";s:5:\"1.8.4\";}s:20:\"gravityformspicatcha\";a:3:{s:12:\"is_available\";b:0;s:7:\"version\";s:3:\"2.0\";s:14:\"version_latest\";s:3:\"2.0\";}s:16:\"gravityformspipe\";a:3:{s:12:\"is_available\";b:0;s:7:\"version\";s:3:\"1.2\";s:14:\"version_latest\";s:3:\"1.3\";}s:17:\"gravityformspolls\";a:3:{s:12:\"is_available\";b:0;s:7:\"version\";s:3:\"3.9\";s:14:\"version_latest\";s:3:\"3.9\";}s:20:\"gravityformspostmark\";a:3:{s:12:\"is_available\";b:0;s:7:\"version\";s:3:\"1.3\";s:14:\"version_latest\";s:3:\"1.3\";}s:16:\"gravityformsppcp\";a:3:{s:12:\"is_available\";b:0;s:7:\"version\";s:3:\"2.2\";s:14:\"version_latest\";s:5:\"2.2.1\";}s:21:\"gravityformsppcp-beta\";a:3:{s:12:\"is_available\";b:0;s:7:\"version\";s:10:\"2.0-beta-3\";s:14:\"version_latest\";s:10:\"2.0-beta-3\";}s:24:\"gravityformsppcp-release\";a:3:{s:12:\"is_available\";b:0;s:7:\"version\";s:3:\"2.0\";s:14:\"version_latest\";s:5:\"2.0.2\";}s:16:\"gravityformsquiz\";a:3:{s:12:\"is_available\";b:0;s:7:\"version\";s:3:\"3.8\";s:14:\"version_latest\";s:3:\"3.8\";}s:21:\"gravityformsrecaptcha\";a:3:{s:12:\"is_available\";b:0;s:7:\"version\";s:3:\"1.1\";s:14:\"version_latest\";s:3:\"1.1\";}s:19:\"gravityformsrestapi\";a:3:{s:12:\"is_available\";b:0;s:7:\"version\";s:10:\"2.0-beta-2\";s:14:\"version_latest\";s:10:\"2.0-beta-2\";}s:20:\"gravityformssendgrid\";a:3:{s:12:\"is_available\";b:0;s:7:\"version\";s:3:\"1.5\";s:14:\"version_latest\";s:3:\"1.5\";}s:21:\"gravityformssignature\";a:3:{s:12:\"is_available\";b:0;s:7:\"version\";s:3:\"4.1\";s:14:\"version_latest\";s:3:\"4.1\";}s:17:\"gravityformsslack\";a:3:{s:12:\"is_available\";b:0;s:7:\"version\";s:3:\"2.0\";s:14:\"version_latest\";s:3:\"2.0\";}s:18:\"gravityformssquare\";a:3:{s:12:\"is_available\";b:0;s:7:\"version\";s:3:\"1.5\";s:14:\"version_latest\";s:5:\"1.5.3\";}s:18:\"gravityformsstripe\";a:3:{s:12:\"is_available\";b:0;s:7:\"version\";s:3:\"4.1\";s:14:\"version_latest\";s:5:\"4.1.1\";}s:18:\"gravityformssurvey\";a:3:{s:12:\"is_available\";b:0;s:7:\"version\";s:3:\"3.7\";s:14:\"version_latest\";s:3:\"3.7\";}s:18:\"gravityformstrello\";a:3:{s:12:\"is_available\";b:0;s:7:\"version\";s:3:\"2.0\";s:14:\"version_latest\";s:3:\"2.0\";}s:18:\"gravityformstwilio\";a:3:{s:12:\"is_available\";b:0;s:7:\"version\";s:3:\"2.8\";s:14:\"version_latest\";s:3:\"2.8\";}s:28:\"gravityformsuserregistration\";a:3:{s:12:\"is_available\";b:0;s:7:\"version\";s:3:\"4.9\";s:14:\"version_latest\";s:3:\"4.9\";}s:20:\"gravityformswebhooks\";a:3:{s:12:\"is_available\";b:0;s:7:\"version\";s:3:\"1.5\";s:14:\"version_latest\";s:3:\"1.5\";}s:18:\"gravityformszapier\";a:3:{s:12:\"is_available\";b:0;s:7:\"version\";s:3:\"4.1\";s:14:\"version_latest\";s:5:\"4.1.1\";}s:23:\"gravityformszapier-beta\";a:3:{s:12:\"is_available\";b:0;s:7:\"version\";s:10:\"4.0-beta-2\";s:14:\"version_latest\";s:10:\"4.0-beta-2\";}s:19:\"gravityformszohocrm\";a:3:{s:12:\"is_available\";b:0;s:7:\"version\";s:3:\"2.0\";s:14:\"version_latest\";s:3:\"2.0\";}}s:14:\"version_latest\";s:8:\"2.5.15.2\";s:9:\"timestamp\";i:1639425505;}','no');
INSERT INTO `wp_options` VALUES (235,'redirection_options','a:31:{s:7:\"support\";b:0;s:5:\"token\";s:32:\"5cc1287bcb6233bbf7983f62e2330dc6\";s:12:\"monitor_post\";i:0;s:13:\"monitor_types\";a:0:{}s:19:\"associated_redirect\";s:0:\"\";s:11:\"auto_target\";s:0:\"\";s:15:\"expire_redirect\";i:7;s:10:\"expire_404\";i:7;s:12:\"log_external\";b:0;s:10:\"log_header\";b:0;s:10:\"track_hits\";b:1;s:7:\"modules\";a:0:{}s:10:\"newsletter\";b:0;s:14:\"redirect_cache\";i:1;s:10:\"ip_logging\";i:1;s:13:\"last_group_id\";i:0;s:8:\"rest_api\";i:0;s:5:\"https\";b:0;s:7:\"headers\";a:0:{}s:8:\"database\";s:0:\"\";s:8:\"relocate\";s:0:\"\";s:16:\"preferred_domain\";s:0:\"\";s:7:\"aliases\";a:0:{}s:10:\"permalinks\";a:0:{}s:9:\"cache_key\";i:0;s:13:\"plugin_update\";s:6:\"prompt\";s:13:\"update_notice\";i:0;s:10:\"flag_query\";s:5:\"exact\";s:9:\"flag_case\";b:0;s:13:\"flag_trailing\";b:0;s:10:\"flag_regex\";b:0;}','yes');
INSERT INTO `wp_options` VALUES (236,'relevanssi_admin_search','off','yes');
INSERT INTO `wp_options` VALUES (237,'relevanssi_bg_col','#ffaf75','yes');
INSERT INTO `wp_options` VALUES (238,'relevanssi_cat','0','yes');
INSERT INTO `wp_options` VALUES (239,'relevanssi_class','relevanssi-query-term','yes');
INSERT INTO `wp_options` VALUES (240,'relevanssi_comment_boost','0.75','yes');
INSERT INTO `wp_options` VALUES (241,'relevanssi_content_boost','1','yes');
INSERT INTO `wp_options` VALUES (242,'relevanssi_css','text-decoration: underline; text-color: #ff0000','yes');
INSERT INTO `wp_options` VALUES (243,'relevanssi_db_version','6','yes');
INSERT INTO `wp_options` VALUES (244,'relevanssi_default_orderby','relevance','yes');
INSERT INTO `wp_options` VALUES (245,'relevanssi_disable_or_fallback','off','yes');
INSERT INTO `wp_options` VALUES (246,'relevanssi_exact_match_bonus','on','yes');
INSERT INTO `wp_options` VALUES (247,'relevanssi_excat','0','yes');
INSERT INTO `wp_options` VALUES (248,'relevanssi_excerpt_allowable_tags','','yes');
INSERT INTO `wp_options` VALUES (249,'relevanssi_excerpt_custom_fields','off','yes');
INSERT INTO `wp_options` VALUES (250,'relevanssi_excerpt_length','30','yes');
INSERT INTO `wp_options` VALUES (251,'relevanssi_excerpt_type','words','yes');
INSERT INTO `wp_options` VALUES (252,'relevanssi_excerpts','on','yes');
INSERT INTO `wp_options` VALUES (253,'relevanssi_exclude_posts','','yes');
INSERT INTO `wp_options` VALUES (254,'relevanssi_expand_highlights','off','yes');
INSERT INTO `wp_options` VALUES (255,'relevanssi_expand_shortcodes','on','yes');
INSERT INTO `wp_options` VALUES (256,'relevanssi_extag','0','yes');
INSERT INTO `wp_options` VALUES (257,'relevanssi_fuzzy','sometimes','yes');
INSERT INTO `wp_options` VALUES (258,'relevanssi_highlight','strong','yes');
INSERT INTO `wp_options` VALUES (259,'relevanssi_highlight_comments','off','yes');
INSERT INTO `wp_options` VALUES (260,'relevanssi_highlight_docs','off','yes');
INSERT INTO `wp_options` VALUES (261,'relevanssi_hilite_title','','yes');
INSERT INTO `wp_options` VALUES (262,'relevanssi_implicit_operator','OR','yes');
INSERT INTO `wp_options` VALUES (263,'relevanssi_index_author','','yes');
INSERT INTO `wp_options` VALUES (264,'relevanssi_index_comments','none','yes');
INSERT INTO `wp_options` VALUES (265,'relevanssi_index_excerpt','off','yes');
INSERT INTO `wp_options` VALUES (266,'relevanssi_index_fields','','yes');
INSERT INTO `wp_options` VALUES (267,'relevanssi_index_image_files','on','yes');
INSERT INTO `wp_options` VALUES (268,'relevanssi_index_post_types','a:2:{i:0;s:4:\"post\";i:1;s:4:\"page\";}','yes');
INSERT INTO `wp_options` VALUES (269,'relevanssi_index_taxonomies_list','a:0:{}','yes');
INSERT INTO `wp_options` VALUES (270,'relevanssi_indexed','done','yes');
INSERT INTO `wp_options` VALUES (271,'relevanssi_log_queries','off','yes');
INSERT INTO `wp_options` VALUES (272,'relevanssi_log_queries_with_ip','off','yes');
INSERT INTO `wp_options` VALUES (273,'relevanssi_min_word_length','3','yes');
INSERT INTO `wp_options` VALUES (274,'relevanssi_omit_from_logs','','yes');
INSERT INTO `wp_options` VALUES (275,'relevanssi_polylang_all_languages','off','yes');
INSERT INTO `wp_options` VALUES (276,'relevanssi_punctuation','a:3:{s:6:\"quotes\";s:7:\"replace\";s:7:\"hyphens\";s:7:\"replace\";s:10:\"ampersands\";s:7:\"replace\";}','yes');
INSERT INTO `wp_options` VALUES (277,'relevanssi_respect_exclude','on','yes');
INSERT INTO `wp_options` VALUES (278,'relevanssi_seo_noindex','on','yes');
INSERT INTO `wp_options` VALUES (279,'relevanssi_show_matches','','yes');
INSERT INTO `wp_options` VALUES (280,'relevanssi_show_matches_text','(Search hits: %body% in body, %title% in title, %categories% in categories, %tags% in tags, %taxonomies% in other taxonomies, %comments% in comments. Score: %score%)','yes');
INSERT INTO `wp_options` VALUES (281,'relevanssi_stopwords','a:1:{s:5:\"en_US\";s:1908:\"a,about,above,across,after,afterwards,again,against,all,almost,alone,along,already,also,although,always,am,among,amongst,amoungst,amount,an,and,another,any,anyhow,anyone,anything,anyway,anywhere,are,around,as,at,back,be,became,because,become,becomes,becoming,been,before,beforehand,behind,being,below,beside,besides,between,beyond,bill,both,bottom,but,by,call,can,cannot,cant,co,con,could,couldnt,cry,de,describe,detail,do,done,down,due,during,each,eg,eight,either,eleven,else,elsewhere,empty,enough,etc,even,ever,every,everyone,everything,everywhere,except,few,fifteen,fifty,fill,find,fire,first,five,for,former,formerly,forty,found,four,from,front,full,further,get,give,go,had,has,hasnt,have,he,hence,her,here,hereafter,hereby,herein,hereupon,hers,herself,him,himself,his,how,however,hundred,ie,if,in,inc,indeed,interest,into,is,it,its,itself,keep,last,latter,latterly,least,less,ltd,made,many,may,me,meanwhile,might,mill,mine,more,moreover,most,mostly,move,much,must,my,myself,name,namely,neither,never,nevertheless,next,nine,no,nobody,none,noone,nor,not,nothing,now,nowhere,of,off,often,on,once,one,only,onto,or,other,others,otherwise,our,ours,ourselves,out,over,own,part,per,perhaps,please,put,rather,re,same,see,seem,seemed,seeming,seems,serious,several,she,should,show,side,since,sincere,six,sixty,so,some,somehow,someone,something,sometime,sometimes,somewhere,still,such,system,take,ten,than,that,the,their,them,themselves,then,thence,there,thereafter,thereby,therefore,therein,thereupon,these,they,thickv,thin,third,this,those,though,three,through,throughout,thru,thus,to,together,too,top,toward,towards,twelve,twenty,two,un,under,until,up,upon,us,very,via,was,we,well,were,what,whatever,when,whence,whenever,where,whereafter,whereas,whereby,wherein,whereupon,wherever,whether,which,while,whither,who,whoever,whole,whom,whose,why,will,with,within,without,would,yet,you,your,yours,yourself,yourselves\";}','yes');
INSERT INTO `wp_options` VALUES (282,'relevanssi_synonyms','a:0:{}','yes');
INSERT INTO `wp_options` VALUES (283,'relevanssi_throttle','on','yes');
INSERT INTO `wp_options` VALUES (284,'relevanssi_throttle_limit','500','yes');
INSERT INTO `wp_options` VALUES (285,'relevanssi_title_boost','5','yes');
INSERT INTO `wp_options` VALUES (286,'relevanssi_txt_col','#ff0000','yes');
INSERT INTO `wp_options` VALUES (287,'relevanssi_wpml_only_current','on','yes');
INSERT INTO `wp_options` VALUES (289,'relevanssi_doc_count','7','yes');
INSERT INTO `wp_options` VALUES (290,'relevanssi_terms_count','527','no');
INSERT INTO `wp_options` VALUES (297,'nav_menu_options','a:2:{i:0;b:0;s:8:\"auto_add\";a:0:{}}','yes');
INSERT INTO `wp_options` VALUES (313,'recovery_mode_email_last_sent','1635262087','yes');
INSERT INTO `wp_options` VALUES (351,'options_address_0_address_google_map','a:15:{s:7:\"address\";s:42:\"8658 West Patrick Lane, Las Vegas, NV, USA\";s:3:\"lat\";d:36.077801099999988;s:3:\"lng\";d:-115.28065650000001;s:4:\"zoom\";i:14;s:8:\"place_id\";s:171:\"Eis4NjU4IFcgUGF0cmljayBMbiwgTGFzIFZlZ2FzLCBOViA4OTE0OCwgVVNBIlESTwo0CjIJc0R0MoDHyIAR8inbRmo9jOwaHgsQ7sHuoQEaFAoSCYnbphtbuMiAET8_-7H7B9caDBDSQyoUChIJNbqpgaXHyIARL1EaOeJJEMo\";s:4:\"name\";s:17:\"8658 W Patrick Ln\";s:13:\"street_number\";s:4:\"8658\";s:11:\"street_name\";s:17:\"West Patrick Lane\";s:17:\"street_name_short\";s:12:\"W Patrick Ln\";s:4:\"city\";s:9:\"Las Vegas\";s:5:\"state\";s:6:\"Nevada\";s:11:\"state_short\";s:2:\"NV\";s:9:\"post_code\";s:5:\"89148\";s:7:\"country\";s:13:\"United States\";s:13:\"country_short\";s:2:\"US\";}','no');
INSERT INTO `wp_options` VALUES (352,'_options_address_0_address_google_map','field_global_options_address_address_google_map','no');
INSERT INTO `wp_options` VALUES (411,'options_sidebar_0_item_title','Careers','no');
INSERT INTO `wp_options` VALUES (412,'_options_sidebar_0_item_title','field_global_options_sidebar_item_title','no');
INSERT INTO `wp_options` VALUES (413,'options_sidebar_0_item_content','[button class=\"simple\" url=\"#\"]Work at Valley Health Specialty Hospital[/button]','no');
INSERT INTO `wp_options` VALUES (414,'_options_sidebar_0_item_content','field_global_options_sidebar_item_content','no');
INSERT INTO `wp_options` VALUES (415,'options_sidebar_0_item','','no');
INSERT INTO `wp_options` VALUES (416,'_options_sidebar_0_item','field_global_options_sidebar_item','no');
INSERT INTO `wp_options` VALUES (417,'options_sidebar_1_item_title','Contact Us','no');
INSERT INTO `wp_options` VALUES (418,'_options_sidebar_1_item_title','field_global_options_sidebar_item_title','no');
INSERT INTO `wp_options` VALUES (419,'options_sidebar_1_item_content','[button class=\"phone-icon\" url=\"tel:702-777-7100 \"]702-777-7100 [/button]\r\n[button class=\"email-icon\" url=\"#\"]Send us an email[/button]','no');
INSERT INTO `wp_options` VALUES (420,'_options_sidebar_1_item_content','field_global_options_sidebar_item_content','no');
INSERT INTO `wp_options` VALUES (421,'options_sidebar_1_item','','no');
INSERT INTO `wp_options` VALUES (422,'_options_sidebar_1_item','field_global_options_sidebar_item','no');
INSERT INTO `wp_options` VALUES (423,'options_sidebar_2_item_title','','no');
INSERT INTO `wp_options` VALUES (424,'_options_sidebar_2_item_title','field_global_options_sidebar_item_title','no');
INSERT INTO `wp_options` VALUES (425,'options_sidebar_2_item_content','[button class=\"simple\" url=\"https://www.springvalleyhospital.com/\" target=\"_blank\"]Visit Spring Valley Hospital[/button]','no');
INSERT INTO `wp_options` VALUES (426,'_options_sidebar_2_item_content','field_global_options_sidebar_item_content','no');
INSERT INTO `wp_options` VALUES (427,'options_sidebar_2_item','','no');
INSERT INTO `wp_options` VALUES (428,'_options_sidebar_2_item','field_global_options_sidebar_item','no');
INSERT INTO `wp_options` VALUES (429,'options_sidebar','3','no');
INSERT INTO `wp_options` VALUES (430,'_options_sidebar','field_global_options_sidebar','no');
INSERT INTO `wp_options` VALUES (544,'options_add_translate','1','no');
INSERT INTO `wp_options` VALUES (545,'_options_add_translate','field_header_options_add_translate','no');
INSERT INTO `wp_options` VALUES (546,'options_translate_script','<script>\r\n// parseUri 1.2.2\r\n// (c) Steven Levithan <stevenlevithan.com>\r\n// MIT License\r\nfunction parseUri (str) {\r\n    var    o   = parseUri.options,\r\n        m   = o.parser[o.strictMode ? \"strict\" : \"loose\"].exec(str),\r\n        uri = {},\r\n        i   = 14;\r\n    while (i--) uri[o.key[i]] = m[i] || \"\";\r\n    uri[o.q.name] = {};\r\n    uri[o.key[12]].replace(o.q.parser, function ($0, $1, $2) {\r\n        if ($1) uri[o.q.name][$1] = $2;\r\n    });\r\n    return uri;\r\n};\r\nparseUri.options = {\r\n    strictMode: true,\r\n    key: [\"source\",\"protocol\",\"authority\",\"userInfo\",\"user\",\"password\",\"host\",\"port\",\"relative\",\"path\",\"directory\",\"file\",\"query\",\"anchor\"],\r\n    q:   {\r\n        name:   \"queryKey\",\r\n        parser: /(?:^|&)([^&=]*)=?([^&]*)/g\r\n    },\r\n    parser: {\r\n        strict: /^(?:([^:\\/?#]+):)?(?:\\/\\/((?:(([^:@]*)(?::([^:@]*))?)?@)?([^:\\/?#]*)(?::(\\d*))?))?((((?:[^?#\\/]*\\/)*)([^?#]*))(?:\\?([^#]*))?(?:#(.*))?)/,\r\n        loose:  /^(?:(?![^:@]+:[^:@\\/]*@)([^:\\/?#.]+):)?(?:\\/\\/)?((?:(([^:@]*)(?::([^:@]*))?)?@)?([^:\\/?#]*)(?::(\\d*))?)(((\\/(?:[^?#](?![^?#\\/]*\\.[^?#\\/.]+(?:[?#]|$)))*\\/?)?([^?#\\/]*))(?:\\?([^#]*))?(?:#(.*))?)/\r\n    }\r\n};\r\nfunction gt_init() {\r\n  var uri = parseUri(document.location.href);\r\n  var host = uri.host;\r\n  var path = uri.path;\r\n  var transButton = document.getElementById(\"sl_translate\");\r\n  console.log(\"URL: \"+host);\r\n  if ( host != null ) {\r\n    let parts = host.split(\'.\');\r\n    let sub = parts[0].toString();\r\n    console.log(\"lang: \"+sub);\r\n    if ( sub == \"es\" ) {\r\n      transButton.innerHTML = \'<a href=\"https://\'+parts[1]+\'.\'+parts[2]+path+\'\" style=\"text-transform: uppercase;\">English</a>\';\r\n    }\r\n    if ( sub == \"valleyhealth\" ) {\r\n      transButton.innerHTML = \'<a href=\"https://es.\'+parts[0]+\'.\'+parts[1]+path+\'\" style=\"text-transform: uppercase;\">Español</a>\';\r\n    }\r\n  }\r\n  console.log(\"translation service loaded!\");\r\n}\r\nwindow.onload = gt_init;\r\n</script>','no');
INSERT INTO `wp_options` VALUES (547,'_options_translate_script','field_header_options_translate_script','no');
INSERT INTO `wp_options` VALUES (574,'rg_gforms_key','6746784010efdc56d73b2f13604f4daf','yes');
INSERT INTO `wp_options` VALUES (575,'rg_gforms_enable_akismet','1','yes');
INSERT INTO `wp_options` VALUES (576,'rg_gforms_currency','USD','yes');
INSERT INTO `wp_options` VALUES (577,'gform_enable_toolbar_menu','1','yes');
INSERT INTO `wp_options` VALUES (684,'options_footer_wysiwyg','<p>Spring Valley Hospital Medical Center is owned and operated by a subsidiary of Universal Health Services, Inc. (UHS), a King of Prussia, PA-based company, that is one of the largest healthcare management companies in the nation.</p>\r\n\r\n		<p>The information on this website is provided as general health guidelines and may not be applicable to your particular health condition. Your individual health status and any required medical treatments can only be properly addressed by a professional healthcare provider of your choice. Remember: There is no adequate substitution for a personal consultation with your physician. Neither Spring Valley Hospital Medical Center, or any of their affiliates, nor any contributors shall have any liability for the content or any errors or omissions in the information provided by this website.</p>\r\n\r\n		<p>The information, content and artwork provided by this website is intended for non-commercial use by the reader. The reader is permitted to make one copy of the information displayed for his/her own non-commercial use. The making of additional copies is prohibited.</p>','no');
INSERT INTO `wp_options` VALUES (685,'_options_footer_wysiwyg','field_footer_options_footer_wysiwyg','no');
INSERT INTO `wp_options` VALUES (774,'_site_transient_timeout_theme_roots','1639427305','no');
INSERT INTO `wp_options` VALUES (775,'_site_transient_theme_roots','a:4:{s:5:\"slate\";s:7:\"/themes\";s:14:\"twentynineteen\";s:7:\"/themes\";s:12:\"twentytwenty\";s:7:\"/themes\";s:15:\"twentytwentyone\";s:7:\"/themes\";}','no');
INSERT INTO `wp_options` VALUES (776,'_site_transient_timeout_php_check_472f71d2a071d463a95f84346288dc89','1640030306','no');
INSERT INTO `wp_options` VALUES (777,'_site_transient_php_check_472f71d2a071d463a95f84346288dc89','a:5:{s:19:\"recommended_version\";s:3:\"7.4\";s:15:\"minimum_version\";s:6:\"5.6.20\";s:12:\"is_supported\";b:0;s:9:\"is_secure\";b:1;s:13:\"is_acceptable\";b:1;}','no');
INSERT INTO `wp_options` VALUES (779,'_site_transient_update_themes','O:8:\"stdClass\":5:{s:12:\"last_checked\";i:1639425512;s:7:\"checked\";a:4:{s:5:\"slate\";s:0:\"\";s:14:\"twentynineteen\";s:3:\"2.0\";s:12:\"twentytwenty\";s:3:\"1.7\";s:15:\"twentytwentyone\";s:3:\"1.3\";}s:8:\"response\";a:3:{s:14:\"twentynineteen\";a:6:{s:5:\"theme\";s:14:\"twentynineteen\";s:11:\"new_version\";s:3:\"2.1\";s:3:\"url\";s:44:\"https://wordpress.org/themes/twentynineteen/\";s:7:\"package\";s:60:\"https://downloads.wordpress.org/theme/twentynineteen.2.1.zip\";s:8:\"requires\";s:5:\"4.9.6\";s:12:\"requires_php\";s:5:\"5.2.4\";}s:12:\"twentytwenty\";a:6:{s:5:\"theme\";s:12:\"twentytwenty\";s:11:\"new_version\";s:3:\"1.8\";s:3:\"url\";s:42:\"https://wordpress.org/themes/twentytwenty/\";s:7:\"package\";s:58:\"https://downloads.wordpress.org/theme/twentytwenty.1.8.zip\";s:8:\"requires\";s:3:\"4.7\";s:12:\"requires_php\";s:5:\"5.2.4\";}s:15:\"twentytwentyone\";a:6:{s:5:\"theme\";s:15:\"twentytwentyone\";s:11:\"new_version\";s:3:\"1.4\";s:3:\"url\";s:45:\"https://wordpress.org/themes/twentytwentyone/\";s:7:\"package\";s:61:\"https://downloads.wordpress.org/theme/twentytwentyone.1.4.zip\";s:8:\"requires\";s:3:\"5.3\";s:12:\"requires_php\";s:3:\"5.6\";}}s:9:\"no_update\";a:0:{}s:12:\"translations\";a:0:{}}','no');
INSERT INTO `wp_options` VALUES (780,'_site_transient_update_plugins','O:8:\"stdClass\":5:{s:12:\"last_checked\";i:1639425512;s:7:\"checked\";a:4:{s:33:\"classic-editor/classic-editor.php\";s:3:\"1.6\";s:29:\"gravityforms/gravityforms.php\";s:8:\"2.4.22.5\";s:27:\"redirection/redirection.php\";s:5:\"5.1.1\";s:25:\"relevanssi/relevanssi.php\";s:6:\"4.13.0\";}s:8:\"response\";a:3:{s:33:\"classic-editor/classic-editor.php\";O:8:\"stdClass\":13:{s:2:\"id\";s:28:\"w.org/plugins/classic-editor\";s:4:\"slug\";s:14:\"classic-editor\";s:6:\"plugin\";s:33:\"classic-editor/classic-editor.php\";s:11:\"new_version\";s:5:\"1.6.2\";s:3:\"url\";s:45:\"https://wordpress.org/plugins/classic-editor/\";s:7:\"package\";s:63:\"https://downloads.wordpress.org/plugin/classic-editor.1.6.2.zip\";s:5:\"icons\";a:2:{s:2:\"2x\";s:67:\"https://ps.w.org/classic-editor/assets/icon-256x256.png?rev=1998671\";s:2:\"1x\";s:67:\"https://ps.w.org/classic-editor/assets/icon-128x128.png?rev=1998671\";}s:7:\"banners\";a:2:{s:2:\"2x\";s:70:\"https://ps.w.org/classic-editor/assets/banner-1544x500.png?rev=1998671\";s:2:\"1x\";s:69:\"https://ps.w.org/classic-editor/assets/banner-772x250.png?rev=1998676\";}s:11:\"banners_rtl\";a:0:{}s:8:\"requires\";s:3:\"4.9\";s:6:\"tested\";s:5:\"5.8.2\";s:12:\"requires_php\";s:5:\"5.2.4\";s:13:\"compatibility\";O:8:\"stdClass\":0:{}}s:27:\"redirection/redirection.php\";O:8:\"stdClass\":13:{s:2:\"id\";s:25:\"w.org/plugins/redirection\";s:4:\"slug\";s:11:\"redirection\";s:6:\"plugin\";s:27:\"redirection/redirection.php\";s:11:\"new_version\";s:5:\"5.1.3\";s:3:\"url\";s:42:\"https://wordpress.org/plugins/redirection/\";s:7:\"package\";s:54:\"https://downloads.wordpress.org/plugin/redirection.zip\";s:5:\"icons\";a:2:{s:2:\"2x\";s:63:\"https://ps.w.org/redirection/assets/icon-256x256.jpg?rev=983639\";s:2:\"1x\";s:63:\"https://ps.w.org/redirection/assets/icon-128x128.jpg?rev=983640\";}s:7:\"banners\";a:2:{s:2:\"2x\";s:66:\"https://ps.w.org/redirection/assets/banner-1544x500.jpg?rev=983641\";s:2:\"1x\";s:65:\"https://ps.w.org/redirection/assets/banner-772x250.jpg?rev=983642\";}s:11:\"banners_rtl\";a:0:{}s:8:\"requires\";s:3:\"5.2\";s:6:\"tested\";s:5:\"5.8.2\";s:12:\"requires_php\";s:3:\"5.6\";s:13:\"compatibility\";O:8:\"stdClass\":0:{}}s:25:\"relevanssi/relevanssi.php\";O:8:\"stdClass\":14:{s:2:\"id\";s:24:\"w.org/plugins/relevanssi\";s:4:\"slug\";s:10:\"relevanssi\";s:6:\"plugin\";s:25:\"relevanssi/relevanssi.php\";s:11:\"new_version\";s:6:\"4.14.4\";s:3:\"url\";s:41:\"https://wordpress.org/plugins/relevanssi/\";s:7:\"package\";s:60:\"https://downloads.wordpress.org/plugin/relevanssi.4.14.4.zip\";s:5:\"icons\";a:2:{s:2:\"2x\";s:63:\"https://ps.w.org/relevanssi/assets/icon-256x256.png?rev=2025044\";s:2:\"1x\";s:63:\"https://ps.w.org/relevanssi/assets/icon-128x128.png?rev=2025044\";}s:7:\"banners\";a:2:{s:2:\"2x\";s:66:\"https://ps.w.org/relevanssi/assets/banner-1544x500.jpg?rev=1647178\";s:2:\"1x\";s:65:\"https://ps.w.org/relevanssi/assets/banner-772x250.jpg?rev=1647180\";}s:11:\"banners_rtl\";a:0:{}s:8:\"requires\";s:3:\"4.9\";s:6:\"tested\";s:5:\"5.8.2\";s:12:\"requires_php\";s:3:\"7.0\";s:13:\"compatibility\";O:8:\"stdClass\":0:{}s:14:\"upgrade_notice\";s:36:\"<ul>\n<li>Small bug fixes.</li>\n</ul>\";}}s:12:\"translations\";a:0:{}s:9:\"no_update\";a:0:{}}','no');
/*!40000 ALTER TABLE `wp_options` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `wp_postmeta`
--

DROP TABLE IF EXISTS `wp_postmeta`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
 SET character_set_client = utf8mb4 ;
CREATE TABLE `wp_postmeta` (
  `meta_id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `post_id` bigint(20) unsigned NOT NULL DEFAULT '0',
  `meta_key` varchar(255) COLLATE utf8mb4_unicode_520_ci DEFAULT NULL,
  `meta_value` longtext COLLATE utf8mb4_unicode_520_ci,
  PRIMARY KEY (`meta_id`),
  KEY `post_id` (`post_id`),
  KEY `meta_key` (`meta_key`(191))
) ENGINE=InnoDB AUTO_INCREMENT=373 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_520_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `wp_postmeta`
--

LOCK TABLES `wp_postmeta` WRITE;
/*!40000 ALTER TABLE `wp_postmeta` DISABLE KEYS */;
INSERT INTO `wp_postmeta` VALUES (2,3,'_wp_page_template','default');
INSERT INTO `wp_postmeta` VALUES (5,6,'_wp_attached_file','2021/04/valley-health-logo.svg');
INSERT INTO `wp_postmeta` VALUES (6,6,'_wp_attachment_image_alt','Valley Health Logo');
INSERT INTO `wp_postmeta` VALUES (11,9,'_edit_last','1');
INSERT INTO `wp_postmeta` VALUES (12,9,'_edit_lock','1635269532:1');
INSERT INTO `wp_postmeta` VALUES (13,9,'_wp_page_template','template-landing.php');
INSERT INTO `wp_postmeta` VALUES (14,9,'modules','a:6:{i:0;s:5:\"alert\";i:1;s:9:\"billboard\";i:2;s:7:\"wysiwyg\";i:3;s:3:\"duo\";i:4;s:4:\"news\";i:5;s:8:\"location\";}');
INSERT INTO `wp_postmeta` VALUES (15,9,'_modules','field_modules_modules');
INSERT INTO `wp_postmeta` VALUES (16,11,'_edit_last','1');
INSERT INTO `wp_postmeta` VALUES (17,11,'_edit_lock','1620756502:1');
INSERT INTO `wp_postmeta` VALUES (18,11,'_wp_page_template','default');
INSERT INTO `wp_postmeta` VALUES (19,13,'_edit_last','1');
INSERT INTO `wp_postmeta` VALUES (20,13,'_wp_page_template','default');
INSERT INTO `wp_postmeta` VALUES (21,13,'_edit_lock','1621019792:1');
INSERT INTO `wp_postmeta` VALUES (25,16,'_menu_item_type','post_type');
INSERT INTO `wp_postmeta` VALUES (26,16,'_menu_item_menu_item_parent','0');
INSERT INTO `wp_postmeta` VALUES (27,16,'_menu_item_object_id','11');
INSERT INTO `wp_postmeta` VALUES (28,16,'_menu_item_object','page');
INSERT INTO `wp_postmeta` VALUES (29,16,'_menu_item_target','');
INSERT INTO `wp_postmeta` VALUES (30,16,'_menu_item_classes','a:1:{i:0;s:0:\"\";}');
INSERT INTO `wp_postmeta` VALUES (31,16,'_menu_item_xfn','');
INSERT INTO `wp_postmeta` VALUES (32,16,'_menu_item_url','');
INSERT INTO `wp_postmeta` VALUES (34,17,'_menu_item_type','post_type');
INSERT INTO `wp_postmeta` VALUES (35,17,'_menu_item_menu_item_parent','0');
INSERT INTO `wp_postmeta` VALUES (36,17,'_menu_item_object_id','13');
INSERT INTO `wp_postmeta` VALUES (37,17,'_menu_item_object','page');
INSERT INTO `wp_postmeta` VALUES (38,17,'_menu_item_target','');
INSERT INTO `wp_postmeta` VALUES (39,17,'_menu_item_classes','a:1:{i:0;s:0:\"\";}');
INSERT INTO `wp_postmeta` VALUES (40,17,'_menu_item_xfn','');
INSERT INTO `wp_postmeta` VALUES (41,17,'_menu_item_url','');
INSERT INTO `wp_postmeta` VALUES (43,18,'_menu_item_type','custom');
INSERT INTO `wp_postmeta` VALUES (44,18,'_menu_item_menu_item_parent','0');
INSERT INTO `wp_postmeta` VALUES (45,18,'_menu_item_object_id','18');
INSERT INTO `wp_postmeta` VALUES (46,18,'_menu_item_object','custom');
INSERT INTO `wp_postmeta` VALUES (47,18,'_menu_item_target','');
INSERT INTO `wp_postmeta` VALUES (48,18,'_menu_item_classes','a:1:{i:0;s:0:\"\";}');
INSERT INTO `wp_postmeta` VALUES (49,18,'_menu_item_xfn','');
INSERT INTO `wp_postmeta` VALUES (50,18,'_menu_item_url','#');
INSERT INTO `wp_postmeta` VALUES (61,20,'_menu_item_type','post_type');
INSERT INTO `wp_postmeta` VALUES (62,20,'_menu_item_menu_item_parent','0');
INSERT INTO `wp_postmeta` VALUES (63,20,'_menu_item_object_id','13');
INSERT INTO `wp_postmeta` VALUES (64,20,'_menu_item_object','page');
INSERT INTO `wp_postmeta` VALUES (65,20,'_menu_item_target','');
INSERT INTO `wp_postmeta` VALUES (66,20,'_menu_item_classes','a:1:{i:0;s:0:\"\";}');
INSERT INTO `wp_postmeta` VALUES (67,20,'_menu_item_xfn','');
INSERT INTO `wp_postmeta` VALUES (68,20,'_menu_item_url','');
INSERT INTO `wp_postmeta` VALUES (70,21,'_menu_item_type','custom');
INSERT INTO `wp_postmeta` VALUES (71,21,'_menu_item_menu_item_parent','0');
INSERT INTO `wp_postmeta` VALUES (72,21,'_menu_item_object_id','21');
INSERT INTO `wp_postmeta` VALUES (73,21,'_menu_item_object','custom');
INSERT INTO `wp_postmeta` VALUES (74,21,'_menu_item_target','');
INSERT INTO `wp_postmeta` VALUES (75,21,'_menu_item_classes','a:1:{i:0;s:0:\"\";}');
INSERT INTO `wp_postmeta` VALUES (76,21,'_menu_item_xfn','');
INSERT INTO `wp_postmeta` VALUES (77,21,'_menu_item_url','#');
INSERT INTO `wp_postmeta` VALUES (79,22,'_menu_item_type','custom');
INSERT INTO `wp_postmeta` VALUES (80,22,'_menu_item_menu_item_parent','0');
INSERT INTO `wp_postmeta` VALUES (81,22,'_menu_item_object_id','22');
INSERT INTO `wp_postmeta` VALUES (82,22,'_menu_item_object','custom');
INSERT INTO `wp_postmeta` VALUES (83,22,'_menu_item_target','_blank');
INSERT INTO `wp_postmeta` VALUES (84,22,'_menu_item_classes','a:1:{i:0;s:0:\"\";}');
INSERT INTO `wp_postmeta` VALUES (85,22,'_menu_item_xfn','');
INSERT INTO `wp_postmeta` VALUES (86,22,'_menu_item_url','https://www.valleyhealthsystemlv.com/');
INSERT INTO `wp_postmeta` VALUES (88,23,'_menu_item_type','custom');
INSERT INTO `wp_postmeta` VALUES (89,23,'_menu_item_menu_item_parent','0');
INSERT INTO `wp_postmeta` VALUES (90,23,'_menu_item_object_id','23');
INSERT INTO `wp_postmeta` VALUES (91,23,'_menu_item_object','custom');
INSERT INTO `wp_postmeta` VALUES (92,23,'_menu_item_target','_blank');
INSERT INTO `wp_postmeta` VALUES (93,23,'_menu_item_classes','a:1:{i:0;s:0:\"\";}');
INSERT INTO `wp_postmeta` VALUES (94,23,'_menu_item_xfn','');
INSERT INTO `wp_postmeta` VALUES (95,23,'_menu_item_url','https://www.springvalleyhospital.com/');
INSERT INTO `wp_postmeta` VALUES (97,24,'_menu_item_type','custom');
INSERT INTO `wp_postmeta` VALUES (98,24,'_menu_item_menu_item_parent','0');
INSERT INTO `wp_postmeta` VALUES (99,24,'_menu_item_object_id','24');
INSERT INTO `wp_postmeta` VALUES (100,24,'_menu_item_object','custom');
INSERT INTO `wp_postmeta` VALUES (101,24,'_menu_item_target','');
INSERT INTO `wp_postmeta` VALUES (102,24,'_menu_item_classes','a:1:{i:0;s:0:\"\";}');
INSERT INTO `wp_postmeta` VALUES (103,24,'_menu_item_xfn','');
INSERT INTO `wp_postmeta` VALUES (104,24,'_menu_item_url','#');
INSERT INTO `wp_postmeta` VALUES (106,25,'_menu_item_type','custom');
INSERT INTO `wp_postmeta` VALUES (107,25,'_menu_item_menu_item_parent','0');
INSERT INTO `wp_postmeta` VALUES (108,25,'_menu_item_object_id','25');
INSERT INTO `wp_postmeta` VALUES (109,25,'_menu_item_object','custom');
INSERT INTO `wp_postmeta` VALUES (110,25,'_menu_item_target','');
INSERT INTO `wp_postmeta` VALUES (111,25,'_menu_item_classes','a:1:{i:0;s:0:\"\";}');
INSERT INTO `wp_postmeta` VALUES (112,25,'_menu_item_xfn','');
INSERT INTO `wp_postmeta` VALUES (113,25,'_menu_item_url','#');
INSERT INTO `wp_postmeta` VALUES (115,26,'_menu_item_type','custom');
INSERT INTO `wp_postmeta` VALUES (116,26,'_menu_item_menu_item_parent','0');
INSERT INTO `wp_postmeta` VALUES (117,26,'_menu_item_object_id','26');
INSERT INTO `wp_postmeta` VALUES (118,26,'_menu_item_object','custom');
INSERT INTO `wp_postmeta` VALUES (119,26,'_menu_item_target','');
INSERT INTO `wp_postmeta` VALUES (120,26,'_menu_item_classes','a:1:{i:0;s:0:\"\";}');
INSERT INTO `wp_postmeta` VALUES (121,26,'_menu_item_xfn','');
INSERT INTO `wp_postmeta` VALUES (122,26,'_menu_item_url','#');
INSERT INTO `wp_postmeta` VALUES (124,27,'_menu_item_type','custom');
INSERT INTO `wp_postmeta` VALUES (125,27,'_menu_item_menu_item_parent','0');
INSERT INTO `wp_postmeta` VALUES (126,27,'_menu_item_object_id','27');
INSERT INTO `wp_postmeta` VALUES (127,27,'_menu_item_object','custom');
INSERT INTO `wp_postmeta` VALUES (128,27,'_menu_item_target','');
INSERT INTO `wp_postmeta` VALUES (129,27,'_menu_item_classes','a:1:{i:0;s:0:\"\";}');
INSERT INTO `wp_postmeta` VALUES (130,27,'_menu_item_xfn','');
INSERT INTO `wp_postmeta` VALUES (131,27,'_menu_item_url','#');
INSERT INTO `wp_postmeta` VALUES (133,28,'_menu_item_type','custom');
INSERT INTO `wp_postmeta` VALUES (134,28,'_menu_item_menu_item_parent','0');
INSERT INTO `wp_postmeta` VALUES (135,28,'_menu_item_object_id','28');
INSERT INTO `wp_postmeta` VALUES (136,28,'_menu_item_object','custom');
INSERT INTO `wp_postmeta` VALUES (137,28,'_menu_item_target','');
INSERT INTO `wp_postmeta` VALUES (138,28,'_menu_item_classes','a:1:{i:0;s:0:\"\";}');
INSERT INTO `wp_postmeta` VALUES (139,28,'_menu_item_xfn','');
INSERT INTO `wp_postmeta` VALUES (140,28,'_menu_item_url','#');
INSERT INTO `wp_postmeta` VALUES (142,9,'modules_0_class','');
INSERT INTO `wp_postmeta` VALUES (143,9,'_modules_0_class','field_modules_modules_alert_class');
INSERT INTO `wp_postmeta` VALUES (144,9,'modules_0_module_title','Health Alert');
INSERT INTO `wp_postmeta` VALUES (145,9,'_modules_0_module_title','field_modules_modules_alert_module_title');
INSERT INTO `wp_postmeta` VALUES (146,9,'modules_0_text','Health Alert: Latest Information on COVID-19 Vaccine');
INSERT INTO `wp_postmeta` VALUES (147,9,'_modules_0_text','field_modules_modules_alert_text');
INSERT INTO `wp_postmeta` VALUES (148,9,'modules_0_button','a:3:{s:5:\"title\";s:8:\"Read Now\";s:3:\"url\";s:106:\"https://www.valleyhealthsystemlv.com/patients-visitors/covid-19/latest-information-on-covid-19-precautions\";s:6:\"target\";s:6:\"_blank\";}');
INSERT INTO `wp_postmeta` VALUES (149,9,'_modules_0_button','field_modules_modules_alert_button');
INSERT INTO `wp_postmeta` VALUES (150,9,'modules_1_class','');
INSERT INTO `wp_postmeta` VALUES (151,9,'_modules_1_class','field_modules_modules_billboard_class');
INSERT INTO `wp_postmeta` VALUES (152,9,'modules_1_module_title','');
INSERT INTO `wp_postmeta` VALUES (153,9,'_modules_1_module_title','field_modules_modules_billboard_module_title');
INSERT INTO `wp_postmeta` VALUES (154,9,'modules_1_billboard_header','Your Valley.<br> Your Care.');
INSERT INTO `wp_postmeta` VALUES (155,9,'_modules_1_billboard_header','field_modules_modules_billboard_billboard_header');
INSERT INTO `wp_postmeta` VALUES (156,9,'modules_1_billboard_billboard_image_large','37');
INSERT INTO `wp_postmeta` VALUES (157,9,'_modules_1_billboard_billboard_image_large','field_modules_modules_billboard_billboard_billboard_image_large');
INSERT INTO `wp_postmeta` VALUES (158,9,'modules_1_billboard_billboard_image_small','38');
INSERT INTO `wp_postmeta` VALUES (159,9,'_modules_1_billboard_billboard_image_small','field_modules_modules_billboard_billboard_billboard_image_small');
INSERT INTO `wp_postmeta` VALUES (160,9,'modules_1_billboard_billboard_image','');
INSERT INTO `wp_postmeta` VALUES (161,9,'_modules_1_billboard_billboard_image','field_modules_modules_billboard_billboard_billboard_image');
INSERT INTO `wp_postmeta` VALUES (162,9,'modules_1_billboard_button_group','0');
INSERT INTO `wp_postmeta` VALUES (163,9,'_modules_1_billboard_button_group','field_modules_modules_billboard_billboard_button_group');
INSERT INTO `wp_postmeta` VALUES (164,9,'modules_1_billboard_buttons','a:1:{i:0;s:14:\"Default Button\";}');
INSERT INTO `wp_postmeta` VALUES (165,9,'_modules_1_billboard_buttons','field_modules_modules_billboard_billboard_buttons');
INSERT INTO `wp_postmeta` VALUES (166,9,'modules_1_billboard','');
INSERT INTO `wp_postmeta` VALUES (167,9,'_modules_1_billboard','field_modules_modules_billboard_billboard');
INSERT INTO `wp_postmeta` VALUES (168,9,'modules_1_billboard_subheader','For Orthopedics and Rehabilitation');
INSERT INTO `wp_postmeta` VALUES (169,9,'_modules_1_billboard_subheader','field_modules_modules_billboard_billboard_subheader');
INSERT INTO `wp_postmeta` VALUES (170,9,'modules_1_billboard_buttons_0_class_field','0');
INSERT INTO `wp_postmeta` VALUES (171,9,'_modules_1_billboard_buttons_0_class_field','field_modules_modules_billboard_billboard_buttons_default_button_class_field');
INSERT INTO `wp_postmeta` VALUES (172,9,'modules_1_billboard_buttons_0_button','a:3:{s:5:\"title\";s:24:\"Learn about our services\";s:3:\"url\";s:10:\"/services/\";s:6:\"target\";s:0:\"\";}');
INSERT INTO `wp_postmeta` VALUES (173,9,'_modules_1_billboard_buttons_0_button','field_modules_modules_billboard_billboard_buttons_default_button_button');
INSERT INTO `wp_postmeta` VALUES (174,9,'modules_2_class','');
INSERT INTO `wp_postmeta` VALUES (175,9,'_modules_2_class','field_modules_modules_wysiwyg_class');
INSERT INTO `wp_postmeta` VALUES (176,9,'modules_2_module_title','Valley Health Intro');
INSERT INTO `wp_postmeta` VALUES (177,9,'_modules_2_module_title','field_modules_modules_wysiwyg_module_title');
INSERT INTO `wp_postmeta` VALUES (178,9,'modules_2_content','<h1 style=\"text-align:center;\">Valley Health Specialty Hospital</h1>\r\n[callout class=\"large\"]\r\n<p style=\"text-align:center;\">Welcome to the first orthopedic surgery and inpatient rehabilitation facility in the Las Vegas area. When we open our doors, Valley Health Specialty Hospital will be the first of its kind to offer both specialty orthopedics and inpatient rehab in one location.</p>\r\n[/callout]');
INSERT INTO `wp_postmeta` VALUES (179,9,'_modules_2_content','field_modules_modules_wysiwyg_content');
INSERT INTO `wp_postmeta` VALUES (180,9,'modules_2_map_add_markers','0');
INSERT INTO `wp_postmeta` VALUES (181,9,'_modules_2_map_add_markers','field_modules_modules_wysiwyg_map_add_markers');
INSERT INTO `wp_postmeta` VALUES (182,9,'modules_2_map','');
INSERT INTO `wp_postmeta` VALUES (183,9,'_modules_2_map','field_modules_modules_wysiwyg_map');
INSERT INTO `wp_postmeta` VALUES (184,37,'_wp_attached_file','2021/04/VHSH_family_herobanner_art_3500-scaled.jpg');
INSERT INTO `wp_postmeta` VALUES (185,37,'_wp_attachment_metadata','a:6:{s:5:\"width\";i:2560;s:6:\"height\";i:843;s:4:\"file\";s:50:\"2021/04/VHSH_family_herobanner_art_3500-scaled.jpg\";s:5:\"sizes\";a:6:{s:6:\"medium\";a:4:{s:4:\"file\";s:42:\"VHSH_family_herobanner_art_3500-300x99.jpg\";s:5:\"width\";i:300;s:6:\"height\";i:99;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:5:\"large\";a:4:{s:4:\"file\";s:44:\"VHSH_family_herobanner_art_3500-1024x337.jpg\";s:5:\"width\";i:1024;s:6:\"height\";i:337;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:9:\"thumbnail\";a:4:{s:4:\"file\";s:43:\"VHSH_family_herobanner_art_3500-150x150.jpg\";s:5:\"width\";i:150;s:6:\"height\";i:150;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:12:\"medium_large\";a:4:{s:4:\"file\";s:43:\"VHSH_family_herobanner_art_3500-768x253.jpg\";s:5:\"width\";i:768;s:6:\"height\";i:253;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:9:\"1536x1536\";a:4:{s:4:\"file\";s:44:\"VHSH_family_herobanner_art_3500-1536x506.jpg\";s:5:\"width\";i:1536;s:6:\"height\";i:506;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:9:\"2048x2048\";a:4:{s:4:\"file\";s:44:\"VHSH_family_herobanner_art_3500-2048x675.jpg\";s:5:\"width\";i:2048;s:6:\"height\";i:675;s:9:\"mime-type\";s:10:\"image/jpeg\";}}s:10:\"image_meta\";a:12:{s:8:\"aperture\";s:1:\"0\";s:6:\"credit\";s:0:\"\";s:6:\"camera\";s:0:\"\";s:7:\"caption\";s:0:\"\";s:17:\"created_timestamp\";s:1:\"0\";s:9:\"copyright\";s:30:\"©Kevin Dodge/Blend Images LLC\";s:12:\"focal_length\";s:1:\"0\";s:3:\"iso\";s:1:\"0\";s:13:\"shutter_speed\";s:1:\"0\";s:5:\"title\";s:0:\"\";s:11:\"orientation\";s:1:\"0\";s:8:\"keywords\";a:0:{}}s:14:\"original_image\";s:35:\"VHSH_family_herobanner_art_3500.jpg\";}');
INSERT INTO `wp_postmeta` VALUES (186,38,'_wp_attached_file','2021/04/VHSH_family_herobanner_mobile_800.jpg');
INSERT INTO `wp_postmeta` VALUES (187,38,'_wp_attachment_metadata','a:5:{s:5:\"width\";i:800;s:6:\"height\";i:800;s:4:\"file\";s:45:\"2021/04/VHSH_family_herobanner_mobile_800.jpg\";s:5:\"sizes\";a:3:{s:6:\"medium\";a:4:{s:4:\"file\";s:45:\"VHSH_family_herobanner_mobile_800-300x300.jpg\";s:5:\"width\";i:300;s:6:\"height\";i:300;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:9:\"thumbnail\";a:4:{s:4:\"file\";s:45:\"VHSH_family_herobanner_mobile_800-150x150.jpg\";s:5:\"width\";i:150;s:6:\"height\";i:150;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:12:\"medium_large\";a:4:{s:4:\"file\";s:45:\"VHSH_family_herobanner_mobile_800-768x768.jpg\";s:5:\"width\";i:768;s:6:\"height\";i:768;s:9:\"mime-type\";s:10:\"image/jpeg\";}}s:10:\"image_meta\";a:12:{s:8:\"aperture\";s:1:\"0\";s:6:\"credit\";s:0:\"\";s:6:\"camera\";s:0:\"\";s:7:\"caption\";s:0:\"\";s:17:\"created_timestamp\";s:1:\"0\";s:9:\"copyright\";s:30:\"©Kevin Dodge/Blend Images LLC\";s:12:\"focal_length\";s:1:\"0\";s:3:\"iso\";s:1:\"0\";s:13:\"shutter_speed\";s:1:\"0\";s:5:\"title\";s:0:\"\";s:11:\"orientation\";s:1:\"0\";s:8:\"keywords\";a:0:{}}}');
INSERT INTO `wp_postmeta` VALUES (188,40,'_wp_attached_file','2021/04/physicaltherapy_928.jpg');
INSERT INTO `wp_postmeta` VALUES (189,40,'_wp_attachment_metadata','a:5:{s:5:\"width\";i:928;s:6:\"height\";i:500;s:4:\"file\";s:31:\"2021/04/physicaltherapy_928.jpg\";s:5:\"sizes\";a:3:{s:6:\"medium\";a:4:{s:4:\"file\";s:31:\"physicaltherapy_928-300x162.jpg\";s:5:\"width\";i:300;s:6:\"height\";i:162;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:9:\"thumbnail\";a:4:{s:4:\"file\";s:31:\"physicaltherapy_928-150x150.jpg\";s:5:\"width\";i:150;s:6:\"height\";i:150;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:12:\"medium_large\";a:4:{s:4:\"file\";s:31:\"physicaltherapy_928-768x414.jpg\";s:5:\"width\";i:768;s:6:\"height\";i:414;s:9:\"mime-type\";s:10:\"image/jpeg\";}}s:10:\"image_meta\";a:12:{s:8:\"aperture\";s:1:\"0\";s:6:\"credit\";s:0:\"\";s:6:\"camera\";s:0:\"\";s:7:\"caption\";s:0:\"\";s:17:\"created_timestamp\";s:1:\"0\";s:9:\"copyright\";s:23:\"COPYRIGHT 2020 FSTOP123\";s:12:\"focal_length\";s:1:\"0\";s:3:\"iso\";s:1:\"0\";s:13:\"shutter_speed\";s:1:\"0\";s:5:\"title\";s:0:\"\";s:11:\"orientation\";s:1:\"0\";s:8:\"keywords\";a:0:{}}}');
INSERT INTO `wp_postmeta` VALUES (190,9,'modules_3_class','');
INSERT INTO `wp_postmeta` VALUES (191,9,'_modules_3_class','field_modules_modules_duo_class');
INSERT INTO `wp_postmeta` VALUES (192,9,'modules_3_module_title','Services ');
INSERT INTO `wp_postmeta` VALUES (193,9,'_modules_3_module_title','field_modules_modules_duo_module_title');
INSERT INTO `wp_postmeta` VALUES (194,9,'modules_3_grid_type','cell');
INSERT INTO `wp_postmeta` VALUES (195,9,'_modules_3_grid_type','field_modules_modules_duo_grid_type');
INSERT INTO `wp_postmeta` VALUES (196,9,'modules_3_block_size','1');
INSERT INTO `wp_postmeta` VALUES (197,9,'_modules_3_block_size','field_modules_modules_duo_block_size');
INSERT INTO `wp_postmeta` VALUES (198,9,'modules_3_block_size_med','1');
INSERT INTO `wp_postmeta` VALUES (199,9,'_modules_3_block_size_med','field_modules_modules_duo_block_size_med');
INSERT INTO `wp_postmeta` VALUES (200,9,'modules_3_media','image');
INSERT INTO `wp_postmeta` VALUES (201,9,'_modules_3_media','field_modules_modules_duo_media');
INSERT INTO `wp_postmeta` VALUES (202,9,'modules_3_media_position','left');
INSERT INTO `wp_postmeta` VALUES (203,9,'_modules_3_media_position','field_modules_modules_duo_media_position');
INSERT INTO `wp_postmeta` VALUES (204,9,'modules_3_duo_image','40');
INSERT INTO `wp_postmeta` VALUES (205,9,'_modules_3_duo_image','field_modules_modules_duo_duo_image');
INSERT INTO `wp_postmeta` VALUES (206,9,'modules_3_card_pre_header_check','0');
INSERT INTO `wp_postmeta` VALUES (207,9,'_modules_3_card_pre_header_check','field_modules_modules_duo_card_pre_header_check');
INSERT INTO `wp_postmeta` VALUES (208,9,'modules_3_card_header','Our Services');
INSERT INTO `wp_postmeta` VALUES (209,9,'_modules_3_card_header','field_modules_modules_duo_card_header');
INSERT INTO `wp_postmeta` VALUES (210,9,'modules_3_card_paragraph','<p>Our rapid recovery process is designed to guide you through orthopedic surgery and recovery and return you to an active lifestyle as quickly as possible.</p>');
INSERT INTO `wp_postmeta` VALUES (211,9,'_modules_3_card_paragraph','field_modules_modules_duo_card_paragraph');
INSERT INTO `wp_postmeta` VALUES (212,9,'modules_3_card_button_group','0');
INSERT INTO `wp_postmeta` VALUES (213,9,'_modules_3_card_button_group','field_modules_modules_duo_card_button_group');
INSERT INTO `wp_postmeta` VALUES (214,9,'modules_3_card_buttons_0_class_field','0');
INSERT INTO `wp_postmeta` VALUES (215,9,'_modules_3_card_buttons_0_class_field','field_modules_modules_duo_card_buttons_default_button_class_field');
INSERT INTO `wp_postmeta` VALUES (216,9,'modules_3_card_buttons_0_button','a:3:{s:5:\"title\";s:10:\"Learn More\";s:3:\"url\";s:10:\"/services/\";s:6:\"target\";s:0:\"\";}');
INSERT INTO `wp_postmeta` VALUES (217,9,'_modules_3_card_buttons_0_button','field_modules_modules_duo_card_buttons_default_button_button');
INSERT INTO `wp_postmeta` VALUES (218,9,'modules_3_card_buttons','a:1:{i:0;s:14:\"Default Button\";}');
INSERT INTO `wp_postmeta` VALUES (219,9,'_modules_3_card_buttons','field_modules_modules_duo_card_buttons');
INSERT INTO `wp_postmeta` VALUES (220,9,'modules_3_card','');
INSERT INTO `wp_postmeta` VALUES (221,9,'_modules_3_card','field_modules_modules_duo_card');
INSERT INTO `wp_postmeta` VALUES (222,9,'modules_3_cell_size','8');
INSERT INTO `wp_postmeta` VALUES (223,9,'_modules_3_cell_size','field_modules_modules_duo_cell_size');
INSERT INTO `wp_postmeta` VALUES (224,9,'modules_3_cell_size_med','6');
INSERT INTO `wp_postmeta` VALUES (225,9,'_modules_3_cell_size_med','field_modules_modules_duo_cell_size_med');
INSERT INTO `wp_postmeta` VALUES (248,48,'_wp_attached_file','2021/04/servicepage_top_3400-scaled.jpg');
INSERT INTO `wp_postmeta` VALUES (249,48,'_wp_attachment_metadata','a:6:{s:5:\"width\";i:2560;s:6:\"height\";i:376;s:4:\"file\";s:39:\"2021/04/servicepage_top_3400-scaled.jpg\";s:5:\"sizes\";a:6:{s:6:\"medium\";a:4:{s:4:\"file\";s:31:\"servicepage_top_3400-300x44.jpg\";s:5:\"width\";i:300;s:6:\"height\";i:44;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:5:\"large\";a:4:{s:4:\"file\";s:33:\"servicepage_top_3400-1024x151.jpg\";s:5:\"width\";i:1024;s:6:\"height\";i:151;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:9:\"thumbnail\";a:4:{s:4:\"file\";s:32:\"servicepage_top_3400-150x150.jpg\";s:5:\"width\";i:150;s:6:\"height\";i:150;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:12:\"medium_large\";a:4:{s:4:\"file\";s:32:\"servicepage_top_3400-768x113.jpg\";s:5:\"width\";i:768;s:6:\"height\";i:113;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:9:\"1536x1536\";a:4:{s:4:\"file\";s:33:\"servicepage_top_3400-1536x226.jpg\";s:5:\"width\";i:1536;s:6:\"height\";i:226;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:9:\"2048x2048\";a:4:{s:4:\"file\";s:33:\"servicepage_top_3400-2048x301.jpg\";s:5:\"width\";i:2048;s:6:\"height\";i:301;s:9:\"mime-type\";s:10:\"image/jpeg\";}}s:10:\"image_meta\";a:12:{s:8:\"aperture\";s:1:\"0\";s:6:\"credit\";s:0:\"\";s:6:\"camera\";s:0:\"\";s:7:\"caption\";s:0:\"\";s:17:\"created_timestamp\";s:1:\"0\";s:9:\"copyright\";s:0:\"\";s:12:\"focal_length\";s:1:\"0\";s:3:\"iso\";s:1:\"0\";s:13:\"shutter_speed\";s:1:\"0\";s:5:\"title\";s:0:\"\";s:11:\"orientation\";s:1:\"0\";s:8:\"keywords\";a:0:{}}s:14:\"original_image\";s:24:\"servicepage_top_3400.jpg\";}');
INSERT INTO `wp_postmeta` VALUES (250,11,'_thumbnail_id','48');
INSERT INTO `wp_postmeta` VALUES (251,11,'map_header','0');
INSERT INTO `wp_postmeta` VALUES (252,11,'_map_header','field_map_header_map_header');
INSERT INTO `wp_postmeta` VALUES (253,13,'map_header','1');
INSERT INTO `wp_postmeta` VALUES (254,13,'_map_header','field_map_header_map_header');
INSERT INTO `wp_postmeta` VALUES (255,16,'_wp_old_date','2021-04-20');
INSERT INTO `wp_postmeta` VALUES (256,18,'_wp_old_date','2021-04-20');
INSERT INTO `wp_postmeta` VALUES (257,17,'_wp_old_date','2021-04-20');
INSERT INTO `wp_postmeta` VALUES (258,55,'_edit_last','1');
INSERT INTO `wp_postmeta` VALUES (259,55,'_edit_lock','1620760066:1');
INSERT INTO `wp_postmeta` VALUES (260,55,'_wp_page_template','default');
INSERT INTO `wp_postmeta` VALUES (261,55,'map_header','0');
INSERT INTO `wp_postmeta` VALUES (262,55,'_map_header','field_map_header_map_header');
INSERT INTO `wp_postmeta` VALUES (263,20,'_wp_old_date','2021-04-20');
INSERT INTO `wp_postmeta` VALUES (264,21,'_wp_old_date','2021-04-20');
INSERT INTO `wp_postmeta` VALUES (265,22,'_wp_old_date','2021-04-20');
INSERT INTO `wp_postmeta` VALUES (266,23,'_wp_old_date','2021-04-20');
INSERT INTO `wp_postmeta` VALUES (276,16,'_wp_old_date','2021-04-21');
INSERT INTO `wp_postmeta` VALUES (277,18,'_wp_old_date','2021-04-21');
INSERT INTO `wp_postmeta` VALUES (278,17,'_wp_old_date','2021-04-21');
INSERT INTO `wp_postmeta` VALUES (279,9,'map_header','0');
INSERT INTO `wp_postmeta` VALUES (280,9,'_map_header','field_map_header_map_header');
INSERT INTO `wp_postmeta` VALUES (299,70,'_menu_item_type','post_type');
INSERT INTO `wp_postmeta` VALUES (300,70,'_menu_item_menu_item_parent','16');
INSERT INTO `wp_postmeta` VALUES (301,70,'_menu_item_object_id','55');
INSERT INTO `wp_postmeta` VALUES (302,70,'_menu_item_object','page');
INSERT INTO `wp_postmeta` VALUES (303,70,'_menu_item_target','');
INSERT INTO `wp_postmeta` VALUES (304,70,'_menu_item_classes','a:1:{i:0;s:0:\"\";}');
INSERT INTO `wp_postmeta` VALUES (305,70,'_menu_item_xfn','');
INSERT INTO `wp_postmeta` VALUES (306,70,'_menu_item_url','');
INSERT INTO `wp_postmeta` VALUES (308,16,'_wp_old_date','2021-04-27');
INSERT INTO `wp_postmeta` VALUES (309,18,'_wp_old_date','2021-04-27');
INSERT INTO `wp_postmeta` VALUES (310,17,'_wp_old_date','2021-04-27');
INSERT INTO `wp_postmeta` VALUES (311,72,'_edit_last','1');
INSERT INTO `wp_postmeta` VALUES (312,72,'_edit_lock','1624547675:1');
INSERT INTO `wp_postmeta` VALUES (313,72,'_wp_page_template','template-formatting.php');
INSERT INTO `wp_postmeta` VALUES (314,72,'map_header','0');
INSERT INTO `wp_postmeta` VALUES (315,72,'_map_header','field_map_header_map_header');
INSERT INTO `wp_postmeta` VALUES (316,78,'_edit_last','1');
INSERT INTO `wp_postmeta` VALUES (317,78,'_edit_lock','1634918482:1');
INSERT INTO `wp_postmeta` VALUES (318,78,'_thumbnail_id','40');
INSERT INTO `wp_postmeta` VALUES (319,9,'modules_4_class','');
INSERT INTO `wp_postmeta` VALUES (320,9,'_modules_4_class','field_modules_modules_news_class');
INSERT INTO `wp_postmeta` VALUES (321,9,'modules_4_module_title','');
INSERT INTO `wp_postmeta` VALUES (322,9,'_modules_4_module_title','field_modules_modules_news_module_title');
INSERT INTO `wp_postmeta` VALUES (323,9,'modules_4_header','News');
INSERT INTO `wp_postmeta` VALUES (324,9,'_modules_4_header','field_modules_modules_news_header');
INSERT INTO `wp_postmeta` VALUES (325,9,'modules_4_description_check','0');
INSERT INTO `wp_postmeta` VALUES (326,9,'_modules_4_description_check','field_modules_modules_news_description_check');
INSERT INTO `wp_postmeta` VALUES (327,9,'modules_4_article','a:3:{i:0;s:2:\"78\";i:1;s:2:\"81\";i:2;s:2:\"83\";}');
INSERT INTO `wp_postmeta` VALUES (328,9,'_modules_4_article','field_modules_modules_news_article');
INSERT INTO `wp_postmeta` VALUES (329,9,'modules_4_button_group','0');
INSERT INTO `wp_postmeta` VALUES (330,9,'_modules_4_button_group','field_modules_modules_news_button_group');
INSERT INTO `wp_postmeta` VALUES (331,9,'modules_4_buttons','a:1:{i:0;s:14:\"Default Button\";}');
INSERT INTO `wp_postmeta` VALUES (332,9,'_modules_4_buttons','field_modules_modules_news_buttons');
INSERT INTO `wp_postmeta` VALUES (333,9,'modules_5_class','sl_module--nobottom');
INSERT INTO `wp_postmeta` VALUES (334,9,'_modules_5_class','field_modules_modules_location_class');
INSERT INTO `wp_postmeta` VALUES (335,9,'modules_5_module_title','');
INSERT INTO `wp_postmeta` VALUES (336,9,'_modules_5_module_title','field_modules_modules_location_module_title');
INSERT INTO `wp_postmeta` VALUES (337,9,'modules_5_card_pre_header_check','0');
INSERT INTO `wp_postmeta` VALUES (338,9,'_modules_5_card_pre_header_check','field_modules_modules_location_card_pre_header_check');
INSERT INTO `wp_postmeta` VALUES (339,9,'modules_5_card_header','Location');
INSERT INTO `wp_postmeta` VALUES (340,9,'_modules_5_card_header','field_modules_modules_location_card_header');
INSERT INTO `wp_postmeta` VALUES (341,9,'modules_5_card_paragraph','<p>The hospital will be located in southwest Las Vegas, close to Spring Valley Hospital Medical Center. If you would like to learn more, please contact us.</p>');
INSERT INTO `wp_postmeta` VALUES (342,9,'_modules_5_card_paragraph','field_modules_modules_location_card_paragraph');
INSERT INTO `wp_postmeta` VALUES (343,9,'modules_5_card_button_group','0');
INSERT INTO `wp_postmeta` VALUES (344,9,'_modules_5_card_button_group','field_modules_modules_location_card_button_group');
INSERT INTO `wp_postmeta` VALUES (345,9,'modules_5_card_buttons_0_class_field','1');
INSERT INTO `wp_postmeta` VALUES (346,9,'_modules_5_card_buttons_0_class_field','field_modules_modules_location_card_buttons_default_button_class_field');
INSERT INTO `wp_postmeta` VALUES (347,9,'modules_5_card_buttons_0_button_class','simple');
INSERT INTO `wp_postmeta` VALUES (348,9,'_modules_5_card_buttons_0_button_class','field_modules_modules_location_card_buttons_default_button_button_class');
INSERT INTO `wp_postmeta` VALUES (349,9,'modules_5_card_buttons_0_button','a:3:{s:5:\"title\";s:44:\"Contact Valley Health  Specialty Hospital \";s:3:\"url\";s:12:\"/contact-us/\";s:6:\"target\";s:0:\"\";}');
INSERT INTO `wp_postmeta` VALUES (350,9,'_modules_5_card_buttons_0_button','field_modules_modules_location_card_buttons_default_button_button');
INSERT INTO `wp_postmeta` VALUES (351,9,'modules_5_card_buttons','a:1:{i:0;s:14:\"Default Button\";}');
INSERT INTO `wp_postmeta` VALUES (352,9,'_modules_5_card_buttons','field_modules_modules_location_card_buttons');
INSERT INTO `wp_postmeta` VALUES (353,9,'modules_5_card','');
INSERT INTO `wp_postmeta` VALUES (354,9,'_modules_5_card','field_modules_modules_location_card');
INSERT INTO `wp_postmeta` VALUES (355,81,'_edit_last','1');
INSERT INTO `wp_postmeta` VALUES (356,81,'_edit_lock','1635265574:1');
INSERT INTO `wp_postmeta` VALUES (357,81,'_thumbnail_id','37');
INSERT INTO `wp_postmeta` VALUES (358,83,'_edit_last','1');
INSERT INTO `wp_postmeta` VALUES (359,83,'_edit_lock','1635265617:1');
INSERT INTO `wp_postmeta` VALUES (360,83,'_thumbnail_id','48');
INSERT INTO `wp_postmeta` VALUES (361,9,'modules_4_grid_type','block');
INSERT INTO `wp_postmeta` VALUES (362,9,'_modules_4_grid_type','field_modules_modules_news_grid_type');
INSERT INTO `wp_postmeta` VALUES (363,9,'modules_4_block_size','3');
INSERT INTO `wp_postmeta` VALUES (364,9,'_modules_4_block_size','field_modules_modules_news_block_size');
INSERT INTO `wp_postmeta` VALUES (365,9,'modules_4_block_size_med','3');
INSERT INTO `wp_postmeta` VALUES (366,9,'_modules_4_block_size_med','field_modules_modules_news_block_size_med');
INSERT INTO `wp_postmeta` VALUES (367,9,'modules_4_description','At The Valley Health System Blog you\'ll find information you can use to improve your overall health and well-being, and stories about our amazing patients and Healthcare Heroes serving our community.\r\n\r\n');
INSERT INTO `wp_postmeta` VALUES (368,9,'_modules_4_description','field_modules_modules_news_description');
INSERT INTO `wp_postmeta` VALUES (369,9,'modules_4_buttons_0_class_field','0');
INSERT INTO `wp_postmeta` VALUES (370,9,'_modules_4_buttons_0_class_field','field_modules_modules_news_buttons_default_button_class_field');
INSERT INTO `wp_postmeta` VALUES (371,9,'modules_4_buttons_0_button','a:3:{s:5:\"title\";s:13:\"View All News\";s:3:\"url\";s:31:\"http://valleyhealth.local/news/\";s:6:\"target\";s:0:\"\";}');
INSERT INTO `wp_postmeta` VALUES (372,9,'_modules_4_buttons_0_button','field_modules_modules_news_buttons_default_button_button');
/*!40000 ALTER TABLE `wp_postmeta` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `wp_posts`
--

DROP TABLE IF EXISTS `wp_posts`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
 SET character_set_client = utf8mb4 ;
CREATE TABLE `wp_posts` (
  `ID` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `post_author` bigint(20) unsigned NOT NULL DEFAULT '0',
  `post_date` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `post_date_gmt` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `post_content` longtext COLLATE utf8mb4_unicode_520_ci NOT NULL,
  `post_title` text COLLATE utf8mb4_unicode_520_ci NOT NULL,
  `post_excerpt` text COLLATE utf8mb4_unicode_520_ci NOT NULL,
  `post_status` varchar(20) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT 'publish',
  `comment_status` varchar(20) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT 'open',
  `ping_status` varchar(20) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT 'open',
  `post_password` varchar(255) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `post_name` varchar(200) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `to_ping` text COLLATE utf8mb4_unicode_520_ci NOT NULL,
  `pinged` text COLLATE utf8mb4_unicode_520_ci NOT NULL,
  `post_modified` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `post_modified_gmt` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `post_content_filtered` longtext COLLATE utf8mb4_unicode_520_ci NOT NULL,
  `post_parent` bigint(20) unsigned NOT NULL DEFAULT '0',
  `guid` varchar(255) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `menu_order` int(11) NOT NULL DEFAULT '0',
  `post_type` varchar(20) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT 'post',
  `post_mime_type` varchar(100) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `comment_count` bigint(20) NOT NULL DEFAULT '0',
  PRIMARY KEY (`ID`),
  KEY `post_name` (`post_name`(191)),
  KEY `type_status_date` (`post_type`,`post_status`,`post_date`,`ID`),
  KEY `post_parent` (`post_parent`),
  KEY `post_author` (`post_author`)
) ENGINE=InnoDB AUTO_INCREMENT=87 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_520_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `wp_posts`
--

LOCK TABLES `wp_posts` WRITE;
/*!40000 ALTER TABLE `wp_posts` DISABLE KEYS */;
INSERT INTO `wp_posts` VALUES (1,1,'2021-04-19 17:39:35','2021-04-19 17:39:35','<!-- wp:paragraph -->\n<p>Welcome to WordPress. This is your first post. Edit or delete it, then start writing!</p>\n<!-- /wp:paragraph -->','Hello world!','','publish','open','open','','hello-world','','','2021-04-19 17:39:35','2021-04-19 17:39:35','',0,'http://valleyhealth.local/?p=1',0,'post','',1);
INSERT INTO `wp_posts` VALUES (3,1,'2021-04-19 17:39:35','2021-04-19 17:39:35','<!-- wp:heading --><h2>Who we are</h2><!-- /wp:heading --><!-- wp:paragraph --><p><strong class=\"privacy-policy-tutorial\">Suggested text: </strong>Our website address is: http://valleyhealth.local.</p><!-- /wp:paragraph --><!-- wp:heading --><h2>Comments</h2><!-- /wp:heading --><!-- wp:paragraph --><p><strong class=\"privacy-policy-tutorial\">Suggested text: </strong>When visitors leave comments on the site we collect the data shown in the comments form, and also the visitor&#8217;s IP address and browser user agent string to help spam detection.</p><!-- /wp:paragraph --><!-- wp:paragraph --><p>An anonymized string created from your email address (also called a hash) may be provided to the Gravatar service to see if you are using it. The Gravatar service privacy policy is available here: https://automattic.com/privacy/. After approval of your comment, your profile picture is visible to the public in the context of your comment.</p><!-- /wp:paragraph --><!-- wp:heading --><h2>Media</h2><!-- /wp:heading --><!-- wp:paragraph --><p><strong class=\"privacy-policy-tutorial\">Suggested text: </strong>If you upload images to the website, you should avoid uploading images with embedded location data (EXIF GPS) included. Visitors to the website can download and extract any location data from images on the website.</p><!-- /wp:paragraph --><!-- wp:heading --><h2>Cookies</h2><!-- /wp:heading --><!-- wp:paragraph --><p><strong class=\"privacy-policy-tutorial\">Suggested text: </strong>If you leave a comment on our site you may opt-in to saving your name, email address and website in cookies. These are for your convenience so that you do not have to fill in your details again when you leave another comment. These cookies will last for one year.</p><!-- /wp:paragraph --><!-- wp:paragraph --><p>If you visit our login page, we will set a temporary cookie to determine if your browser accepts cookies. This cookie contains no personal data and is discarded when you close your browser.</p><!-- /wp:paragraph --><!-- wp:paragraph --><p>When you log in, we will also set up several cookies to save your login information and your screen display choices. Login cookies last for two days, and screen options cookies last for a year. If you select &quot;Remember Me&quot;, your login will persist for two weeks. If you log out of your account, the login cookies will be removed.</p><!-- /wp:paragraph --><!-- wp:paragraph --><p>If you edit or publish an article, an additional cookie will be saved in your browser. This cookie includes no personal data and simply indicates the post ID of the article you just edited. It expires after 1 day.</p><!-- /wp:paragraph --><!-- wp:heading --><h2>Embedded content from other websites</h2><!-- /wp:heading --><!-- wp:paragraph --><p><strong class=\"privacy-policy-tutorial\">Suggested text: </strong>Articles on this site may include embedded content (e.g. videos, images, articles, etc.). Embedded content from other websites behaves in the exact same way as if the visitor has visited the other website.</p><!-- /wp:paragraph --><!-- wp:paragraph --><p>These websites may collect data about you, use cookies, embed additional third-party tracking, and monitor your interaction with that embedded content, including tracking your interaction with the embedded content if you have an account and are logged in to that website.</p><!-- /wp:paragraph --><!-- wp:heading --><h2>Who we share your data with</h2><!-- /wp:heading --><!-- wp:paragraph --><p><strong class=\"privacy-policy-tutorial\">Suggested text: </strong>If you request a password reset, your IP address will be included in the reset email.</p><!-- /wp:paragraph --><!-- wp:heading --><h2>How long we retain your data</h2><!-- /wp:heading --><!-- wp:paragraph --><p><strong class=\"privacy-policy-tutorial\">Suggested text: </strong>If you leave a comment, the comment and its metadata are retained indefinitely. This is so we can recognize and approve any follow-up comments automatically instead of holding them in a moderation queue.</p><!-- /wp:paragraph --><!-- wp:paragraph --><p>For users that register on our website (if any), we also store the personal information they provide in their user profile. All users can see, edit, or delete their personal information at any time (except they cannot change their username). Website administrators can also see and edit that information.</p><!-- /wp:paragraph --><!-- wp:heading --><h2>What rights you have over your data</h2><!-- /wp:heading --><!-- wp:paragraph --><p><strong class=\"privacy-policy-tutorial\">Suggested text: </strong>If you have an account on this site, or have left comments, you can request to receive an exported file of the personal data we hold about you, including any data you have provided to us. You can also request that we erase any personal data we hold about you. This does not include any data we are obliged to keep for administrative, legal, or security purposes.</p><!-- /wp:paragraph --><!-- wp:heading --><h2>Where we send your data</h2><!-- /wp:heading --><!-- wp:paragraph --><p><strong class=\"privacy-policy-tutorial\">Suggested text: </strong>Visitor comments may be checked through an automated spam detection service.</p><!-- /wp:paragraph -->','Privacy Policy','','draft','closed','open','','privacy-policy','','','2021-04-19 17:39:35','2021-04-19 17:39:35','',0,'http://valleyhealth.local/?page_id=3',0,'page','',0);
INSERT INTO `wp_posts` VALUES (6,1,'2021-04-20 18:20:55','2021-04-20 18:20:55','','valley-health-logo','','inherit','open','closed','','valley-health-logo','','','2021-04-20 18:21:02','2021-04-20 18:21:02','',0,'http://valleyhealth.local/wp-content/uploads/2021/04/valley-health-logo.svg',0,'attachment','image/svg+xml',0);
INSERT INTO `wp_posts` VALUES (9,1,'2021-04-20 19:29:48','2021-04-20 19:29:48','','Home Page','','publish','closed','closed','','home-page','','','2021-10-26 16:44:11','2021-10-26 16:44:11','',0,'http://valleyhealth.local/?page_id=9',0,'page','',0);
INSERT INTO `wp_posts` VALUES (10,1,'2021-04-20 19:29:48','2021-04-20 19:29:48','','Home Page','','inherit','closed','closed','','9-revision-v1','','','2021-04-20 19:29:48','2021-04-20 19:29:48','',9,'http://valleyhealth.local/?p=10',0,'revision','',0);
INSERT INTO `wp_posts` VALUES (11,1,'2021-04-20 19:30:13','2021-04-20 19:30:13','Valley Health Specialty Hospital will serve adults 18 and over by providing specialized orthopedic services and inpatient rehabilitation and physical therapy. We will be the first hospital in the area to offer both specialty orthopedics and inpatient rehab in one location.\r\n\r\nAs an extension of Spring Valley Hospital, our service providers will leverage the expertise and advanced treatments used at Spring Valley and other hospitals within The Valley Health System network. \r\nYou will be able to receive the following services at Valley Health Specialty Hospital: \r\n\r\n<h2>Orthopedic Surgery</h2>\r\n\r\nOur experienced team will provide specialized orthopedic services including spine surgery; hip and knee replacement; arthroscopic surgery of the shoulder, elbow, knee and ankle; hand surgery and sports medicine. \r\n\r\n[button class=\"simple\" url=\"/services/orthopedic-surgery/\"]Learn More[/button]\r\n\r\n<h2>Inpatient Rehabilitation</h2>\r\n\r\nOur extensive rehabilitation program will include physical, occupational and speech therapy based on the individual needs of the patient. Specialized programs in neurological, spinal and orthopedic therapy will be available for those recovering from a stroke, Parkinson\'s and other brain disorders, spinal cord and back injuries, and orthopedic surgeries. The team will also work with amputees, burn victims, and those suffering major trauma and other disabling conditions. \r\n','Services','','publish','closed','closed','','services','','','2021-04-27 16:43:40','2021-04-27 16:43:40','',0,'http://valleyhealth.local/?page_id=11',0,'page','',0);
INSERT INTO `wp_posts` VALUES (12,1,'2021-04-20 19:30:13','2021-04-20 19:30:13','','Services','','inherit','closed','closed','','11-revision-v1','','','2021-04-20 19:30:13','2021-04-20 19:30:13','',11,'http://valleyhealth.local/?p=12',0,'revision','',0);
INSERT INTO `wp_posts` VALUES (13,1,'2021-04-20 19:30:26','2021-04-20 19:30:26','<p><strong>Valley Health Specialty Hospital</strong><br>\r\n 8656 West Patrick Lane<br>\r\n Las Vegas, NV 89148<br>\r\n 702-777-7100\r\n</p>\r\n[button class=\"simple\" url=\"https://www.google.com/maps/dir/36.145901,-86.8521476/36.0778011,-115.2806565/@34.6202612,-119.1032809,4z/data=!3m1!4b1!4m4!4m3!1m1!4e1!1m0\" target=\"_blank\"]Directions[/button]\r\n\r\n<p>The hospital will be located in southwest Las Vegas, close to Spring Valley Hospital Medical Center. If you would like more information, please call 702-777-7100 or fill out the form below.</p>\r\n\r\n[gravityform id=\"1\" title=\"true\" description=\"false\"]','Contact Valley Health Specialty Hospital','','publish','closed','closed','','contact-us','','','2021-05-14 18:40:24','2021-05-14 18:40:24','',0,'http://valleyhealth.local/?page_id=13',0,'page','',0);
INSERT INTO `wp_posts` VALUES (14,1,'2021-04-20 19:30:26','2021-04-20 19:30:26','','Contact Us','','inherit','closed','closed','','13-revision-v1','','','2021-04-20 19:30:26','2021-04-20 19:30:26','',13,'http://valleyhealth.local/?p=14',0,'revision','',0);
INSERT INTO `wp_posts` VALUES (16,1,'2021-06-10 14:52:55','2021-04-20 19:34:40',' ','','','publish','closed','closed','','16','','','2021-06-10 14:52:55','2021-06-10 14:52:55','',0,'http://valleyhealth.local/?p=16',1,'nav_menu_item','',0);
INSERT INTO `wp_posts` VALUES (17,1,'2021-06-10 14:52:55','2021-04-20 19:34:40','','Contact Us','','publish','closed','closed','','17','','','2021-06-10 14:52:55','2021-06-10 14:52:55','',0,'http://valleyhealth.local/?p=17',4,'nav_menu_item','',0);
INSERT INTO `wp_posts` VALUES (18,1,'2021-06-10 14:52:55','2021-04-20 19:34:40','','Careers','','publish','closed','closed','','careers','','','2021-06-10 14:52:55','2021-06-10 14:52:55','',0,'http://valleyhealth.local/?p=18',3,'nav_menu_item','',0);
INSERT INTO `wp_posts` VALUES (20,1,'2021-04-27 15:46:57','2021-04-20 19:38:14','','Contact Us','','publish','closed','closed','','20','','','2021-04-27 15:46:57','2021-04-27 15:46:57','',0,'http://valleyhealth.local/?p=20',1,'nav_menu_item','',0);
INSERT INTO `wp_posts` VALUES (21,1,'2021-04-27 15:46:57','2021-04-20 19:38:14','','Careers','','publish','closed','closed','','careers-2','','','2021-04-27 15:46:57','2021-04-27 15:46:57','',0,'http://valleyhealth.local/?p=21',2,'nav_menu_item','',0);
INSERT INTO `wp_posts` VALUES (22,1,'2021-04-27 15:46:57','2021-04-20 19:38:14','','Valley Health System','','publish','closed','closed','','valley-health-system','','','2021-04-27 15:46:57','2021-04-27 15:46:57','',0,'http://valleyhealth.local/?p=22',3,'nav_menu_item','',0);
INSERT INTO `wp_posts` VALUES (23,1,'2021-04-27 15:46:57','2021-04-20 19:38:14','','Spring Valley Hospital','','publish','closed','closed','','spring-valley-hospital','','','2021-04-27 15:46:57','2021-04-27 15:46:57','',0,'http://valleyhealth.local/?p=23',4,'nav_menu_item','',0);
INSERT INTO `wp_posts` VALUES (24,1,'2021-04-20 19:39:29','2021-04-20 19:39:29','','Privacy Policy','','publish','closed','closed','','privacy-policy','','','2021-04-20 19:39:29','2021-04-20 19:39:29','',0,'http://valleyhealth.local/?p=24',1,'nav_menu_item','',0);
INSERT INTO `wp_posts` VALUES (25,1,'2021-04-20 19:39:29','2021-04-20 19:39:29','','HIPAA Statement','','publish','closed','closed','','hipaa-statement','','','2021-04-20 19:39:29','2021-04-20 19:39:29','',0,'http://valleyhealth.local/?p=25',2,'nav_menu_item','',0);
INSERT INTO `wp_posts` VALUES (26,1,'2021-04-20 19:39:29','2021-04-20 19:39:29','','Physician Disclaimer','','publish','closed','closed','','physician-disclaimer','','','2021-04-20 19:39:29','2021-04-20 19:39:29','',0,'http://valleyhealth.local/?p=26',3,'nav_menu_item','',0);
INSERT INTO `wp_posts` VALUES (27,1,'2021-04-20 19:39:30','2021-04-20 19:39:30','','Nondiscrimination Notice','','publish','closed','closed','','nondiscrimination-notice','','','2021-04-20 19:39:30','2021-04-20 19:39:30','',0,'http://valleyhealth.local/?p=27',4,'nav_menu_item','',0);
INSERT INTO `wp_posts` VALUES (28,1,'2021-04-20 19:39:30','2021-04-20 19:39:30','','Language Assistance','','publish','closed','closed','','language-assistance','','','2021-04-20 19:39:30','2021-04-20 19:39:30','',0,'http://valleyhealth.local/?p=28',5,'nav_menu_item','',0);
INSERT INTO `wp_posts` VALUES (29,1,'2021-04-21 14:29:32','2021-04-21 14:29:32','','Home Page','','inherit','closed','closed','','9-revision-v1','','','2021-04-21 14:29:32','2021-04-21 14:29:32','',9,'http://valleyhealth.local/?p=29',0,'revision','',0);
INSERT INTO `wp_posts` VALUES (30,1,'2021-04-21 14:31:17','2021-04-21 14:31:17','','Home Page','','inherit','closed','closed','','9-revision-v1','','','2021-04-21 14:31:17','2021-04-21 14:31:17','',9,'http://valleyhealth.local/?p=30',0,'revision','',0);
INSERT INTO `wp_posts` VALUES (31,1,'2021-04-21 14:33:34','2021-04-21 14:33:34','','Home Page','','inherit','closed','closed','','9-revision-v1','','','2021-04-21 14:33:34','2021-04-21 14:33:34','',9,'http://valleyhealth.local/?p=31',0,'revision','',0);
INSERT INTO `wp_posts` VALUES (32,1,'2021-04-21 14:36:37','2021-04-21 14:36:37','','Home Page','','inherit','closed','closed','','9-revision-v1','','','2021-04-21 14:36:37','2021-04-21 14:36:37','',9,'http://valleyhealth.local/?p=32',0,'revision','',0);
INSERT INTO `wp_posts` VALUES (33,1,'2021-04-21 14:46:36','2021-04-21 14:46:36','','Home Page','','inherit','closed','closed','','9-revision-v1','','','2021-04-21 14:46:36','2021-04-21 14:46:36','',9,'http://valleyhealth.local/?p=33',0,'revision','',0);
INSERT INTO `wp_posts` VALUES (34,1,'2021-04-21 14:59:14','2021-04-21 14:59:14','','Home Page','','inherit','closed','closed','','9-revision-v1','','','2021-04-21 14:59:14','2021-04-21 14:59:14','',9,'http://valleyhealth.local/?p=34',0,'revision','',0);
INSERT INTO `wp_posts` VALUES (35,1,'2021-04-21 15:01:47','2021-04-21 15:01:47','','Home Page','','inherit','closed','closed','','9-revision-v1','','','2021-04-21 15:01:47','2021-04-21 15:01:47','',9,'http://valleyhealth.local/?p=35',0,'revision','',0);
INSERT INTO `wp_posts` VALUES (36,1,'2021-04-21 15:02:11','2021-04-21 15:02:11','','Home Page','','inherit','closed','closed','','9-revision-v1','','','2021-04-21 15:02:11','2021-04-21 15:02:11','',9,'http://valleyhealth.local/?p=36',0,'revision','',0);
INSERT INTO `wp_posts` VALUES (37,1,'2021-04-21 15:14:23','2021-04-21 15:14:23','','VHSH_family_herobanner_art_3500','','inherit','open','closed','','vhsh_family_herobanner_art_3500','','','2021-04-21 15:14:23','2021-04-21 15:14:23','',9,'http://valleyhealth.local/wp-content/uploads/2021/04/VHSH_family_herobanner_art_3500.jpg',0,'attachment','image/jpeg',0);
INSERT INTO `wp_posts` VALUES (38,1,'2021-04-21 15:14:25','2021-04-21 15:14:25','','VHSH_family_herobanner_mobile_800','','inherit','open','closed','','vhsh_family_herobanner_mobile_800','','','2021-04-21 15:14:25','2021-04-21 15:14:25','',9,'http://valleyhealth.local/wp-content/uploads/2021/04/VHSH_family_herobanner_mobile_800.jpg',0,'attachment','image/jpeg',0);
INSERT INTO `wp_posts` VALUES (39,1,'2021-04-21 15:14:51','2021-04-21 15:14:51','','Home Page','','inherit','closed','closed','','9-revision-v1','','','2021-04-21 15:14:51','2021-04-21 15:14:51','',9,'http://valleyhealth.local/?p=39',0,'revision','',0);
INSERT INTO `wp_posts` VALUES (40,1,'2021-04-21 15:20:48','2021-04-21 15:20:48','','physicaltherapy_928','','inherit','open','closed','','physicaltherapy_928','','','2021-04-21 15:20:48','2021-04-21 15:20:48','',9,'http://valleyhealth.local/wp-content/uploads/2021/04/physicaltherapy_928.jpg',0,'attachment','image/jpeg',0);
INSERT INTO `wp_posts` VALUES (41,1,'2021-04-21 15:21:54','2021-04-21 15:21:54','','Home Page','','inherit','closed','closed','','9-revision-v1','','','2021-04-21 15:21:54','2021-04-21 15:21:54','',9,'http://valleyhealth.local/?p=41',0,'revision','',0);
INSERT INTO `wp_posts` VALUES (42,1,'2021-04-21 15:22:42','2021-04-21 15:22:42','','Home Page','','inherit','closed','closed','','9-revision-v1','','','2021-04-21 15:22:42','2021-04-21 15:22:42','',9,'http://valleyhealth.local/?p=42',0,'revision','',0);
INSERT INTO `wp_posts` VALUES (43,1,'2021-04-21 15:41:25','2021-04-21 15:41:25','','Home Page','','inherit','closed','closed','','9-revision-v1','','','2021-04-21 15:41:25','2021-04-21 15:41:25','',9,'http://valleyhealth.local/?p=43',0,'revision','',0);
INSERT INTO `wp_posts` VALUES (44,1,'2021-04-21 15:53:09','2021-04-21 15:53:09','','Home Page','','inherit','closed','closed','','9-revision-v1','','','2021-04-21 15:53:09','2021-04-21 15:53:09','',9,'http://valleyhealth.local/?p=44',0,'revision','',0);
INSERT INTO `wp_posts` VALUES (45,1,'2021-04-21 16:02:18','2021-04-21 16:02:18','','Home Page','','inherit','closed','closed','','9-revision-v1','','','2021-04-21 16:02:18','2021-04-21 16:02:18','',9,'http://valleyhealth.local/?p=45',0,'revision','',0);
INSERT INTO `wp_posts` VALUES (46,1,'2021-04-21 16:03:41','2021-04-21 16:03:41','','Home Page','','inherit','closed','closed','','9-revision-v1','','','2021-04-21 16:03:41','2021-04-21 16:03:41','',9,'http://valleyhealth.local/?p=46',0,'revision','',0);
INSERT INTO `wp_posts` VALUES (47,1,'2021-04-21 16:23:46','2021-04-21 16:23:46','Valley Health Specialty Hospital will serve adults 18 and over by providing specialized orthopedic services and inpatient rehabilitation and physical therapy. We will be the first hospital in the area to offer both specialty orthopedics and inpatient rehab in one location.\r\n\r\nAs an extension of Spring Valley Hospital, our service providers will leverage the expertise and advanced treatments used at Spring Valley and other hospitals within The Valley Health System network. \r\nYou will be able to receive the following services at Valley Health Specialty Hospital: \r\n\r\n<h2>Orthopedic Surgery</h2>\r\n\r\nOur experienced team will provide specialized orthopedic services including spine surgery; hip and knee replacement; arthroscopic surgery of the shoulder, elbow, knee and ankle; hand surgery and sports medicine. \r\n\r\n[button class=\"simple\" url=\"#\"]Learn More[/button]\r\n\r\n<h2>Inpatient Rehabilitation</h2>\r\n\r\nOur extensive rehabilitation program will include physical, occupational and speech therapy based on the individual needs of the patient. Specialized programs in neurological, spinal and orthopedic therapy will be available for those recovering from a stroke, Parkinson\'s and other brain disorders, spinal cord and back injuries, and orthopedic surgeries. The team will also work with amputees, burn victims, and those suffering major trauma and other disabling conditions. \r\n','Services','','inherit','closed','closed','','11-revision-v1','','','2021-04-21 16:23:46','2021-04-21 16:23:46','',11,'http://valleyhealth.local/?p=47',0,'revision','',0);
INSERT INTO `wp_posts` VALUES (48,1,'2021-04-21 16:33:36','2021-04-21 16:33:36','','servicepage_top_3400','','inherit','open','closed','','servicepage_top_3400','','','2021-04-21 16:33:36','2021-04-21 16:33:36','',11,'http://valleyhealth.local/wp-content/uploads/2021/04/servicepage_top_3400.jpg',0,'attachment','image/jpeg',0);
INSERT INTO `wp_posts` VALUES (49,1,'2021-04-21 16:46:18','2021-04-21 16:46:18','Valley Health Specialty Hospital will serve adults 18 and over by providing specialized orthopedic services and inpatient rehabilitation and physical therapy. We will be the first hospital in the area to offer both specialty orthopedics and inpatient rehab in one location.\r\n\r\nAs an extension of Spring Valley Hospital, our service providers will leverage the expertise and advanced treatments used at Spring Valley and other hospitals within The Valley Health System network. \r\nYou will be able to receive the following services at Valley Health Specialty Hospital: \r\n\r\n<h2>Orthopedic Surgery</h2>\r\n\r\nOur experienced team will provide specialized orthopedic services including spine surgery; hip and knee replacement; arthroscopic surgery of the shoulder, elbow, knee and ankle; hand surgery and sports medicine. \r\n\r\n[button class=\"simple\" url=\"#\"]Learn More[/button]\r\n\r\n<h2>Inpatient Rehabilitation</h2>\r\n\r\nOur extensive rehabilitation program will include physical, occupational and speech therapy based on the individual needs of the patient. Specialized programs in neurological, spinal and orthopedic therapy will be available for those recovering from a stroke, Parkinson\'s and other brain disorders, spinal cord and back injuries, and orthopedic surgeries. The team will also work with amputees, burn victims, and those suffering major trauma and other disabling conditions. \r\n','Services','','inherit','closed','closed','','11-revision-v1','','','2021-04-21 16:46:18','2021-04-21 16:46:18','',11,'http://valleyhealth.local/?p=49',0,'revision','',0);
INSERT INTO `wp_posts` VALUES (50,1,'2021-04-21 16:53:16','2021-04-21 16:53:16','Valley Health Specialty Hospital will serve adults 18 and over by providing specialized orthopedic services and inpatient rehabilitation and physical therapy. We will be the first hospital in the area to offer both specialty orthopedics and inpatient rehab in one location.\r\n\r\nAs an extension of Spring Valley Hospital, our service providers will leverage the expertise and advanced treatments used at Spring Valley and other hospitals within The Valley Health System network. \r\nYou will be able to receive the following services at Valley Health Specialty Hospital: \r\n\r\n<h2>Orthopedic Surgery</h2>\r\n\r\nOur experienced team will provide specialized orthopedic services including spine surgery; hip and knee replacement; arthroscopic surgery of the shoulder, elbow, knee and ankle; hand surgery and sports medicine. \r\n\r\n[button class=\"simple\" url=\"#\"]Learn More[/button]\r\n\r\n<h2>Inpatient Rehabilitation</h2>\r\n\r\nOur extensive rehabilitation program will include physical, occupational and speech therapy based on the individual needs of the patient. Specialized programs in neurological, spinal and orthopedic therapy will be available for those recovering from a stroke, Parkinson\'s and other brain disorders, spinal cord and back injuries, and orthopedic surgeries. The team will also work with amputees, burn victims, and those suffering major trauma and other disabling conditions. \r\n','Services','','inherit','closed','closed','','11-revision-v1','','','2021-04-21 16:53:16','2021-04-21 16:53:16','',11,'http://valleyhealth.local/?p=50',0,'revision','',0);
INSERT INTO `wp_posts` VALUES (51,1,'2021-04-21 16:54:39','2021-04-21 16:54:39','<strong>Valley Health Specialty Hospital<strong>\n<p> 8656 West Patrick Lane<br>\n Las Vegas, NV 89148<br>\n 702-777-7100\n</p>','Contact Valley Health Specialty Hospital','','inherit','closed','closed','','13-autosave-v1','','','2021-04-21 16:54:39','2021-04-21 16:54:39','',13,'http://valleyhealth.local/?p=51',0,'revision','',0);
INSERT INTO `wp_posts` VALUES (52,1,'2021-04-21 16:55:47','2021-04-21 16:55:47','<strong>Valley Health Specialty Hospital<strong>\r\n<p> 8656 West Patrick Lane<br>\r\n Las Vegas, NV 89148<br>\r\n 702-777-7100\r\n</p>\r\n[button class=\"simple\" url=\"https://www.google.com/maps/dir/36.145901,-86.8521476/36.0778011,-115.2806565/@34.6202612,-119.1032809,4z/data=!3m1!4b1!4m4!4m3!1m1!4e1!1m0\"] target=\"_blank\"]Directions[/button]\r\n\r\n<p>The hospital will be located in southwest Las Vegas, close to Spring Valley Hospital Medical Center. If you would like more information, please call 702-777-7100 or fill out the form below.</p>','Contact Valley Health Specialty Hospital','','inherit','closed','closed','','13-revision-v1','','','2021-04-21 16:55:47','2021-04-21 16:55:47','',13,'http://valleyhealth.local/?p=52',0,'revision','',0);
INSERT INTO `wp_posts` VALUES (53,1,'2021-04-21 16:56:27','2021-04-21 16:56:27','<p><strong>Valley Health Specialty Hospital</strong><br>\r\n 8656 West Patrick Lane<br>\r\n Las Vegas, NV 89148<br>\r\n 702-777-7100\r\n</p>\r\n[button class=\"simple\" url=\"https://www.google.com/maps/dir/36.145901,-86.8521476/36.0778011,-115.2806565/@34.6202612,-119.1032809,4z/data=!3m1!4b1!4m4!4m3!1m1!4e1!1m0\"] target=\"_blank\"]Directions[/button]\r\n\r\n<p>The hospital will be located in southwest Las Vegas, close to Spring Valley Hospital Medical Center. If you would like more information, please call 702-777-7100 or fill out the form below.</p>','Contact Valley Health Specialty Hospital','','inherit','closed','closed','','13-revision-v1','','','2021-04-21 16:56:27','2021-04-21 16:56:27','',13,'http://valleyhealth.local/?p=53',0,'revision','',0);
INSERT INTO `wp_posts` VALUES (54,1,'2021-04-21 16:56:45','2021-04-21 16:56:45','<p><strong>Valley Health Specialty Hospital</strong><br>\r\n 8656 West Patrick Lane<br>\r\n Las Vegas, NV 89148<br>\r\n 702-777-7100\r\n</p>\r\n[button class=\"simple\" url=\"https://www.google.com/maps/dir/36.145901,-86.8521476/36.0778011,-115.2806565/@34.6202612,-119.1032809,4z/data=!3m1!4b1!4m4!4m3!1m1!4e1!1m0\" target=\"_blank\"]Directions[/button]\r\n\r\n<p>The hospital will be located in southwest Las Vegas, close to Spring Valley Hospital Medical Center. If you would like more information, please call 702-777-7100 or fill out the form below.</p>','Contact Valley Health Specialty Hospital','','inherit','closed','closed','','13-revision-v1','','','2021-04-21 16:56:45','2021-04-21 16:56:45','',13,'http://valleyhealth.local/?p=54',0,'revision','',0);
INSERT INTO `wp_posts` VALUES (55,1,'2021-04-21 17:32:22','2021-04-21 17:32:22','Every injury or disorder to the musculoskeletal system is different, and in most cases, there can be multiple forms of treatment. If your physician finds that medication, exercise or other alternative options cannot provide the proper care, they may recommend surgery.\r\n\r\nOrthopedic surgeons at the Valley Health Specialty Hospital will use advanced techniques and technology to diagnose and treat your injuries and disorders involving your bones, joints, ligaments, tendons, muscles and nerves. We will offer the following orthopedic services:\r\n\r\n<ul>\r\n<li>Spine surgery</li>\r\n<li>Hip and knee replacement</li>\r\n<li>Fracture care</li>\r\n<li>Arthroscopic surgery of the shoulder, elbow, knee and ankle</li>\r\n<li>Hand surgery</li>\r\n<li>Sports medicine</li>\r\n</ul>\r\n\r\n<h2>Minimally Invasive Surgery</h2>\r\n\r\nOrthopedic surgeons at the Valley Health Specialty Hospital will perform minimally invasive procedures whenever possible. Using advanced technology, surgeons make small pencil-sized holes in the body while video equipment is used to provide a magnified view of the surgical site. Instruments are used to perform the surgery through the incisions. This type of surgery may result in smaller scars, less time in the hospital and a faster recovery and return to work and daily activities.*\r\n\r\nHip replacement surgery and spinal surgery are two types of orthopedic procedures that have been revolutionized by minimally invasive surgery. In anterior hip replacement surgery, the surgeon makes a tiny incision at the front of the hip that leaves muscles intact. If you are looking into spine surgery, ask your doctor if a minimally invasive procedure is right for your particular situation.\r\n\r\n<h2>Rehabilitation</h2>\r\n\r\nFollowing orthopedic, spinal or sports-related procedures, patients may need therapy and rehabilitation to help relieve pain and increase function. The Valley Health Specialty Hospital will offer comprehensive rehabilitation services to assist those recovering from surgery and other orthopedic procedures.\r\n\r\n<h2>Hip and Knee Replacement</h2>\r\n\r\nThe Total Joint Services program at the Valley Health Specialty Hospital will focus on hip and knee replacement surgery. Total hip or knee replacement involves replacing diseased joint cartilage with artificial materials. Joint cartilage is a tough, smooth tissue that covers the ends of bones where the joints are located. It enables joints to move with minimal friction.\r\n\r\nTotal hip or knee replacement procedures are most commonly performed in people who have a severe form of osteoarthritis, a common degenerative condition of the joints that is usually associated with aging (age 45 and up). Other indications for joint replacement are trauma or injury, rheumatoid arthritis (the body\'s immune system attacks joint membranes), and bone deformities.\r\n\r\nAbout one million hip and knee replacement procedures are performed in the U.S. each year, according to the National Institutes of Health (NIH). This is expected to increase in the coming years due to an aging population. Nearly 70 million people in the U.S. have some form of arthritis or chronic joint symptoms, according to the NIH. Nearly 21 million U.S. adults have osteoarthritis, and most people over 65 have osteoarthritis in at least one joint.\r\n\r\n<h3>After Surgery</h3>\r\n\r\nOur goal at the Valley Health Specialty Hospital is to discharge you home safely. The best therapy is to get your new joint moving as quickly as possible. Our physical and occupational therapists will see you the day of surgery. You will walk with a walker and be given additional equipment as deemed necessary by the therapy team.\r\n\r\nProper walking in the best way to help your new joint heal. Walk as rhythmically and smoothly as you can using your walker. Don’t hurry. Adjust the length of your step and speed as necessary to walk with an even pattern. As your muscle strength and endurance improve, you may spend more time walking. Most people use their walker for two-three weeks after surgery.\r\n\r\n<h2>Sports Medicine</h2>\r\n\r\nSports medicine is a specialty that covers the prevention, diagnosis, treatment and rehabilitation of injuries caused by sports and exercise-related activities. The focus of sports medicine at the Valley Health Specialty Hospital will be to help patients make a full recovery so they are able to resume their daily activities and return to the sport of choice as they are ready and able.\r\n\r\n<h3>Sports, Exercise, Fall or Overuse Injury Treatment</h3>\r\nOur orthopedic specialists will offer comprehensive care to address conditions or trauma involving the musculoskeletal system resulting from a sports injury, accident, overuse, fall or other condition. Our teams will use a personalized approach in the management and treatment of a variety of conditions including:\r\n<ul>\r\n<li>Anterior cruciate ligament (ACL) and medial collateral ligament (MCL) injuries of the knee</li>\r\n<li>Cartilage injuries in the knee</li>\r\n<li>Achilles tendon injuries</li>\r\n<li>Rotator cuff (shoulder joint area) tears</li>\r\n<li>Other shoulder injuries and conditions</li>\r\n<li>Torn ligaments</li>\r\n<li>Foot and ankle injuries</li>\r\n<li>Osteoarthritis of the hip and knee</li>\r\n</ul>\r\n\r\n[callout class=\"disclaimer\"]\r\n\r\n<p>* Individual results may vary. There are risks associated with any surgical procedure. Talk with your doctor about these risks to find out if minimally invasive surgery is right for you.</p>\r\n\r\n[/callout]','Orthopedic Surgery','','publish','closed','closed','','orthopedic-surgery','','','2021-05-11 18:11:44','2021-05-11 18:11:44','',11,'http://valleyhealth.local/?page_id=55',0,'page','',0);
INSERT INTO `wp_posts` VALUES (56,1,'2021-04-21 17:32:22','2021-04-21 17:32:22','Every injury or disorder to the musculoskeletal system is different, and in most cases, there can be multiple forms of treatment. If your physician finds that medication, exercise or other alternative options cannot provide the proper care, they may recommend surgery.\r\n\r\nOrthopedic surgeons at the Valley Health Specialty Hospital will use advanced techniques and technology to diagnose and treat your injuries and disorders involving your bones, joints, ligaments, tendons, muscles and nerves. We will offer the following orthopedic services:\r\n\r\n<ul>\r\n<li>Spine surgery</li>\r\n<li>Hip and knee replacement</li>\r\n<li>Fracture care</li>\r\n<li>Arthroscopic surgery of the shoulder, elbow, knee and ankle</li>\r\n<li>Hand surgery</li>\r\n<li>Sports medicine</li>\r\n</ul>\r\n\r\n<h2>Minimally Invasive Surgery</h2>\r\n\r\nOrthopedic surgeons at the Valley Health Specialty Hospital will perform minimally invasive procedures whenever possible. Using advanced technology, surgeons make small pencil-sized holes in the body while video equipment is used to provide a magnified view of the surgical site. Instruments are used to perform the surgery through the incisions. This type of surgery may result in smaller scars, less time in the hospital and a faster recovery and return to work and daily activities.*\r\n\r\nHip replacement surgery and spinal surgery are two types of orthopedic procedures that have been revolutionized by minimally invasive surgery. In anterior hip replacement surgery, the surgeon makes a tiny incision at the front of the hip that leaves muscles intact. If you are looking into spine surgery, ask your doctor if a minimally invasive procedure is right for your particular situation.\r\n\r\n<h2>Rehabilitation</h2>\r\n\r\nFollowing orthopedic, spinal or sports-related procedures, patients may need therapy and rehabilitation to help relieve pain and increase function. The Valley Health Specialty Hospital will offer comprehensive rehabilitation services to assist those recovering from surgery and other orthopedic procedures.\r\n\r\n<h2>Hip and Knee Replacement</h2>\r\n\r\nThe Total Joint Services program at the Valley Health Specialty Hospital will focus on hip and knee replacement surgery. Total hip or knee replacement involves replacing diseased joint cartilage with artificial materials. Joint cartilage is a tough, smooth tissue that covers the ends of bones where the joints are located. It enables joints to move with minimal friction.\r\n\r\nTotal hip or knee replacement procedures are most commonly performed in people who have a severe form of osteoarthritis, a common degenerative condition of the joints that is usually associated with aging (age 45 and up). Other indications for joint replacement are trauma or injury, rheumatoid arthritis (the body\'s immune system attacks joint membranes), and bone deformities.\r\n\r\nAbout one million hip and knee replacement procedures are performed in the U.S. each year, according to the National Institutes of Health (NIH). This is expected to increase in the coming years due to an aging population. Nearly 70 million people in the U.S. have some form of arthritis or chronic joint symptoms, according to the NIH. Nearly 21 million U.S. adults have osteoarthritis, and most people over 65 have osteoarthritis in at least one joint.\r\n\r\n<h3>After Surgery</h3>\r\n\r\nOur goal at the Valley Health Specialty Hospital is to discharge you home safely. The best therapy is to get your new joint moving as quickly as possible. Our physical and occupational therapists will see you the day of surgery. You will walk with a walker and be given additional equipment as deemed necessary by the therapy team.\r\n\r\nProper walking in the best way to help your new joint heal. Walk as rhythmically and smoothly as you can using your walker. Don’t hurry. Adjust the length of your step and speed as necessary to walk with an even pattern. As your muscle strength and endurance improve, you may spend more time walking. Most people use their walker for two-three weeks after surgery.\r\n\r\n<h2>Sports Medicine</h2>\r\n\r\nSports medicine is a specialty that covers the prevention, diagnosis, treatment and rehabilitation of injuries caused by sports and exercise-related activities. The focus of sports medicine at the Valley Health Specialty Hospital will be to help patients make a full recovery so they are able to resume their daily activities and return to the sport of choice as they are ready and able.\r\n\r\n<h3>Sports, Exercise, Fall or Overuse Injury Treatment</h3>\r\nOur orthopedic specialists will offer comprehensive care to address conditions or trauma involving the musculoskeletal system resulting from a sports injury, accident, overuse, fall or other condition. Our teams will use a personalized approach in the management and treatment of a variety of conditions including:\r\n<ul>\r\n<li>Anterior cruciate ligament (ACL) and medial collateral ligament (MCL) injuries of the knee</li>\r\n<li>Cartilage injuries in the knee</li>\r\n<li>Achilles tendon injuries</li>\r\n<li>Rotator cuff (shoulder joint area) tears</li>\r\n<li>Other shoulder injuries and conditions</li>\r\n<li>Torn ligaments</li>\r\n<li>Foot and ankle injuries</li>\r\n<li>Osteoarthritis of the hip and knee</li>\r\n</ul>\r\n\r\n<em>* Individual results may vary. There are risks associated with any surgical procedure. Talk with your doctor about these risks to find out if minimally invasive surgery is right for you.</em>','Orthopedic Surgery','','inherit','closed','closed','','55-revision-v1','','','2021-04-21 17:32:22','2021-04-21 17:32:22','',55,'http://valleyhealth.local/?p=56',0,'revision','',0);
INSERT INTO `wp_posts` VALUES (59,1,'2021-04-27 16:43:40','2021-04-27 16:43:40','Valley Health Specialty Hospital will serve adults 18 and over by providing specialized orthopedic services and inpatient rehabilitation and physical therapy. We will be the first hospital in the area to offer both specialty orthopedics and inpatient rehab in one location.\r\n\r\nAs an extension of Spring Valley Hospital, our service providers will leverage the expertise and advanced treatments used at Spring Valley and other hospitals within The Valley Health System network. \r\nYou will be able to receive the following services at Valley Health Specialty Hospital: \r\n\r\n<h2>Orthopedic Surgery</h2>\r\n\r\nOur experienced team will provide specialized orthopedic services including spine surgery; hip and knee replacement; arthroscopic surgery of the shoulder, elbow, knee and ankle; hand surgery and sports medicine. \r\n\r\n[button class=\"simple\" url=\"/services/orthopedic-surgery/\"]Learn More[/button]\r\n\r\n<h2>Inpatient Rehabilitation</h2>\r\n\r\nOur extensive rehabilitation program will include physical, occupational and speech therapy based on the individual needs of the patient. Specialized programs in neurological, spinal and orthopedic therapy will be available for those recovering from a stroke, Parkinson\'s and other brain disorders, spinal cord and back injuries, and orthopedic surgeries. The team will also work with amputees, burn victims, and those suffering major trauma and other disabling conditions. \r\n','Services','','inherit','closed','closed','','11-revision-v1','','','2021-04-27 16:43:40','2021-04-27 16:43:40','',11,'http://valleyhealth.local/?p=59',0,'revision','',0);
INSERT INTO `wp_posts` VALUES (60,1,'2021-04-27 16:45:24','2021-04-27 16:45:24','','Home Page','','inherit','closed','closed','','9-revision-v1','','','2021-04-27 16:45:24','2021-04-27 16:45:24','',9,'http://valleyhealth.local/?p=60',0,'revision','',0);
INSERT INTO `wp_posts` VALUES (64,1,'2021-05-11 18:11:13','2021-05-11 18:11:13','Every injury or disorder to the musculoskeletal system is different, and in most cases, there can be multiple forms of treatment. If your physician finds that medication, exercise or other alternative options cannot provide the proper care, they may recommend surgery.\r\n\r\nOrthopedic surgeons at the Valley Health Specialty Hospital will use advanced techniques and technology to diagnose and treat your injuries and disorders involving your bones, joints, ligaments, tendons, muscles and nerves. We will offer the following orthopedic services:\r\n\r\n<ul>\r\n<li>Spine surgery</li>\r\n<li>Hip and knee replacement</li>\r\n<li>Fracture care</li>\r\n<li>Arthroscopic surgery of the shoulder, elbow, knee and ankle</li>\r\n<li>Hand surgery</li>\r\n<li>Sports medicine</li>\r\n</ul>\r\n\r\n<h2>Minimally Invasive Surgery</h2>\r\n\r\nOrthopedic surgeons at the Valley Health Specialty Hospital will perform minimally invasive procedures whenever possible. Using advanced technology, surgeons make small pencil-sized holes in the body while video equipment is used to provide a magnified view of the surgical site. Instruments are used to perform the surgery through the incisions. This type of surgery may result in smaller scars, less time in the hospital and a faster recovery and return to work and daily activities.*\r\n\r\nHip replacement surgery and spinal surgery are two types of orthopedic procedures that have been revolutionized by minimally invasive surgery. In anterior hip replacement surgery, the surgeon makes a tiny incision at the front of the hip that leaves muscles intact. If you are looking into spine surgery, ask your doctor if a minimally invasive procedure is right for your particular situation.\r\n\r\n<h2>Rehabilitation</h2>\r\n\r\nFollowing orthopedic, spinal or sports-related procedures, patients may need therapy and rehabilitation to help relieve pain and increase function. The Valley Health Specialty Hospital will offer comprehensive rehabilitation services to assist those recovering from surgery and other orthopedic procedures.\r\n\r\n<h2>Hip and Knee Replacement</h2>\r\n\r\nThe Total Joint Services program at the Valley Health Specialty Hospital will focus on hip and knee replacement surgery. Total hip or knee replacement involves replacing diseased joint cartilage with artificial materials. Joint cartilage is a tough, smooth tissue that covers the ends of bones where the joints are located. It enables joints to move with minimal friction.\r\n\r\nTotal hip or knee replacement procedures are most commonly performed in people who have a severe form of osteoarthritis, a common degenerative condition of the joints that is usually associated with aging (age 45 and up). Other indications for joint replacement are trauma or injury, rheumatoid arthritis (the body\'s immune system attacks joint membranes), and bone deformities.\r\n\r\nAbout one million hip and knee replacement procedures are performed in the U.S. each year, according to the National Institutes of Health (NIH). This is expected to increase in the coming years due to an aging population. Nearly 70 million people in the U.S. have some form of arthritis or chronic joint symptoms, according to the NIH. Nearly 21 million U.S. adults have osteoarthritis, and most people over 65 have osteoarthritis in at least one joint.\r\n\r\n<h3>After Surgery</h3>\r\n\r\nOur goal at the Valley Health Specialty Hospital is to discharge you home safely. The best therapy is to get your new joint moving as quickly as possible. Our physical and occupational therapists will see you the day of surgery. You will walk with a walker and be given additional equipment as deemed necessary by the therapy team.\r\n\r\nProper walking in the best way to help your new joint heal. Walk as rhythmically and smoothly as you can using your walker. Don’t hurry. Adjust the length of your step and speed as necessary to walk with an even pattern. As your muscle strength and endurance improve, you may spend more time walking. Most people use their walker for two-three weeks after surgery.\r\n\r\n<h2>Sports Medicine</h2>\r\n\r\nSports medicine is a specialty that covers the prevention, diagnosis, treatment and rehabilitation of injuries caused by sports and exercise-related activities. The focus of sports medicine at the Valley Health Specialty Hospital will be to help patients make a full recovery so they are able to resume their daily activities and return to the sport of choice as they are ready and able.\r\n\r\n<h3>Sports, Exercise, Fall or Overuse Injury Treatment</h3>\r\nOur orthopedic specialists will offer comprehensive care to address conditions or trauma involving the musculoskeletal system resulting from a sports injury, accident, overuse, fall or other condition. Our teams will use a personalized approach in the management and treatment of a variety of conditions including:\r\n<ul>\r\n<li>Anterior cruciate ligament (ACL) and medial collateral ligament (MCL) injuries of the knee</li>\r\n<li>Cartilage injuries in the knee</li>\r\n<li>Achilles tendon injuries</li>\r\n<li>Rotator cuff (shoulder joint area) tears</li>\r\n<li>Other shoulder injuries and conditions</li>\r\n<li>Torn ligaments</li>\r\n<li>Foot and ankle injuries</li>\r\n<li>Osteoarthritis of the hip and knee</li>\r\n</ul>\r\n\r\n[callout class=\"disclaimer\"]* Individual results may vary. There are risks associated with any surgical procedure. Talk with your doctor about these risks to find out if minimally invasive surgery is right for you.[/callout]','Orthopedic Surgery','','inherit','closed','closed','','55-revision-v1','','','2021-05-11 18:11:13','2021-05-11 18:11:13','',55,'http://valleyhealth.local/?p=64',0,'revision','',0);
INSERT INTO `wp_posts` VALUES (65,1,'2021-05-11 18:11:44','2021-05-11 18:11:44','Every injury or disorder to the musculoskeletal system is different, and in most cases, there can be multiple forms of treatment. If your physician finds that medication, exercise or other alternative options cannot provide the proper care, they may recommend surgery.\r\n\r\nOrthopedic surgeons at the Valley Health Specialty Hospital will use advanced techniques and technology to diagnose and treat your injuries and disorders involving your bones, joints, ligaments, tendons, muscles and nerves. We will offer the following orthopedic services:\r\n\r\n<ul>\r\n<li>Spine surgery</li>\r\n<li>Hip and knee replacement</li>\r\n<li>Fracture care</li>\r\n<li>Arthroscopic surgery of the shoulder, elbow, knee and ankle</li>\r\n<li>Hand surgery</li>\r\n<li>Sports medicine</li>\r\n</ul>\r\n\r\n<h2>Minimally Invasive Surgery</h2>\r\n\r\nOrthopedic surgeons at the Valley Health Specialty Hospital will perform minimally invasive procedures whenever possible. Using advanced technology, surgeons make small pencil-sized holes in the body while video equipment is used to provide a magnified view of the surgical site. Instruments are used to perform the surgery through the incisions. This type of surgery may result in smaller scars, less time in the hospital and a faster recovery and return to work and daily activities.*\r\n\r\nHip replacement surgery and spinal surgery are two types of orthopedic procedures that have been revolutionized by minimally invasive surgery. In anterior hip replacement surgery, the surgeon makes a tiny incision at the front of the hip that leaves muscles intact. If you are looking into spine surgery, ask your doctor if a minimally invasive procedure is right for your particular situation.\r\n\r\n<h2>Rehabilitation</h2>\r\n\r\nFollowing orthopedic, spinal or sports-related procedures, patients may need therapy and rehabilitation to help relieve pain and increase function. The Valley Health Specialty Hospital will offer comprehensive rehabilitation services to assist those recovering from surgery and other orthopedic procedures.\r\n\r\n<h2>Hip and Knee Replacement</h2>\r\n\r\nThe Total Joint Services program at the Valley Health Specialty Hospital will focus on hip and knee replacement surgery. Total hip or knee replacement involves replacing diseased joint cartilage with artificial materials. Joint cartilage is a tough, smooth tissue that covers the ends of bones where the joints are located. It enables joints to move with minimal friction.\r\n\r\nTotal hip or knee replacement procedures are most commonly performed in people who have a severe form of osteoarthritis, a common degenerative condition of the joints that is usually associated with aging (age 45 and up). Other indications for joint replacement are trauma or injury, rheumatoid arthritis (the body\'s immune system attacks joint membranes), and bone deformities.\r\n\r\nAbout one million hip and knee replacement procedures are performed in the U.S. each year, according to the National Institutes of Health (NIH). This is expected to increase in the coming years due to an aging population. Nearly 70 million people in the U.S. have some form of arthritis or chronic joint symptoms, according to the NIH. Nearly 21 million U.S. adults have osteoarthritis, and most people over 65 have osteoarthritis in at least one joint.\r\n\r\n<h3>After Surgery</h3>\r\n\r\nOur goal at the Valley Health Specialty Hospital is to discharge you home safely. The best therapy is to get your new joint moving as quickly as possible. Our physical and occupational therapists will see you the day of surgery. You will walk with a walker and be given additional equipment as deemed necessary by the therapy team.\r\n\r\nProper walking in the best way to help your new joint heal. Walk as rhythmically and smoothly as you can using your walker. Don’t hurry. Adjust the length of your step and speed as necessary to walk with an even pattern. As your muscle strength and endurance improve, you may spend more time walking. Most people use their walker for two-three weeks after surgery.\r\n\r\n<h2>Sports Medicine</h2>\r\n\r\nSports medicine is a specialty that covers the prevention, diagnosis, treatment and rehabilitation of injuries caused by sports and exercise-related activities. The focus of sports medicine at the Valley Health Specialty Hospital will be to help patients make a full recovery so they are able to resume their daily activities and return to the sport of choice as they are ready and able.\r\n\r\n<h3>Sports, Exercise, Fall or Overuse Injury Treatment</h3>\r\nOur orthopedic specialists will offer comprehensive care to address conditions or trauma involving the musculoskeletal system resulting from a sports injury, accident, overuse, fall or other condition. Our teams will use a personalized approach in the management and treatment of a variety of conditions including:\r\n<ul>\r\n<li>Anterior cruciate ligament (ACL) and medial collateral ligament (MCL) injuries of the knee</li>\r\n<li>Cartilage injuries in the knee</li>\r\n<li>Achilles tendon injuries</li>\r\n<li>Rotator cuff (shoulder joint area) tears</li>\r\n<li>Other shoulder injuries and conditions</li>\r\n<li>Torn ligaments</li>\r\n<li>Foot and ankle injuries</li>\r\n<li>Osteoarthritis of the hip and knee</li>\r\n</ul>\r\n\r\n[callout class=\"disclaimer\"]\r\n\r\n<p>* Individual results may vary. There are risks associated with any surgical procedure. Talk with your doctor about these risks to find out if minimally invasive surgery is right for you.</p>\r\n\r\n[/callout]','Orthopedic Surgery','','inherit','closed','closed','','55-revision-v1','','','2021-05-11 18:11:44','2021-05-11 18:11:44','',55,'http://valleyhealth.local/?p=65',0,'revision','',0);
INSERT INTO `wp_posts` VALUES (66,1,'2021-05-14 18:10:40','2021-05-14 18:10:40','','Home Page','','inherit','closed','closed','','9-revision-v1','','','2021-05-14 18:10:40','2021-05-14 18:10:40','',9,'http://valleyhealth.local/?p=66',0,'revision','',0);
INSERT INTO `wp_posts` VALUES (67,1,'2021-05-14 18:40:24','2021-05-14 18:40:24','<p><strong>Valley Health Specialty Hospital</strong><br>\r\n 8656 West Patrick Lane<br>\r\n Las Vegas, NV 89148<br>\r\n 702-777-7100\r\n</p>\r\n[button class=\"simple\" url=\"https://www.google.com/maps/dir/36.145901,-86.8521476/36.0778011,-115.2806565/@34.6202612,-119.1032809,4z/data=!3m1!4b1!4m4!4m3!1m1!4e1!1m0\" target=\"_blank\"]Directions[/button]\r\n\r\n<p>The hospital will be located in southwest Las Vegas, close to Spring Valley Hospital Medical Center. If you would like more information, please call 702-777-7100 or fill out the form below.</p>\r\n\r\n[gravityform id=\"1\" title=\"true\" description=\"false\"]','Contact Valley Health Specialty Hospital','','inherit','closed','closed','','13-revision-v1','','','2021-05-14 18:40:24','2021-05-14 18:40:24','',13,'http://valleyhealth.local/?p=67',0,'revision','',0);
INSERT INTO `wp_posts` VALUES (70,1,'2021-06-10 14:52:55','2021-06-10 14:52:55',' ','','','publish','closed','closed','','70','','','2021-06-10 14:52:55','2021-06-10 14:52:55','',11,'http://valleyhealth.local/?p=70',2,'nav_menu_item','',0);
INSERT INTO `wp_posts` VALUES (72,1,'2021-06-24 15:16:55','2021-06-24 15:16:55','','Formatting','','publish','closed','closed','','formatting','','','2021-06-24 15:16:55','2021-06-24 15:16:55','',0,'http://valleyhealth.local/?page_id=72',0,'page','',0);
INSERT INTO `wp_posts` VALUES (73,1,'2021-06-24 15:16:55','2021-06-24 15:16:55','','Formatting','','inherit','closed','closed','','72-revision-v1','','','2021-06-24 15:16:55','2021-06-24 15:16:55','',72,'http://valleyhealth.local/?p=73',0,'revision','',0);
INSERT INTO `wp_posts` VALUES (76,1,'2021-10-22 15:34:43','2021-10-22 15:34:43','','Home Page','','inherit','closed','closed','','9-revision-v1','','','2021-10-22 15:34:43','2021-10-22 15:34:43','',9,'http://valleyhealth.local/?p=76',0,'revision','',0);
INSERT INTO `wp_posts` VALUES (78,1,'2021-10-22 15:58:04','2021-10-22 15:58:04','Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.','Testing News','','publish','closed','closed','','testing-news','','','2021-10-22 15:58:04','2021-10-22 15:58:04','',0,'http://valleyhealth.local/?post_type=sl_news_cpts&#038;p=78',0,'sl_news_cpts','',0);
INSERT INTO `wp_posts` VALUES (79,1,'2021-10-22 15:58:04','2021-10-22 15:58:04','Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.','Testing News','','inherit','closed','closed','','78-revision-v1','','','2021-10-22 15:58:04','2021-10-22 15:58:04','',78,'http://valleyhealth.local/?p=79',0,'revision','',0);
INSERT INTO `wp_posts` VALUES (80,1,'2021-10-22 15:58:54','2021-10-22 15:58:54','','Home Page','','inherit','closed','closed','','9-revision-v1','','','2021-10-22 15:58:54','2021-10-22 15:58:54','',9,'http://valleyhealth.local/?p=80',0,'revision','',0);
INSERT INTO `wp_posts` VALUES (81,1,'2021-10-26 16:28:24','2021-10-26 16:28:24','Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.\r\n','Test News Title','','publish','closed','closed','','test-news-title','','','2021-10-26 16:28:34','2021-10-26 16:28:34','',0,'http://valleyhealth.local/?post_type=sl_news_cpts&#038;p=81',0,'sl_news_cpts','',0);
INSERT INTO `wp_posts` VALUES (82,1,'2021-10-26 16:28:24','2021-10-26 16:28:24','Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.\r\n','Test News Title','','inherit','closed','closed','','81-revision-v1','','','2021-10-26 16:28:24','2021-10-26 16:28:24','',81,'http://valleyhealth.local/?p=82',0,'revision','',0);
INSERT INTO `wp_posts` VALUES (83,1,'2021-10-26 16:29:00','2021-10-26 16:29:00','Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum\r\n\r\n','News Three','','publish','closed','closed','','news-three','','','2021-10-26 16:29:00','2021-10-26 16:29:00','',0,'http://valleyhealth.local/?post_type=sl_news_cpts&#038;p=83',0,'sl_news_cpts','',0);
INSERT INTO `wp_posts` VALUES (84,1,'2021-10-26 16:29:00','2021-10-26 16:29:00','Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum\r\n\r\n','News Three','','inherit','closed','closed','','83-revision-v1','','','2021-10-26 16:29:00','2021-10-26 16:29:00','',83,'http://valleyhealth.local/?p=84',0,'revision','',0);
INSERT INTO `wp_posts` VALUES (85,1,'2021-10-26 16:31:30','2021-10-26 16:31:30','','Home Page','','inherit','closed','closed','','9-revision-v1','','','2021-10-26 16:31:30','2021-10-26 16:31:30','',9,'http://valleyhealth.local/?p=85',0,'revision','',0);
INSERT INTO `wp_posts` VALUES (86,1,'2021-10-26 16:44:11','2021-10-26 16:44:11','','Home Page','','inherit','closed','closed','','9-revision-v1','','','2021-10-26 16:44:11','2021-10-26 16:44:11','',9,'http://valleyhealth.local/?p=86',0,'revision','',0);
/*!40000 ALTER TABLE `wp_posts` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `wp_relevanssi`
--

DROP TABLE IF EXISTS `wp_relevanssi`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
 SET character_set_client = utf8mb4 ;
CREATE TABLE `wp_relevanssi` (
  `doc` bigint(20) NOT NULL DEFAULT '0',
  `term` varchar(50) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '0',
  `term_reverse` varchar(50) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '0',
  `content` mediumint(9) NOT NULL DEFAULT '0',
  `title` mediumint(9) NOT NULL DEFAULT '0',
  `comment` mediumint(9) NOT NULL DEFAULT '0',
  `tag` mediumint(9) NOT NULL DEFAULT '0',
  `link` mediumint(9) NOT NULL DEFAULT '0',
  `author` mediumint(9) NOT NULL DEFAULT '0',
  `category` mediumint(9) NOT NULL DEFAULT '0',
  `excerpt` mediumint(9) NOT NULL DEFAULT '0',
  `taxonomy` mediumint(9) NOT NULL DEFAULT '0',
  `customfield` mediumint(9) NOT NULL DEFAULT '0',
  `mysqlcolumn` mediumint(9) NOT NULL DEFAULT '0',
  `taxonomy_detail` longtext COLLATE utf8mb4_unicode_520_ci NOT NULL,
  `customfield_detail` longtext COLLATE utf8mb4_unicode_520_ci NOT NULL,
  `mysqlcolumn_detail` longtext COLLATE utf8mb4_unicode_520_ci NOT NULL,
  `type` varchar(210) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT 'post',
  `item` bigint(20) NOT NULL DEFAULT '0',
  PRIMARY KEY (`doc`,`term`,`item`),
  KEY `terms` (`term`(20)),
  KEY `relevanssi_term_reverse_idx` (`term_reverse`(10)),
  KEY `typeitem` (`type`(190),`item`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_520_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `wp_relevanssi`
--

LOCK TABLES `wp_relevanssi` WRITE;
/*!40000 ALTER TABLE `wp_relevanssi` DISABLE KEYS */;
INSERT INTO `wp_relevanssi` VALUES (1,'delete','eteled',1,0,0,0,0,0,0,0,0,0,0,'','','','post',0);
INSERT INTO `wp_relevanssi` VALUES (1,'edit','tide',1,0,0,0,0,0,0,0,0,0,0,'','','','post',0);
INSERT INTO `wp_relevanssi` VALUES (1,'hello','olleh',0,1,0,0,0,0,0,0,0,0,0,'','','','post',0);
INSERT INTO `wp_relevanssi` VALUES (1,'post','tsop',1,0,0,0,0,0,0,0,0,0,0,'','','','post',0);
INSERT INTO `wp_relevanssi` VALUES (1,'start','trats',1,0,0,0,0,0,0,0,0,0,0,'','','','post',0);
INSERT INTO `wp_relevanssi` VALUES (1,'welcome','emoclew',1,0,0,0,0,0,0,0,0,0,0,'','','','post',0);
INSERT INTO `wp_relevanssi` VALUES (1,'wordpress','sserpdrow',1,0,0,0,0,0,0,0,0,0,0,'','','','post',0);
INSERT INTO `wp_relevanssi` VALUES (1,'world','dlrow',0,1,0,0,0,0,0,0,0,0,0,'','','','post',0);
INSERT INTO `wp_relevanssi` VALUES (1,'writing','gnitirw',1,0,0,0,0,0,0,0,0,0,0,'','','','post',0);
INSERT INTO `wp_relevanssi` VALUES (3,'accepts','stpecca',1,0,0,0,0,0,0,0,0,0,0,'','','','post',0);
INSERT INTO `wp_relevanssi` VALUES (3,'account','tnuocca',3,0,0,0,0,0,0,0,0,0,0,'','','','post',0);
INSERT INTO `wp_relevanssi` VALUES (3,'additional','lanoitidda',2,0,0,0,0,0,0,0,0,0,0,'','','','post',0);
INSERT INTO `wp_relevanssi` VALUES (3,'address','sserdda',5,0,0,0,0,0,0,0,0,0,0,'','','','post',0);
INSERT INTO `wp_relevanssi` VALUES (3,'administrative','evitartsinimda',1,0,0,0,0,0,0,0,0,0,0,'','','','post',0);
INSERT INTO `wp_relevanssi` VALUES (3,'administrators','srotartsinimda',1,0,0,0,0,0,0,0,0,0,0,'','','','post',0);
INSERT INTO `wp_relevanssi` VALUES (3,'agent','tnega',1,0,0,0,0,0,0,0,0,0,0,'','','','post',0);
INSERT INTO `wp_relevanssi` VALUES (3,'anonymized','dezimynona',1,0,0,0,0,0,0,0,0,0,0,'','','','post',0);
INSERT INTO `wp_relevanssi` VALUES (3,'approval','lavorppa',1,0,0,0,0,0,0,0,0,0,0,'','','','post',0);
INSERT INTO `wp_relevanssi` VALUES (3,'approve','evorppa',1,0,0,0,0,0,0,0,0,0,0,'','','','post',0);
INSERT INTO `wp_relevanssi` VALUES (3,'article','elcitra',2,0,0,0,0,0,0,0,0,0,0,'','','','post',0);
INSERT INTO `wp_relevanssi` VALUES (3,'articles','selcitra',2,0,0,0,0,0,0,0,0,0,0,'','','','post',0);
INSERT INTO `wp_relevanssi` VALUES (3,'automated','detamotua',1,0,0,0,0,0,0,0,0,0,0,'','','','post',0);
INSERT INTO `wp_relevanssi` VALUES (3,'automatically','yllacitamotua',1,0,0,0,0,0,0,0,0,0,0,'','','','post',0);
INSERT INTO `wp_relevanssi` VALUES (3,'automattic','cittamotua',1,0,0,0,0,0,0,0,0,0,0,'','','','post',0);
INSERT INTO `wp_relevanssi` VALUES (3,'available','elbaliava',1,0,0,0,0,0,0,0,0,0,0,'','','','post',0);
INSERT INTO `wp_relevanssi` VALUES (3,'avoid','diova',1,0,0,0,0,0,0,0,0,0,0,'','','','post',0);
INSERT INTO `wp_relevanssi` VALUES (3,'behaves','sevaheb',1,0,0,0,0,0,0,0,0,0,0,'','','','post',0);
INSERT INTO `wp_relevanssi` VALUES (3,'browser','resworb',4,0,0,0,0,0,0,0,0,0,0,'','','','post',0);
INSERT INTO `wp_relevanssi` VALUES (3,'called','dellac',1,0,0,0,0,0,0,0,0,0,0,'','','','post',0);
INSERT INTO `wp_relevanssi` VALUES (3,'change','egnahc',1,0,0,0,0,0,0,0,0,0,0,'','','','post',0);
INSERT INTO `wp_relevanssi` VALUES (3,'checked','dekcehc',1,0,0,0,0,0,0,0,0,0,0,'','','','post',0);
INSERT INTO `wp_relevanssi` VALUES (3,'choices','seciohc',1,0,0,0,0,0,0,0,0,0,0,'','','','post',0);
INSERT INTO `wp_relevanssi` VALUES (3,'close','esolc',1,0,0,0,0,0,0,0,0,0,0,'','','','post',0);
INSERT INTO `wp_relevanssi` VALUES (3,'collect','tcelloc',2,0,0,0,0,0,0,0,0,0,0,'','','','post',0);
INSERT INTO `wp_relevanssi` VALUES (3,'com','moc',1,0,0,0,0,0,0,0,0,0,0,'','','','post',0);
INSERT INTO `wp_relevanssi` VALUES (3,'comment','tnemmoc',6,0,0,0,0,0,0,0,0,0,0,'','','','post',0);
INSERT INTO `wp_relevanssi` VALUES (3,'comments','stnemmoc',6,0,0,0,0,0,0,0,0,0,0,'','','','post',0);
INSERT INTO `wp_relevanssi` VALUES (3,'contains','sniatnoc',1,0,0,0,0,0,0,0,0,0,0,'','','','post',0);
INSERT INTO `wp_relevanssi` VALUES (3,'content','tnetnoc',5,0,0,0,0,0,0,0,0,0,0,'','','','post',0);
INSERT INTO `wp_relevanssi` VALUES (3,'context','txetnoc',1,0,0,0,0,0,0,0,0,0,0,'','','','post',0);
INSERT INTO `wp_relevanssi` VALUES (3,'convenience','ecneinevnoc',1,0,0,0,0,0,0,0,0,0,0,'','','','post',0);
INSERT INTO `wp_relevanssi` VALUES (3,'cookie','eikooc',4,0,0,0,0,0,0,0,0,0,0,'','','','post',0);
INSERT INTO `wp_relevanssi` VALUES (3,'cookies','seikooc',9,0,0,0,0,0,0,0,0,0,0,'','','','post',0);
INSERT INTO `wp_relevanssi` VALUES (3,'created','detaerc',1,0,0,0,0,0,0,0,0,0,0,'','','','post',0);
INSERT INTO `wp_relevanssi` VALUES (3,'data','atad',14,0,0,0,0,0,0,0,0,0,0,'','','','post',0);
INSERT INTO `wp_relevanssi` VALUES (3,'day','yad',1,0,0,0,0,0,0,0,0,0,0,'','','','post',0);
INSERT INTO `wp_relevanssi` VALUES (3,'days','syad',1,0,0,0,0,0,0,0,0,0,0,'','','','post',0);
INSERT INTO `wp_relevanssi` VALUES (3,'delete','eteled',1,0,0,0,0,0,0,0,0,0,0,'','','','post',0);
INSERT INTO `wp_relevanssi` VALUES (3,'details','sliated',1,0,0,0,0,0,0,0,0,0,0,'','','','post',0);
INSERT INTO `wp_relevanssi` VALUES (3,'detection','noitceted',2,0,0,0,0,0,0,0,0,0,0,'','','','post',0);
INSERT INTO `wp_relevanssi` VALUES (3,'determine','enimreted',1,0,0,0,0,0,0,0,0,0,0,'','','','post',0);
INSERT INTO `wp_relevanssi` VALUES (3,'discarded','dedracsid',1,0,0,0,0,0,0,0,0,0,0,'','','','post',0);
INSERT INTO `wp_relevanssi` VALUES (3,'display','yalpsid',1,0,0,0,0,0,0,0,0,0,0,'','','','post',0);
INSERT INTO `wp_relevanssi` VALUES (3,'does','seod',1,0,0,0,0,0,0,0,0,0,0,'','','','post',0);
INSERT INTO `wp_relevanssi` VALUES (3,'download','daolnwod',1,0,0,0,0,0,0,0,0,0,0,'','','','post',0);
INSERT INTO `wp_relevanssi` VALUES (3,'edit','tide',3,0,0,0,0,0,0,0,0,0,0,'','','','post',0);
INSERT INTO `wp_relevanssi` VALUES (3,'edited','detide',1,0,0,0,0,0,0,0,0,0,0,'','','','post',0);
INSERT INTO `wp_relevanssi` VALUES (3,'email','liame',3,0,0,0,0,0,0,0,0,0,0,'','','','post',0);
INSERT INTO `wp_relevanssi` VALUES (3,'embed','debme',1,0,0,0,0,0,0,0,0,0,0,'','','','post',0);
INSERT INTO `wp_relevanssi` VALUES (3,'embedded','deddebme',6,0,0,0,0,0,0,0,0,0,0,'','','','post',0);
INSERT INTO `wp_relevanssi` VALUES (3,'erase','esare',1,0,0,0,0,0,0,0,0,0,0,'','','','post',0);
INSERT INTO `wp_relevanssi` VALUES (3,'exact','tcaxe',1,0,0,0,0,0,0,0,0,0,0,'','','','post',0);
INSERT INTO `wp_relevanssi` VALUES (3,'exif','fixe',1,0,0,0,0,0,0,0,0,0,0,'','','','post',0);
INSERT INTO `wp_relevanssi` VALUES (3,'expires','seripxe',1,0,0,0,0,0,0,0,0,0,0,'','','','post',0);
INSERT INTO `wp_relevanssi` VALUES (3,'exported','detropxe',1,0,0,0,0,0,0,0,0,0,0,'','','','post',0);
INSERT INTO `wp_relevanssi` VALUES (3,'extract','tcartxe',1,0,0,0,0,0,0,0,0,0,0,'','','','post',0);
INSERT INTO `wp_relevanssi` VALUES (3,'file','elif',1,0,0,0,0,0,0,0,0,0,0,'','','','post',0);
INSERT INTO `wp_relevanssi` VALUES (3,'follow','wollof',1,0,0,0,0,0,0,0,0,0,0,'','','','post',0);
INSERT INTO `wp_relevanssi` VALUES (3,'form','mrof',1,0,0,0,0,0,0,0,0,0,0,'','','','post',0);
INSERT INTO `wp_relevanssi` VALUES (3,'gps','spg',1,0,0,0,0,0,0,0,0,0,0,'','','','post',0);
INSERT INTO `wp_relevanssi` VALUES (3,'gravatar','ratavarg',2,0,0,0,0,0,0,0,0,0,0,'','','','post',0);
INSERT INTO `wp_relevanssi` VALUES (3,'hash','hsah',1,0,0,0,0,0,0,0,0,0,0,'','','','post',0);
INSERT INTO `wp_relevanssi` VALUES (3,'help','pleh',1,0,0,0,0,0,0,0,0,0,0,'','','','post',0);
INSERT INTO `wp_relevanssi` VALUES (3,'hold','dloh',2,0,0,0,0,0,0,0,0,0,0,'','','','post',0);
INSERT INTO `wp_relevanssi` VALUES (3,'holding','gnidloh',1,0,0,0,0,0,0,0,0,0,0,'','','','post',0);
INSERT INTO `wp_relevanssi` VALUES (3,'http','ptth',1,0,0,0,0,0,0,0,0,0,0,'','','','post',0);
INSERT INTO `wp_relevanssi` VALUES (3,'https','sptth',1,0,0,0,0,0,0,0,0,0,0,'','','','post',0);
INSERT INTO `wp_relevanssi` VALUES (3,'images','segami',4,0,0,0,0,0,0,0,0,0,0,'','','','post',0);
INSERT INTO `wp_relevanssi` VALUES (3,'include','edulcni',2,0,0,0,0,0,0,0,0,0,0,'','','','post',0);
INSERT INTO `wp_relevanssi` VALUES (3,'included','dedulcni',2,0,0,0,0,0,0,0,0,0,0,'','','','post',0);
INSERT INTO `wp_relevanssi` VALUES (3,'includes','sedulcni',1,0,0,0,0,0,0,0,0,0,0,'','','','post',0);
INSERT INTO `wp_relevanssi` VALUES (3,'including','gnidulcni',2,0,0,0,0,0,0,0,0,0,0,'','','','post',0);
INSERT INTO `wp_relevanssi` VALUES (3,'indefinitely','yletinifedni',1,0,0,0,0,0,0,0,0,0,0,'','','','post',0);
INSERT INTO `wp_relevanssi` VALUES (3,'indicates','setacidni',1,0,0,0,0,0,0,0,0,0,0,'','','','post',0);
INSERT INTO `wp_relevanssi` VALUES (3,'information','noitamrofni',4,0,0,0,0,0,0,0,0,0,0,'','','','post',0);
INSERT INTO `wp_relevanssi` VALUES (3,'instead','daetsni',1,0,0,0,0,0,0,0,0,0,0,'','','','post',0);
INSERT INTO `wp_relevanssi` VALUES (3,'interaction','noitcaretni',2,0,0,0,0,0,0,0,0,0,0,'','','','post',0);
INSERT INTO `wp_relevanssi` VALUES (3,'just','tsuj',1,0,0,0,0,0,0,0,0,0,0,'','','','post',0);
INSERT INTO `wp_relevanssi` VALUES (3,'leave','evael',4,0,0,0,0,0,0,0,0,0,0,'','','','post',0);
INSERT INTO `wp_relevanssi` VALUES (3,'left','tfel',1,0,0,0,0,0,0,0,0,0,0,'','','','post',0);
INSERT INTO `wp_relevanssi` VALUES (3,'legal','lagel',1,0,0,0,0,0,0,0,0,0,0,'','','','post',0);
INSERT INTO `wp_relevanssi` VALUES (3,'local','lacol',1,0,0,0,0,0,0,0,0,0,0,'','','','post',0);
INSERT INTO `wp_relevanssi` VALUES (3,'location','noitacol',2,0,0,0,0,0,0,0,0,0,0,'','','','post',0);
INSERT INTO `wp_relevanssi` VALUES (3,'log','gol',2,0,0,0,0,0,0,0,0,0,0,'','','','post',0);
INSERT INTO `wp_relevanssi` VALUES (3,'logged','deggol',1,0,0,0,0,0,0,0,0,0,0,'','','','post',0);
INSERT INTO `wp_relevanssi` VALUES (3,'login','nigol',5,0,0,0,0,0,0,0,0,0,0,'','','','post',0);
INSERT INTO `wp_relevanssi` VALUES (3,'long','gnol',1,0,0,0,0,0,0,0,0,0,0,'','','','post',0);
INSERT INTO `wp_relevanssi` VALUES (3,'media','aidem',1,0,0,0,0,0,0,0,0,0,0,'','','','post',0);
INSERT INTO `wp_relevanssi` VALUES (3,'metadata','atadatem',1,0,0,0,0,0,0,0,0,0,0,'','','','post',0);
INSERT INTO `wp_relevanssi` VALUES (3,'moderation','noitaredom',1,0,0,0,0,0,0,0,0,0,0,'','','','post',0);
INSERT INTO `wp_relevanssi` VALUES (3,'monitor','rotinom',1,0,0,0,0,0,0,0,0,0,0,'','','','post',0);
INSERT INTO `wp_relevanssi` VALUES (3,'obliged','degilbo',1,0,0,0,0,0,0,0,0,0,0,'','','','post',0);
INSERT INTO `wp_relevanssi` VALUES (3,'opt','tpo',1,0,0,0,0,0,0,0,0,0,0,'','','','post',0);
INSERT INTO `wp_relevanssi` VALUES (3,'options','snoitpo',1,0,0,0,0,0,0,0,0,0,0,'','','','post',0);
INSERT INTO `wp_relevanssi` VALUES (3,'page','egap',1,0,0,0,0,0,0,0,0,0,0,'','','','post',0);
INSERT INTO `wp_relevanssi` VALUES (3,'party','ytrap',1,0,0,0,0,0,0,0,0,0,0,'','','','post',0);
INSERT INTO `wp_relevanssi` VALUES (3,'password','drowssap',1,0,0,0,0,0,0,0,0,0,0,'','','','post',0);
INSERT INTO `wp_relevanssi` VALUES (3,'persist','tsisrep',1,0,0,0,0,0,0,0,0,0,0,'','','','post',0);
INSERT INTO `wp_relevanssi` VALUES (3,'personal','lanosrep',6,0,0,0,0,0,0,0,0,0,0,'','','','post',0);
INSERT INTO `wp_relevanssi` VALUES (3,'picture','erutcip',1,0,0,0,0,0,0,0,0,0,0,'','','','post',0);
INSERT INTO `wp_relevanssi` VALUES (3,'policy','ycilop',1,1,0,0,0,0,0,0,0,0,0,'','','','post',0);
INSERT INTO `wp_relevanssi` VALUES (3,'post','tsop',1,0,0,0,0,0,0,0,0,0,0,'','','','post',0);
INSERT INTO `wp_relevanssi` VALUES (3,'privacy','ycavirp',2,1,0,0,0,0,0,0,0,0,0,'','','','post',0);
INSERT INTO `wp_relevanssi` VALUES (3,'profile','eliforp',2,0,0,0,0,0,0,0,0,0,0,'','','','post',0);
INSERT INTO `wp_relevanssi` VALUES (3,'provide','edivorp',1,0,0,0,0,0,0,0,0,0,0,'','','','post',0);
INSERT INTO `wp_relevanssi` VALUES (3,'provided','dedivorp',2,0,0,0,0,0,0,0,0,0,0,'','','','post',0);
INSERT INTO `wp_relevanssi` VALUES (3,'public','cilbup',1,0,0,0,0,0,0,0,0,0,0,'','','','post',0);
INSERT INTO `wp_relevanssi` VALUES (3,'publish','hsilbup',1,0,0,0,0,0,0,0,0,0,0,'','','','post',0);
INSERT INTO `wp_relevanssi` VALUES (3,'purposes','sesoprup',1,0,0,0,0,0,0,0,0,0,0,'','','','post',0);
INSERT INTO `wp_relevanssi` VALUES (3,'queue','eueuq',1,0,0,0,0,0,0,0,0,0,0,'','','','post',0);
INSERT INTO `wp_relevanssi` VALUES (3,'receive','eviecer',1,0,0,0,0,0,0,0,0,0,0,'','','','post',0);
INSERT INTO `wp_relevanssi` VALUES (3,'recognize','ezingocer',1,0,0,0,0,0,0,0,0,0,0,'','','','post',0);
INSERT INTO `wp_relevanssi` VALUES (3,'register','retsiger',1,0,0,0,0,0,0,0,0,0,0,'','','','post',0);
INSERT INTO `wp_relevanssi` VALUES (3,'remember','rebmemer',1,0,0,0,0,0,0,0,0,0,0,'','','','post',0);
INSERT INTO `wp_relevanssi` VALUES (3,'removed','devomer',1,0,0,0,0,0,0,0,0,0,0,'','','','post',0);
INSERT INTO `wp_relevanssi` VALUES (3,'request','tseuqer',3,0,0,0,0,0,0,0,0,0,0,'','','','post',0);
INSERT INTO `wp_relevanssi` VALUES (3,'reset','teser',2,0,0,0,0,0,0,0,0,0,0,'','','','post',0);
INSERT INTO `wp_relevanssi` VALUES (3,'retain','niater',1,0,0,0,0,0,0,0,0,0,0,'','','','post',0);
INSERT INTO `wp_relevanssi` VALUES (3,'retained','deniater',1,0,0,0,0,0,0,0,0,0,0,'','','','post',0);
INSERT INTO `wp_relevanssi` VALUES (3,'rights','sthgir',1,0,0,0,0,0,0,0,0,0,0,'','','','post',0);
INSERT INTO `wp_relevanssi` VALUES (3,'save','evas',1,0,0,0,0,0,0,0,0,0,0,'','','','post',0);
INSERT INTO `wp_relevanssi` VALUES (3,'saved','devas',1,0,0,0,0,0,0,0,0,0,0,'','','','post',0);
INSERT INTO `wp_relevanssi` VALUES (3,'saving','gnivas',1,0,0,0,0,0,0,0,0,0,0,'','','','post',0);
INSERT INTO `wp_relevanssi` VALUES (3,'screen','neercs',2,0,0,0,0,0,0,0,0,0,0,'','','','post',0);
INSERT INTO `wp_relevanssi` VALUES (3,'security','ytiruces',1,0,0,0,0,0,0,0,0,0,0,'','','','post',0);
INSERT INTO `wp_relevanssi` VALUES (3,'select','tceles',1,0,0,0,0,0,0,0,0,0,0,'','','','post',0);
INSERT INTO `wp_relevanssi` VALUES (3,'send','dnes',1,0,0,0,0,0,0,0,0,0,0,'','','','post',0);
INSERT INTO `wp_relevanssi` VALUES (3,'service','ecivres',3,0,0,0,0,0,0,0,0,0,0,'','','','post',0);
INSERT INTO `wp_relevanssi` VALUES (3,'set','tes',2,0,0,0,0,0,0,0,0,0,0,'','','','post',0);
INSERT INTO `wp_relevanssi` VALUES (3,'share','erahs',1,0,0,0,0,0,0,0,0,0,0,'','','','post',0);
INSERT INTO `wp_relevanssi` VALUES (3,'shown','nwohs',1,0,0,0,0,0,0,0,0,0,0,'','','','post',0);
INSERT INTO `wp_relevanssi` VALUES (3,'simply','ylpmis',1,0,0,0,0,0,0,0,0,0,0,'','','','post',0);
INSERT INTO `wp_relevanssi` VALUES (3,'site','etis',4,0,0,0,0,0,0,0,0,0,0,'','','','post',0);
INSERT INTO `wp_relevanssi` VALUES (3,'spam','maps',2,0,0,0,0,0,0,0,0,0,0,'','','','post',0);
INSERT INTO `wp_relevanssi` VALUES (3,'store','erots',1,0,0,0,0,0,0,0,0,0,0,'','','','post',0);
INSERT INTO `wp_relevanssi` VALUES (3,'string','gnirts',2,0,0,0,0,0,0,0,0,0,0,'','','','post',0);
INSERT INTO `wp_relevanssi` VALUES (3,'suggested','detseggus',9,0,0,0,0,0,0,0,0,0,0,'','','','post',0);
INSERT INTO `wp_relevanssi` VALUES (3,'temporary','yraropmet',1,0,0,0,0,0,0,0,0,0,0,'','','','post',0);
INSERT INTO `wp_relevanssi` VALUES (3,'text','txet',9,0,0,0,0,0,0,0,0,0,0,'','','','post',0);
INSERT INTO `wp_relevanssi` VALUES (3,'time','emit',1,0,0,0,0,0,0,0,0,0,0,'','','','post',0);
INSERT INTO `wp_relevanssi` VALUES (3,'tracking','gnikcart',2,0,0,0,0,0,0,0,0,0,0,'','','','post',0);
INSERT INTO `wp_relevanssi` VALUES (3,'upload','daolpu',1,0,0,0,0,0,0,0,0,0,0,'','','','post',0);
INSERT INTO `wp_relevanssi` VALUES (3,'uploading','gnidaolpu',1,0,0,0,0,0,0,0,0,0,0,'','','','post',0);
INSERT INTO `wp_relevanssi` VALUES (3,'use','esu',1,0,0,0,0,0,0,0,0,0,0,'','','','post',0);
INSERT INTO `wp_relevanssi` VALUES (3,'user','resu',2,0,0,0,0,0,0,0,0,0,0,'','','','post',0);
INSERT INTO `wp_relevanssi` VALUES (3,'username','emanresu',1,0,0,0,0,0,0,0,0,0,0,'','','','post',0);
INSERT INTO `wp_relevanssi` VALUES (3,'users','sresu',2,0,0,0,0,0,0,0,0,0,0,'','','','post',0);
INSERT INTO `wp_relevanssi` VALUES (3,'using','gnisu',1,0,0,0,0,0,0,0,0,0,0,'','','','post',0);
INSERT INTO `wp_relevanssi` VALUES (3,'valleyhealth','htlaehyellav',1,0,0,0,0,0,0,0,0,0,0,'','','','post',0);
INSERT INTO `wp_relevanssi` VALUES (3,'videos','soediv',1,0,0,0,0,0,0,0,0,0,0,'','','','post',0);
INSERT INTO `wp_relevanssi` VALUES (3,'visible','elbisiv',1,0,0,0,0,0,0,0,0,0,0,'','','','post',0);
INSERT INTO `wp_relevanssi` VALUES (3,'visit','tisiv',1,0,0,0,0,0,0,0,0,0,0,'','','','post',0);
INSERT INTO `wp_relevanssi` VALUES (3,'visited','detisiv',1,0,0,0,0,0,0,0,0,0,0,'','','','post',0);
INSERT INTO `wp_relevanssi` VALUES (3,'visitor','rotisiv',3,0,0,0,0,0,0,0,0,0,0,'','','','post',0);
INSERT INTO `wp_relevanssi` VALUES (3,'visitors','srotisiv',2,0,0,0,0,0,0,0,0,0,0,'','','','post',0);
INSERT INTO `wp_relevanssi` VALUES (3,'way','yaw',1,0,0,0,0,0,0,0,0,0,0,'','','','post',0);
INSERT INTO `wp_relevanssi` VALUES (3,'website','etisbew',9,0,0,0,0,0,0,0,0,0,0,'','','','post',0);
INSERT INTO `wp_relevanssi` VALUES (3,'websites','setisbew',3,0,0,0,0,0,0,0,0,0,0,'','','','post',0);
INSERT INTO `wp_relevanssi` VALUES (3,'weeks','skeew',1,0,0,0,0,0,0,0,0,0,0,'','','','post',0);
INSERT INTO `wp_relevanssi` VALUES (3,'year','raey',2,0,0,0,0,0,0,0,0,0,0,'','','','post',0);
INSERT INTO `wp_relevanssi` VALUES (9,'home','emoh',0,1,0,0,0,0,0,0,0,0,0,'','','','post',0);
INSERT INTO `wp_relevanssi` VALUES (9,'page','egap',0,1,0,0,0,0,0,0,0,0,0,'','','','post',0);
INSERT INTO `wp_relevanssi` VALUES (11,'able','elba',1,0,0,0,0,0,0,0,0,0,0,'','','','post',0);
INSERT INTO `wp_relevanssi` VALUES (11,'adults','stluda',1,0,0,0,0,0,0,0,0,0,0,'','','','post',0);
INSERT INTO `wp_relevanssi` VALUES (11,'advanced','decnavda',1,0,0,0,0,0,0,0,0,0,0,'','','','post',0);
INSERT INTO `wp_relevanssi` VALUES (11,'amputees','seetupma',1,0,0,0,0,0,0,0,0,0,0,'','','','post',0);
INSERT INTO `wp_relevanssi` VALUES (11,'ankle','elkna',1,0,0,0,0,0,0,0,0,0,0,'','','','post',0);
INSERT INTO `wp_relevanssi` VALUES (11,'area','aera',1,0,0,0,0,0,0,0,0,0,0,'','','','post',0);
INSERT INTO `wp_relevanssi` VALUES (11,'arthroscopic','cipocsorhtra',1,0,0,0,0,0,0,0,0,0,0,'','','','post',0);
INSERT INTO `wp_relevanssi` VALUES (11,'available','elbaliava',1,0,0,0,0,0,0,0,0,0,0,'','','','post',0);
INSERT INTO `wp_relevanssi` VALUES (11,'based','desab',1,0,0,0,0,0,0,0,0,0,0,'','','','post',0);
INSERT INTO `wp_relevanssi` VALUES (11,'brain','niarb',1,0,0,0,0,0,0,0,0,0,0,'','','','post',0);
INSERT INTO `wp_relevanssi` VALUES (11,'burn','nrub',1,0,0,0,0,0,0,0,0,0,0,'','','','post',0);
INSERT INTO `wp_relevanssi` VALUES (11,'conditions','snoitidnoc',1,0,0,0,0,0,0,0,0,0,0,'','','','post',0);
INSERT INTO `wp_relevanssi` VALUES (11,'cord','droc',1,0,0,0,0,0,0,0,0,0,0,'','','','post',0);
INSERT INTO `wp_relevanssi` VALUES (11,'disabling','gnilbasid',1,0,0,0,0,0,0,0,0,0,0,'','','','post',0);
INSERT INTO `wp_relevanssi` VALUES (11,'disorders','sredrosid',1,0,0,0,0,0,0,0,0,0,0,'','','','post',0);
INSERT INTO `wp_relevanssi` VALUES (11,'elbow','woble',1,0,0,0,0,0,0,0,0,0,0,'','','','post',0);
INSERT INTO `wp_relevanssi` VALUES (11,'experienced','decneirepxe',1,0,0,0,0,0,0,0,0,0,0,'','','','post',0);
INSERT INTO `wp_relevanssi` VALUES (11,'expertise','esitrepxe',1,0,0,0,0,0,0,0,0,0,0,'','','','post',0);
INSERT INTO `wp_relevanssi` VALUES (11,'extension','noisnetxe',1,0,0,0,0,0,0,0,0,0,0,'','','','post',0);
INSERT INTO `wp_relevanssi` VALUES (11,'extensive','evisnetxe',1,0,0,0,0,0,0,0,0,0,0,'','','','post',0);
INSERT INTO `wp_relevanssi` VALUES (11,'following','gniwollof',1,0,0,0,0,0,0,0,0,0,0,'','','','post',0);
INSERT INTO `wp_relevanssi` VALUES (11,'hand','dnah',1,0,0,0,0,0,0,0,0,0,0,'','','','post',0);
INSERT INTO `wp_relevanssi` VALUES (11,'health','htlaeh',3,0,0,0,0,0,0,0,0,0,0,'','','','post',0);
INSERT INTO `wp_relevanssi` VALUES (11,'hip','pih',1,0,0,0,0,0,0,0,0,0,0,'','','','post',0);
INSERT INTO `wp_relevanssi` VALUES (11,'hospital','latipsoh',4,0,0,0,0,0,0,0,0,0,0,'','','','post',0);
INSERT INTO `wp_relevanssi` VALUES (11,'hospitals','slatipsoh',1,0,0,0,0,0,0,0,0,0,0,'','','','post',0);
INSERT INTO `wp_relevanssi` VALUES (11,'include','edulcni',1,0,0,0,0,0,0,0,0,0,0,'','','','post',0);
INSERT INTO `wp_relevanssi` VALUES (11,'including','gnidulcni',1,0,0,0,0,0,0,0,0,0,0,'','','','post',0);
INSERT INTO `wp_relevanssi` VALUES (11,'individual','laudividni',1,0,0,0,0,0,0,0,0,0,0,'','','','post',0);
INSERT INTO `wp_relevanssi` VALUES (11,'injuries','seirujni',1,0,0,0,0,0,0,0,0,0,0,'','','','post',0);
INSERT INTO `wp_relevanssi` VALUES (11,'inpatient','tneitapni',3,0,0,0,0,0,0,0,0,0,0,'','','','post',0);
INSERT INTO `wp_relevanssi` VALUES (11,'knee','eenk',2,0,0,0,0,0,0,0,0,0,0,'','','','post',0);
INSERT INTO `wp_relevanssi` VALUES (11,'learn','nrael',1,0,0,0,0,0,0,0,0,0,0,'','','','post',0);
INSERT INTO `wp_relevanssi` VALUES (11,'leverage','egarevel',1,0,0,0,0,0,0,0,0,0,0,'','','','post',0);
INSERT INTO `wp_relevanssi` VALUES (11,'location','noitacol',1,0,0,0,0,0,0,0,0,0,0,'','','','post',0);
INSERT INTO `wp_relevanssi` VALUES (11,'major','rojam',1,0,0,0,0,0,0,0,0,0,0,'','','','post',0);
INSERT INTO `wp_relevanssi` VALUES (11,'medicine','enicidem',1,0,0,0,0,0,0,0,0,0,0,'','','','post',0);
INSERT INTO `wp_relevanssi` VALUES (11,'needs','sdeen',1,0,0,0,0,0,0,0,0,0,0,'','','','post',0);
INSERT INTO `wp_relevanssi` VALUES (11,'network','krowten',1,0,0,0,0,0,0,0,0,0,0,'','','','post',0);
INSERT INTO `wp_relevanssi` VALUES (11,'neurological','lacigoloruen',1,0,0,0,0,0,0,0,0,0,0,'','','','post',0);
INSERT INTO `wp_relevanssi` VALUES (11,'occupational','lanoitapucco',1,0,0,0,0,0,0,0,0,0,0,'','','','post',0);
INSERT INTO `wp_relevanssi` VALUES (11,'offer','reffo',1,0,0,0,0,0,0,0,0,0,0,'','','','post',0);
INSERT INTO `wp_relevanssi` VALUES (11,'orthopedic','cidepohtro',5,0,0,0,0,0,0,0,0,0,0,'','','','post',0);
INSERT INTO `wp_relevanssi` VALUES (11,'orthopedics','scidepohtro',1,0,0,0,0,0,0,0,0,0,0,'','','','post',0);
INSERT INTO `wp_relevanssi` VALUES (11,'parkinson','nosnikrap',1,0,0,0,0,0,0,0,0,0,0,'','','','post',0);
INSERT INTO `wp_relevanssi` VALUES (11,'patient','tneitap',1,0,0,0,0,0,0,0,0,0,0,'','','','post',0);
INSERT INTO `wp_relevanssi` VALUES (11,'physical','lacisyhp',2,0,0,0,0,0,0,0,0,0,0,'','','','post',0);
INSERT INTO `wp_relevanssi` VALUES (11,'program','margorp',1,0,0,0,0,0,0,0,0,0,0,'','','','post',0);
INSERT INTO `wp_relevanssi` VALUES (11,'programs','smargorp',1,0,0,0,0,0,0,0,0,0,0,'','','','post',0);
INSERT INTO `wp_relevanssi` VALUES (11,'provide','edivorp',1,0,0,0,0,0,0,0,0,0,0,'','','','post',0);
INSERT INTO `wp_relevanssi` VALUES (11,'providers','sredivorp',1,0,0,0,0,0,0,0,0,0,0,'','','','post',0);
INSERT INTO `wp_relevanssi` VALUES (11,'providing','gnidivorp',1,0,0,0,0,0,0,0,0,0,0,'','','','post',0);
INSERT INTO `wp_relevanssi` VALUES (11,'receive','eviecer',1,0,0,0,0,0,0,0,0,0,0,'','','','post',0);
INSERT INTO `wp_relevanssi` VALUES (11,'recovering','gnirevocer',1,0,0,0,0,0,0,0,0,0,0,'','','','post',0);
INSERT INTO `wp_relevanssi` VALUES (11,'rehab','baher',1,0,0,0,0,0,0,0,0,0,0,'','','','post',0);
INSERT INTO `wp_relevanssi` VALUES (11,'rehabilitation','noitatilibaher',3,0,0,0,0,0,0,0,0,0,0,'','','','post',0);
INSERT INTO `wp_relevanssi` VALUES (11,'replacement','tnemecalper',1,0,0,0,0,0,0,0,0,0,0,'','','','post',0);
INSERT INTO `wp_relevanssi` VALUES (11,'serve','evres',1,0,0,0,0,0,0,0,0,0,0,'','','','post',0);
INSERT INTO `wp_relevanssi` VALUES (11,'service','ecivres',1,0,0,0,0,0,0,0,0,0,0,'','','','post',0);
INSERT INTO `wp_relevanssi` VALUES (11,'services','secivres',3,1,0,0,0,0,0,0,0,0,0,'','','','post',0);
INSERT INTO `wp_relevanssi` VALUES (11,'shoulder','redluohs',1,0,0,0,0,0,0,0,0,0,0,'','','','post',0);
INSERT INTO `wp_relevanssi` VALUES (11,'specialized','dezilaiceps',3,0,0,0,0,0,0,0,0,0,0,'','','','post',0);
INSERT INTO `wp_relevanssi` VALUES (11,'specialty','ytlaiceps',3,0,0,0,0,0,0,0,0,0,0,'','','','post',0);
INSERT INTO `wp_relevanssi` VALUES (11,'speech','hceeps',1,0,0,0,0,0,0,0,0,0,0,'','','','post',0);
INSERT INTO `wp_relevanssi` VALUES (11,'spinal','lanips',2,0,0,0,0,0,0,0,0,0,0,'','','','post',0);
INSERT INTO `wp_relevanssi` VALUES (11,'spine','enips',1,0,0,0,0,0,0,0,0,0,0,'','','','post',0);
INSERT INTO `wp_relevanssi` VALUES (11,'sports','strops',1,0,0,0,0,0,0,0,0,0,0,'','','','post',0);
INSERT INTO `wp_relevanssi` VALUES (11,'spring','gnirps',2,0,0,0,0,0,0,0,0,0,0,'','','','post',0);
INSERT INTO `wp_relevanssi` VALUES (11,'stroke','ekorts',1,0,0,0,0,0,0,0,0,0,0,'','','','post',0);
INSERT INTO `wp_relevanssi` VALUES (11,'suffering','gnireffus',1,0,0,0,0,0,0,0,0,0,0,'','','','post',0);
INSERT INTO `wp_relevanssi` VALUES (11,'surgeries','seiregrus',1,0,0,0,0,0,0,0,0,0,0,'','','','post',0);
INSERT INTO `wp_relevanssi` VALUES (11,'surgery','yregrus',4,0,0,0,0,0,0,0,0,0,0,'','','','post',0);
INSERT INTO `wp_relevanssi` VALUES (11,'team','maet',2,0,0,0,0,0,0,0,0,0,0,'','','','post',0);
INSERT INTO `wp_relevanssi` VALUES (11,'therapy','ypareht',3,0,0,0,0,0,0,0,0,0,0,'','','','post',0);
INSERT INTO `wp_relevanssi` VALUES (11,'trauma','amuart',1,0,0,0,0,0,0,0,0,0,0,'','','','post',0);
INSERT INTO `wp_relevanssi` VALUES (11,'treatments','stnemtaert',1,0,0,0,0,0,0,0,0,0,0,'','','','post',0);
INSERT INTO `wp_relevanssi` VALUES (11,'used','desu',1,0,0,0,0,0,0,0,0,0,0,'','','','post',0);
INSERT INTO `wp_relevanssi` VALUES (11,'valley','yellav',5,0,0,0,0,0,0,0,0,0,0,'','','','post',0);
INSERT INTO `wp_relevanssi` VALUES (11,'victims','smitciv',1,0,0,0,0,0,0,0,0,0,0,'','','','post',0);
INSERT INTO `wp_relevanssi` VALUES (11,'work','krow',1,0,0,0,0,0,0,0,0,0,0,'','','','post',0);
INSERT INTO `wp_relevanssi` VALUES (13,'702','207',2,0,0,0,0,0,0,0,0,0,0,'','','','post',0);
INSERT INTO `wp_relevanssi` VALUES (13,'7100','0017',2,0,0,0,0,0,0,0,0,0,0,'','','','post',0);
INSERT INTO `wp_relevanssi` VALUES (13,'777','777',2,0,0,0,0,0,0,0,0,0,0,'','','','post',0);
INSERT INTO `wp_relevanssi` VALUES (13,'8656','6568',1,0,0,0,0,0,0,0,0,0,0,'','','','post',0);
INSERT INTO `wp_relevanssi` VALUES (13,'89148','84198',1,0,0,0,0,0,0,0,0,0,0,'','','','post',0);
INSERT INTO `wp_relevanssi` VALUES (13,'center','retnec',1,0,0,0,0,0,0,0,0,0,0,'','','','post',0);
INSERT INTO `wp_relevanssi` VALUES (13,'close','esolc',1,0,0,0,0,0,0,0,0,0,0,'','','','post',0);
INSERT INTO `wp_relevanssi` VALUES (13,'contact','tcatnoc',0,1,0,0,0,0,0,0,0,0,0,'','','','post',0);
INSERT INTO `wp_relevanssi` VALUES (13,'directions','snoitcerid',1,0,0,0,0,0,0,0,0,0,0,'','','','post',0);
INSERT INTO `wp_relevanssi` VALUES (13,'form','mrof',1,0,0,0,0,0,0,0,0,0,0,'','','','post',0);
INSERT INTO `wp_relevanssi` VALUES (13,'health','htlaeh',1,1,0,0,0,0,0,0,0,0,0,'','','','post',0);
INSERT INTO `wp_relevanssi` VALUES (13,'hospital','latipsoh',3,1,0,0,0,0,0,0,0,0,0,'','','','post',0);
INSERT INTO `wp_relevanssi` VALUES (13,'information','noitamrofni',1,0,0,0,0,0,0,0,0,0,0,'','','','post',0);
INSERT INTO `wp_relevanssi` VALUES (13,'lane','enal',1,0,0,0,0,0,0,0,0,0,0,'','','','post',0);
INSERT INTO `wp_relevanssi` VALUES (13,'las','sal',2,0,0,0,0,0,0,0,0,0,0,'','','','post',0);
INSERT INTO `wp_relevanssi` VALUES (13,'like','ekil',1,0,0,0,0,0,0,0,0,0,0,'','','','post',0);
INSERT INTO `wp_relevanssi` VALUES (13,'located','detacol',1,0,0,0,0,0,0,0,0,0,0,'','','','post',0);
INSERT INTO `wp_relevanssi` VALUES (13,'medical','lacidem',1,0,0,0,0,0,0,0,0,0,0,'','','','post',0);
INSERT INTO `wp_relevanssi` VALUES (13,'patrick','kcirtap',1,0,0,0,0,0,0,0,0,0,0,'','','','post',0);
INSERT INTO `wp_relevanssi` VALUES (13,'southwest','tsewhtuos',1,0,0,0,0,0,0,0,0,0,0,'','','','post',0);
INSERT INTO `wp_relevanssi` VALUES (13,'specialty','ytlaiceps',1,1,0,0,0,0,0,0,0,0,0,'','','','post',0);
INSERT INTO `wp_relevanssi` VALUES (13,'spring','gnirps',1,0,0,0,0,0,0,0,0,0,0,'','','','post',0);
INSERT INTO `wp_relevanssi` VALUES (13,'valley','yellav',2,1,0,0,0,0,0,0,0,0,0,'','','','post',0);
INSERT INTO `wp_relevanssi` VALUES (13,'vegas','sagev',2,0,0,0,0,0,0,0,0,0,0,'','','','post',0);
INSERT INTO `wp_relevanssi` VALUES (13,'west','tsew',1,0,0,0,0,0,0,0,0,0,0,'','','','post',0);
INSERT INTO `wp_relevanssi` VALUES (55,'able','elba',2,0,0,0,0,0,0,0,0,0,0,'','','','post',0);
INSERT INTO `wp_relevanssi` VALUES (55,'accident','tnedicca',1,0,0,0,0,0,0,0,0,0,0,'','','','post',0);
INSERT INTO `wp_relevanssi` VALUES (55,'according','gnidrocca',2,0,0,0,0,0,0,0,0,0,0,'','','','post',0);
INSERT INTO `wp_relevanssi` VALUES (55,'achilles','sellihca',1,0,0,0,0,0,0,0,0,0,0,'','','','post',0);
INSERT INTO `wp_relevanssi` VALUES (55,'acl','lca',1,0,0,0,0,0,0,0,0,0,0,'','','','post',0);
INSERT INTO `wp_relevanssi` VALUES (55,'activities','seitivitca',3,0,0,0,0,0,0,0,0,0,0,'','','','post',0);
INSERT INTO `wp_relevanssi` VALUES (55,'additional','lanoitidda',1,0,0,0,0,0,0,0,0,0,0,'','','','post',0);
INSERT INTO `wp_relevanssi` VALUES (55,'address','sserdda',1,0,0,0,0,0,0,0,0,0,0,'','','','post',0);
INSERT INTO `wp_relevanssi` VALUES (55,'adjust','tsujda',1,0,0,0,0,0,0,0,0,0,0,'','','','post',0);
INSERT INTO `wp_relevanssi` VALUES (55,'adults','stluda',1,0,0,0,0,0,0,0,0,0,0,'','','','post',0);
INSERT INTO `wp_relevanssi` VALUES (55,'advanced','decnavda',2,0,0,0,0,0,0,0,0,0,0,'','','','post',0);
INSERT INTO `wp_relevanssi` VALUES (55,'age','ega',1,0,0,0,0,0,0,0,0,0,0,'','','','post',0);
INSERT INTO `wp_relevanssi` VALUES (55,'aging','gniga',2,0,0,0,0,0,0,0,0,0,0,'','','','post',0);
INSERT INTO `wp_relevanssi` VALUES (55,'alternative','evitanretla',1,0,0,0,0,0,0,0,0,0,0,'','','','post',0);
INSERT INTO `wp_relevanssi` VALUES (55,'ankle','elkna',2,0,0,0,0,0,0,0,0,0,0,'','','','post',0);
INSERT INTO `wp_relevanssi` VALUES (55,'anterior','roiretna',2,0,0,0,0,0,0,0,0,0,0,'','','','post',0);
INSERT INTO `wp_relevanssi` VALUES (55,'approach','hcaorppa',1,0,0,0,0,0,0,0,0,0,0,'','','','post',0);
INSERT INTO `wp_relevanssi` VALUES (55,'area','aera',1,0,0,0,0,0,0,0,0,0,0,'','','','post',0);
INSERT INTO `wp_relevanssi` VALUES (55,'arthritis','sitirhtra',2,0,0,0,0,0,0,0,0,0,0,'','','','post',0);
INSERT INTO `wp_relevanssi` VALUES (55,'arthroscopic','cipocsorhtra',1,0,0,0,0,0,0,0,0,0,0,'','','','post',0);
INSERT INTO `wp_relevanssi` VALUES (55,'artificial','laicifitra',1,0,0,0,0,0,0,0,0,0,0,'','','','post',0);
INSERT INTO `wp_relevanssi` VALUES (55,'ask','ksa',1,0,0,0,0,0,0,0,0,0,0,'','','','post',0);
INSERT INTO `wp_relevanssi` VALUES (55,'assist','tsissa',1,0,0,0,0,0,0,0,0,0,0,'','','','post',0);
INSERT INTO `wp_relevanssi` VALUES (55,'associated','detaicossa',2,0,0,0,0,0,0,0,0,0,0,'','','','post',0);
INSERT INTO `wp_relevanssi` VALUES (55,'attacks','skcatta',1,0,0,0,0,0,0,0,0,0,0,'','','','post',0);
INSERT INTO `wp_relevanssi` VALUES (55,'best','tseb',2,0,0,0,0,0,0,0,0,0,0,'','','','post',0);
INSERT INTO `wp_relevanssi` VALUES (55,'body','ydob',2,0,0,0,0,0,0,0,0,0,0,'','','','post',0);
INSERT INTO `wp_relevanssi` VALUES (55,'bone','enob',1,0,0,0,0,0,0,0,0,0,0,'','','','post',0);
INSERT INTO `wp_relevanssi` VALUES (55,'bones','senob',2,0,0,0,0,0,0,0,0,0,0,'','','','post',0);
INSERT INTO `wp_relevanssi` VALUES (55,'care','erac',3,0,0,0,0,0,0,0,0,0,0,'','','','post',0);
INSERT INTO `wp_relevanssi` VALUES (55,'cartilage','egalitrac',3,0,0,0,0,0,0,0,0,0,0,'','','','post',0);
INSERT INTO `wp_relevanssi` VALUES (55,'cases','sesac',1,0,0,0,0,0,0,0,0,0,0,'','','','post',0);
INSERT INTO `wp_relevanssi` VALUES (55,'caused','desuac',1,0,0,0,0,0,0,0,0,0,0,'','','','post',0);
INSERT INTO `wp_relevanssi` VALUES (55,'choice','eciohc',1,0,0,0,0,0,0,0,0,0,0,'','','','post',0);
INSERT INTO `wp_relevanssi` VALUES (55,'chronic','cinorhc',1,0,0,0,0,0,0,0,0,0,0,'','','','post',0);
INSERT INTO `wp_relevanssi` VALUES (55,'collateral','laretalloc',1,0,0,0,0,0,0,0,0,0,0,'','','','post',0);
INSERT INTO `wp_relevanssi` VALUES (55,'coming','gnimoc',1,0,0,0,0,0,0,0,0,0,0,'','','','post',0);
INSERT INTO `wp_relevanssi` VALUES (55,'common','nommoc',1,0,0,0,0,0,0,0,0,0,0,'','','','post',0);
INSERT INTO `wp_relevanssi` VALUES (55,'commonly','ylnommoc',1,0,0,0,0,0,0,0,0,0,0,'','','','post',0);
INSERT INTO `wp_relevanssi` VALUES (55,'comprehensive','evisneherpmoc',2,0,0,0,0,0,0,0,0,0,0,'','','','post',0);
INSERT INTO `wp_relevanssi` VALUES (55,'condition','noitidnoc',2,0,0,0,0,0,0,0,0,0,0,'','','','post',0);
INSERT INTO `wp_relevanssi` VALUES (55,'conditions','snoitidnoc',3,0,0,0,0,0,0,0,0,0,0,'','','','post',0);
INSERT INTO `wp_relevanssi` VALUES (55,'covers','srevoc',2,0,0,0,0,0,0,0,0,0,0,'','','','post',0);
INSERT INTO `wp_relevanssi` VALUES (55,'cruciate','etaicurc',1,0,0,0,0,0,0,0,0,0,0,'','','','post',0);
INSERT INTO `wp_relevanssi` VALUES (55,'cuff','ffuc',1,0,0,0,0,0,0,0,0,0,0,'','','','post',0);
INSERT INTO `wp_relevanssi` VALUES (55,'daily','yliad',2,0,0,0,0,0,0,0,0,0,0,'','','','post',0);
INSERT INTO `wp_relevanssi` VALUES (55,'day','yad',1,0,0,0,0,0,0,0,0,0,0,'','','','post',0);
INSERT INTO `wp_relevanssi` VALUES (55,'deemed','demeed',1,0,0,0,0,0,0,0,0,0,0,'','','','post',0);
INSERT INTO `wp_relevanssi` VALUES (55,'deformities','seitimrofed',1,0,0,0,0,0,0,0,0,0,0,'','','','post',0);
INSERT INTO `wp_relevanssi` VALUES (55,'degenerative','evitareneged',1,0,0,0,0,0,0,0,0,0,0,'','','','post',0);
INSERT INTO `wp_relevanssi` VALUES (55,'diagnose','esongaid',1,0,0,0,0,0,0,0,0,0,0,'','','','post',0);
INSERT INTO `wp_relevanssi` VALUES (55,'diagnosis','sisongaid',1,0,0,0,0,0,0,0,0,0,0,'','','','post',0);
INSERT INTO `wp_relevanssi` VALUES (55,'different','tnereffid',1,0,0,0,0,0,0,0,0,0,0,'','','','post',0);
INSERT INTO `wp_relevanssi` VALUES (55,'discharge','egrahcsid',1,0,0,0,0,0,0,0,0,0,0,'','','','post',0);
INSERT INTO `wp_relevanssi` VALUES (55,'diseased','desaesid',1,0,0,0,0,0,0,0,0,0,0,'','','','post',0);
INSERT INTO `wp_relevanssi` VALUES (55,'disorder','redrosid',1,0,0,0,0,0,0,0,0,0,0,'','','','post',0);
INSERT INTO `wp_relevanssi` VALUES (55,'disorders','sredrosid',1,0,0,0,0,0,0,0,0,0,0,'','','','post',0);
INSERT INTO `wp_relevanssi` VALUES (55,'doctor','rotcod',2,0,0,0,0,0,0,0,0,0,0,'','','','post',0);
INSERT INTO `wp_relevanssi` VALUES (55,'don','nod',1,0,0,0,0,0,0,0,0,0,0,'','','','post',0);
INSERT INTO `wp_relevanssi` VALUES (55,'elbow','woble',1,0,0,0,0,0,0,0,0,0,0,'','','','post',0);
INSERT INTO `wp_relevanssi` VALUES (55,'enables','selbane',1,0,0,0,0,0,0,0,0,0,0,'','','','post',0);
INSERT INTO `wp_relevanssi` VALUES (55,'ends','sdne',1,0,0,0,0,0,0,0,0,0,0,'','','','post',0);
INSERT INTO `wp_relevanssi` VALUES (55,'endurance','ecnarudne',1,0,0,0,0,0,0,0,0,0,0,'','','','post',0);
INSERT INTO `wp_relevanssi` VALUES (55,'equipment','tnempiuqe',2,0,0,0,0,0,0,0,0,0,0,'','','','post',0);
INSERT INTO `wp_relevanssi` VALUES (55,'exercise','esicrexe',3,0,0,0,0,0,0,0,0,0,0,'','','','post',0);
INSERT INTO `wp_relevanssi` VALUES (55,'expected','detcepxe',1,0,0,0,0,0,0,0,0,0,0,'','','','post',0);
INSERT INTO `wp_relevanssi` VALUES (55,'fall','llaf',2,0,0,0,0,0,0,0,0,0,0,'','','','post',0);
INSERT INTO `wp_relevanssi` VALUES (55,'faster','retsaf',1,0,0,0,0,0,0,0,0,0,0,'','','','post',0);
INSERT INTO `wp_relevanssi` VALUES (55,'finds','sdnif',1,0,0,0,0,0,0,0,0,0,0,'','','','post',0);
INSERT INTO `wp_relevanssi` VALUES (55,'focus','sucof',2,0,0,0,0,0,0,0,0,0,0,'','','','post',0);
INSERT INTO `wp_relevanssi` VALUES (55,'following','gniwollof',2,0,0,0,0,0,0,0,0,0,0,'','','','post',0);
INSERT INTO `wp_relevanssi` VALUES (55,'foot','toof',1,0,0,0,0,0,0,0,0,0,0,'','','','post',0);
INSERT INTO `wp_relevanssi` VALUES (55,'form','mrof',2,0,0,0,0,0,0,0,0,0,0,'','','','post',0);
INSERT INTO `wp_relevanssi` VALUES (55,'forms','smrof',1,0,0,0,0,0,0,0,0,0,0,'','','','post',0);
INSERT INTO `wp_relevanssi` VALUES (55,'fracture','erutcarf',1,0,0,0,0,0,0,0,0,0,0,'','','','post',0);
INSERT INTO `wp_relevanssi` VALUES (55,'friction','noitcirf',1,0,0,0,0,0,0,0,0,0,0,'','','','post',0);
INSERT INTO `wp_relevanssi` VALUES (55,'function','noitcnuf',1,0,0,0,0,0,0,0,0,0,0,'','','','post',0);
INSERT INTO `wp_relevanssi` VALUES (55,'given','nevig',1,0,0,0,0,0,0,0,0,0,0,'','','','post',0);
INSERT INTO `wp_relevanssi` VALUES (55,'goal','laog',1,0,0,0,0,0,0,0,0,0,0,'','','','post',0);
INSERT INTO `wp_relevanssi` VALUES (55,'hand','dnah',1,0,0,0,0,0,0,0,0,0,0,'','','','post',0);
INSERT INTO `wp_relevanssi` VALUES (55,'heal','laeh',1,0,0,0,0,0,0,0,0,0,0,'','','','post',0);
INSERT INTO `wp_relevanssi` VALUES (55,'health','htlaeh',7,0,0,0,0,0,0,0,0,0,0,'','','','post',0);
INSERT INTO `wp_relevanssi` VALUES (55,'help','pleh',3,0,0,0,0,0,0,0,0,0,0,'','','','post',0);
INSERT INTO `wp_relevanssi` VALUES (55,'hip','pih',10,0,0,0,0,0,0,0,0,0,0,'','','','post',0);
INSERT INTO `wp_relevanssi` VALUES (55,'holes','seloh',1,0,0,0,0,0,0,0,0,0,0,'','','','post',0);
INSERT INTO `wp_relevanssi` VALUES (55,'home','emoh',1,0,0,0,0,0,0,0,0,0,0,'','','','post',0);
INSERT INTO `wp_relevanssi` VALUES (55,'hospital','latipsoh',7,0,0,0,0,0,0,0,0,0,0,'','','','post',0);
INSERT INTO `wp_relevanssi` VALUES (55,'hurry','yrruh',1,0,0,0,0,0,0,0,0,0,0,'','','','post',0);
INSERT INTO `wp_relevanssi` VALUES (55,'immune','enummi',1,0,0,0,0,0,0,0,0,0,0,'','','','post',0);
INSERT INTO `wp_relevanssi` VALUES (55,'improve','evorpmi',1,0,0,0,0,0,0,0,0,0,0,'','','','post',0);
INSERT INTO `wp_relevanssi` VALUES (55,'incision','noisicni',1,0,0,0,0,0,0,0,0,0,0,'','','','post',0);
INSERT INTO `wp_relevanssi` VALUES (55,'incisions','snoisicni',1,0,0,0,0,0,0,0,0,0,0,'','','','post',0);
INSERT INTO `wp_relevanssi` VALUES (55,'including','gnidulcni',1,0,0,0,0,0,0,0,0,0,0,'','','','post',0);
INSERT INTO `wp_relevanssi` VALUES (55,'increase','esaercni',2,0,0,0,0,0,0,0,0,0,0,'','','','post',0);
INSERT INTO `wp_relevanssi` VALUES (55,'indications','snoitacidni',1,0,0,0,0,0,0,0,0,0,0,'','','','post',0);
INSERT INTO `wp_relevanssi` VALUES (55,'individual','laudividni',1,0,0,0,0,0,0,0,0,0,0,'','','','post',0);
INSERT INTO `wp_relevanssi` VALUES (55,'injuries','seirujni',7,0,0,0,0,0,0,0,0,0,0,'','','','post',0);
INSERT INTO `wp_relevanssi` VALUES (55,'injury','yrujni',4,0,0,0,0,0,0,0,0,0,0,'','','','post',0);
INSERT INTO `wp_relevanssi` VALUES (55,'institutes','setutitsni',1,0,0,0,0,0,0,0,0,0,0,'','','','post',0);
INSERT INTO `wp_relevanssi` VALUES (55,'instruments','stnemurtsni',1,0,0,0,0,0,0,0,0,0,0,'','','','post',0);
INSERT INTO `wp_relevanssi` VALUES (55,'intact','tcatni',1,0,0,0,0,0,0,0,0,0,0,'','','','post',0);
INSERT INTO `wp_relevanssi` VALUES (55,'invasive','evisavni',5,0,0,0,0,0,0,0,0,0,0,'','','','post',0);
INSERT INTO `wp_relevanssi` VALUES (55,'involves','sevlovni',1,0,0,0,0,0,0,0,0,0,0,'','','','post',0);
INSERT INTO `wp_relevanssi` VALUES (55,'involving','gnivlovni',2,0,0,0,0,0,0,0,0,0,0,'','','','post',0);
INSERT INTO `wp_relevanssi` VALUES (55,'joint','tnioj',10,0,0,0,0,0,0,0,0,0,0,'','','','post',0);
INSERT INTO `wp_relevanssi` VALUES (55,'joints','stnioj',4,0,0,0,0,0,0,0,0,0,0,'','','','post',0);
INSERT INTO `wp_relevanssi` VALUES (55,'knee','eenk',10,0,0,0,0,0,0,0,0,0,0,'','','','post',0);
INSERT INTO `wp_relevanssi` VALUES (55,'leaves','sevael',1,0,0,0,0,0,0,0,0,0,0,'','','','post',0);
INSERT INTO `wp_relevanssi` VALUES (55,'length','htgnel',1,0,0,0,0,0,0,0,0,0,0,'','','','post',0);
INSERT INTO `wp_relevanssi` VALUES (55,'ligament','tnemagil',2,0,0,0,0,0,0,0,0,0,0,'','','','post',0);
INSERT INTO `wp_relevanssi` VALUES (55,'ligaments','stnemagil',2,0,0,0,0,0,0,0,0,0,0,'','','','post',0);
INSERT INTO `wp_relevanssi` VALUES (55,'located','detacol',1,0,0,0,0,0,0,0,0,0,0,'','','','post',0);
INSERT INTO `wp_relevanssi` VALUES (55,'looking','gnikool',1,0,0,0,0,0,0,0,0,0,0,'','','','post',0);
INSERT INTO `wp_relevanssi` VALUES (55,'magnified','deifingam',1,0,0,0,0,0,0,0,0,0,0,'','','','post',0);
INSERT INTO `wp_relevanssi` VALUES (55,'make','ekam',2,0,0,0,0,0,0,0,0,0,0,'','','','post',0);
INSERT INTO `wp_relevanssi` VALUES (55,'makes','sekam',1,0,0,0,0,0,0,0,0,0,0,'','','','post',0);
INSERT INTO `wp_relevanssi` VALUES (55,'management','tnemeganam',1,0,0,0,0,0,0,0,0,0,0,'','','','post',0);
INSERT INTO `wp_relevanssi` VALUES (55,'materials','slairetam',1,0,0,0,0,0,0,0,0,0,0,'','','','post',0);
INSERT INTO `wp_relevanssi` VALUES (55,'mcl','lcm',1,0,0,0,0,0,0,0,0,0,0,'','','','post',0);
INSERT INTO `wp_relevanssi` VALUES (55,'medial','laidem',1,0,0,0,0,0,0,0,0,0,0,'','','','post',0);
INSERT INTO `wp_relevanssi` VALUES (55,'medication','noitacidem',1,0,0,0,0,0,0,0,0,0,0,'','','','post',0);
INSERT INTO `wp_relevanssi` VALUES (55,'medicine','enicidem',4,0,0,0,0,0,0,0,0,0,0,'','','','post',0);
INSERT INTO `wp_relevanssi` VALUES (55,'membranes','senarbmem',1,0,0,0,0,0,0,0,0,0,0,'','','','post',0);
INSERT INTO `wp_relevanssi` VALUES (55,'million','noillim',3,0,0,0,0,0,0,0,0,0,0,'','','','post',0);
INSERT INTO `wp_relevanssi` VALUES (55,'minimal','laminim',1,0,0,0,0,0,0,0,0,0,0,'','','','post',0);
INSERT INTO `wp_relevanssi` VALUES (55,'minimally','yllaminim',5,0,0,0,0,0,0,0,0,0,0,'','','','post',0);
INSERT INTO `wp_relevanssi` VALUES (55,'moving','gnivom',1,0,0,0,0,0,0,0,0,0,0,'','','','post',0);
INSERT INTO `wp_relevanssi` VALUES (55,'multiple','elpitlum',1,0,0,0,0,0,0,0,0,0,0,'','','','post',0);
INSERT INTO `wp_relevanssi` VALUES (55,'muscle','elcsum',1,0,0,0,0,0,0,0,0,0,0,'','','','post',0);
INSERT INTO `wp_relevanssi` VALUES (55,'muscles','selcsum',2,0,0,0,0,0,0,0,0,0,0,'','','','post',0);
INSERT INTO `wp_relevanssi` VALUES (55,'musculoskeletal','lateleksolucsum',2,0,0,0,0,0,0,0,0,0,0,'','','','post',0);
INSERT INTO `wp_relevanssi` VALUES (55,'national','lanoitan',1,0,0,0,0,0,0,0,0,0,0,'','','','post',0);
INSERT INTO `wp_relevanssi` VALUES (55,'nearly','ylraen',2,0,0,0,0,0,0,0,0,0,0,'','','','post',0);
INSERT INTO `wp_relevanssi` VALUES (55,'necessary','yrassecen',2,0,0,0,0,0,0,0,0,0,0,'','','','post',0);
INSERT INTO `wp_relevanssi` VALUES (55,'need','deen',1,0,0,0,0,0,0,0,0,0,0,'','','','post',0);
INSERT INTO `wp_relevanssi` VALUES (55,'nerves','sevren',1,0,0,0,0,0,0,0,0,0,0,'','','','post',0);
INSERT INTO `wp_relevanssi` VALUES (55,'new','wen',2,0,0,0,0,0,0,0,0,0,0,'','','','post',0);
INSERT INTO `wp_relevanssi` VALUES (55,'nih','hin',2,0,0,0,0,0,0,0,0,0,0,'','','','post',0);
INSERT INTO `wp_relevanssi` VALUES (55,'occupational','lanoitapucco',1,0,0,0,0,0,0,0,0,0,0,'','','','post',0);
INSERT INTO `wp_relevanssi` VALUES (55,'offer','reffo',3,0,0,0,0,0,0,0,0,0,0,'','','','post',0);
INSERT INTO `wp_relevanssi` VALUES (55,'options','snoitpo',1,0,0,0,0,0,0,0,0,0,0,'','','','post',0);
INSERT INTO `wp_relevanssi` VALUES (55,'orthopedic','cidepohtro',7,1,0,0,0,0,0,0,0,0,0,'','','','post',0);
INSERT INTO `wp_relevanssi` VALUES (55,'osteoarthritis','sitirhtraoetso',4,0,0,0,0,0,0,0,0,0,0,'','','','post',0);
INSERT INTO `wp_relevanssi` VALUES (55,'overuse','esurevo',2,0,0,0,0,0,0,0,0,0,0,'','','','post',0);
INSERT INTO `wp_relevanssi` VALUES (55,'pain','niap',1,0,0,0,0,0,0,0,0,0,0,'','','','post',0);
INSERT INTO `wp_relevanssi` VALUES (55,'particular','ralucitrap',1,0,0,0,0,0,0,0,0,0,0,'','','','post',0);
INSERT INTO `wp_relevanssi` VALUES (55,'patients','stneitap',2,0,0,0,0,0,0,0,0,0,0,'','','','post',0);
INSERT INTO `wp_relevanssi` VALUES (55,'pattern','nrettap',1,0,0,0,0,0,0,0,0,0,0,'','','','post',0);
INSERT INTO `wp_relevanssi` VALUES (55,'pencil','licnep',1,0,0,0,0,0,0,0,0,0,0,'','','','post',0);
INSERT INTO `wp_relevanssi` VALUES (55,'people','elpoep',4,0,0,0,0,0,0,0,0,0,0,'','','','post',0);
INSERT INTO `wp_relevanssi` VALUES (55,'perform','mrofrep',2,0,0,0,0,0,0,0,0,0,0,'','','','post',0);
INSERT INTO `wp_relevanssi` VALUES (55,'performed','demrofrep',2,0,0,0,0,0,0,0,0,0,0,'','','','post',0);
INSERT INTO `wp_relevanssi` VALUES (55,'personalized','dezilanosrep',1,0,0,0,0,0,0,0,0,0,0,'','','','post',0);
INSERT INTO `wp_relevanssi` VALUES (55,'physical','lacisyhp',1,0,0,0,0,0,0,0,0,0,0,'','','','post',0);
INSERT INTO `wp_relevanssi` VALUES (55,'physician','naicisyhp',1,0,0,0,0,0,0,0,0,0,0,'','','','post',0);
INSERT INTO `wp_relevanssi` VALUES (55,'population','noitalupop',1,0,0,0,0,0,0,0,0,0,0,'','','','post',0);
INSERT INTO `wp_relevanssi` VALUES (55,'possible','elbissop',2,0,0,0,0,0,0,0,0,0,0,'','','','post',0);
INSERT INTO `wp_relevanssi` VALUES (55,'prevention','noitneverp',1,0,0,0,0,0,0,0,0,0,0,'','','','post',0);
INSERT INTO `wp_relevanssi` VALUES (55,'procedure','erudecorp',2,0,0,0,0,0,0,0,0,0,0,'','','','post',0);
INSERT INTO `wp_relevanssi` VALUES (55,'procedures','serudecorp',6,0,0,0,0,0,0,0,0,0,0,'','','','post',0);
INSERT INTO `wp_relevanssi` VALUES (55,'program','margorp',1,0,0,0,0,0,0,0,0,0,0,'','','','post',0);
INSERT INTO `wp_relevanssi` VALUES (55,'proper','reporp',2,0,0,0,0,0,0,0,0,0,0,'','','','post',0);
INSERT INTO `wp_relevanssi` VALUES (55,'provide','edivorp',2,0,0,0,0,0,0,0,0,0,0,'','','','post',0);
INSERT INTO `wp_relevanssi` VALUES (55,'quickly','ylkciuq',1,0,0,0,0,0,0,0,0,0,0,'','','','post',0);
INSERT INTO `wp_relevanssi` VALUES (55,'ready','ydaer',1,0,0,0,0,0,0,0,0,0,0,'','','','post',0);
INSERT INTO `wp_relevanssi` VALUES (55,'recommend','dnemmocer',1,0,0,0,0,0,0,0,0,0,0,'','','','post',0);
INSERT INTO `wp_relevanssi` VALUES (55,'recovering','gnirevocer',1,0,0,0,0,0,0,0,0,0,0,'','','','post',0);
INSERT INTO `wp_relevanssi` VALUES (55,'recovery','yrevocer',2,0,0,0,0,0,0,0,0,0,0,'','','','post',0);
INSERT INTO `wp_relevanssi` VALUES (55,'rehabilitation','noitatilibaher',4,0,0,0,0,0,0,0,0,0,0,'','','','post',0);
INSERT INTO `wp_relevanssi` VALUES (55,'related','detaler',2,0,0,0,0,0,0,0,0,0,0,'','','','post',0);
INSERT INTO `wp_relevanssi` VALUES (55,'relieve','eveiler',1,0,0,0,0,0,0,0,0,0,0,'','','','post',0);
INSERT INTO `wp_relevanssi` VALUES (55,'replacement','tnemecalper',9,0,0,0,0,0,0,0,0,0,0,'','','','post',0);
INSERT INTO `wp_relevanssi` VALUES (55,'replacing','gnicalper',1,0,0,0,0,0,0,0,0,0,0,'','','','post',0);
INSERT INTO `wp_relevanssi` VALUES (55,'result','tluser',1,0,0,0,0,0,0,0,0,0,0,'','','','post',0);
INSERT INTO `wp_relevanssi` VALUES (55,'resulting','gnitluser',1,0,0,0,0,0,0,0,0,0,0,'','','','post',0);
INSERT INTO `wp_relevanssi` VALUES (55,'results','stluser',1,0,0,0,0,0,0,0,0,0,0,'','','','post',0);
INSERT INTO `wp_relevanssi` VALUES (55,'resume','emuser',1,0,0,0,0,0,0,0,0,0,0,'','','','post',0);
INSERT INTO `wp_relevanssi` VALUES (55,'return','nruter',2,0,0,0,0,0,0,0,0,0,0,'','','','post',0);
INSERT INTO `wp_relevanssi` VALUES (55,'revolutionized','dezinoitulover',1,0,0,0,0,0,0,0,0,0,0,'','','','post',0);
INSERT INTO `wp_relevanssi` VALUES (55,'rheumatoid','diotamuehr',1,0,0,0,0,0,0,0,0,0,0,'','','','post',0);
INSERT INTO `wp_relevanssi` VALUES (55,'rhythmically','yllacimhtyhr',1,0,0,0,0,0,0,0,0,0,0,'','','','post',0);
INSERT INTO `wp_relevanssi` VALUES (55,'right','thgir',2,0,0,0,0,0,0,0,0,0,0,'','','','post',0);
INSERT INTO `wp_relevanssi` VALUES (55,'risks','sksir',2,0,0,0,0,0,0,0,0,0,0,'','','','post',0);
INSERT INTO `wp_relevanssi` VALUES (55,'rotator','rotator',1,0,0,0,0,0,0,0,0,0,0,'','','','post',0);
INSERT INTO `wp_relevanssi` VALUES (55,'safely','ylefas',1,0,0,0,0,0,0,0,0,0,0,'','','','post',0);
INSERT INTO `wp_relevanssi` VALUES (55,'scars','sracs',1,0,0,0,0,0,0,0,0,0,0,'','','','post',0);
INSERT INTO `wp_relevanssi` VALUES (55,'services','secivres',3,0,0,0,0,0,0,0,0,0,0,'','','','post',0);
INSERT INTO `wp_relevanssi` VALUES (55,'severe','ereves',1,0,0,0,0,0,0,0,0,0,0,'','','','post',0);
INSERT INTO `wp_relevanssi` VALUES (55,'shoulder','redluohs',3,0,0,0,0,0,0,0,0,0,0,'','','','post',0);
INSERT INTO `wp_relevanssi` VALUES (55,'site','etis',1,0,0,0,0,0,0,0,0,0,0,'','','','post',0);
INSERT INTO `wp_relevanssi` VALUES (55,'situation','noitautis',1,0,0,0,0,0,0,0,0,0,0,'','','','post',0);
INSERT INTO `wp_relevanssi` VALUES (55,'sized','dezis',1,0,0,0,0,0,0,0,0,0,0,'','','','post',0);
INSERT INTO `wp_relevanssi` VALUES (55,'small','llams',1,0,0,0,0,0,0,0,0,0,0,'','','','post',0);
INSERT INTO `wp_relevanssi` VALUES (55,'smaller','rellams',1,0,0,0,0,0,0,0,0,0,0,'','','','post',0);
INSERT INTO `wp_relevanssi` VALUES (55,'smooth','htooms',1,0,0,0,0,0,0,0,0,0,0,'','','','post',0);
INSERT INTO `wp_relevanssi` VALUES (55,'smoothly','ylhtooms',1,0,0,0,0,0,0,0,0,0,0,'','','','post',0);
INSERT INTO `wp_relevanssi` VALUES (55,'specialists','stsilaiceps',1,0,0,0,0,0,0,0,0,0,0,'','','','post',0);
INSERT INTO `wp_relevanssi` VALUES (55,'specialty','ytlaiceps',7,0,0,0,0,0,0,0,0,0,0,'','','','post',0);
INSERT INTO `wp_relevanssi` VALUES (55,'speed','deeps',1,0,0,0,0,0,0,0,0,0,0,'','','','post',0);
INSERT INTO `wp_relevanssi` VALUES (55,'spend','dneps',1,0,0,0,0,0,0,0,0,0,0,'','','','post',0);
INSERT INTO `wp_relevanssi` VALUES (55,'spinal','lanips',2,0,0,0,0,0,0,0,0,0,0,'','','','post',0);
INSERT INTO `wp_relevanssi` VALUES (55,'spine','enips',2,0,0,0,0,0,0,0,0,0,0,'','','','post',0);
INSERT INTO `wp_relevanssi` VALUES (55,'sport','trops',1,0,0,0,0,0,0,0,0,0,0,'','','','post',0);
INSERT INTO `wp_relevanssi` VALUES (55,'sports','strops',8,0,0,0,0,0,0,0,0,0,0,'','','','post',0);
INSERT INTO `wp_relevanssi` VALUES (55,'step','pets',1,0,0,0,0,0,0,0,0,0,0,'','','','post',0);
INSERT INTO `wp_relevanssi` VALUES (55,'strength','htgnerts',1,0,0,0,0,0,0,0,0,0,0,'','','','post',0);
INSERT INTO `wp_relevanssi` VALUES (55,'surgeon','noegrus',1,0,0,0,0,0,0,0,0,0,0,'','','','post',0);
INSERT INTO `wp_relevanssi` VALUES (55,'surgeons','snoegrus',3,0,0,0,0,0,0,0,0,0,0,'','','','post',0);
INSERT INTO `wp_relevanssi` VALUES (55,'surgery','yregrus',18,1,0,0,0,0,0,0,0,0,0,'','','','post',0);
INSERT INTO `wp_relevanssi` VALUES (55,'surgical','lacigrus',2,0,0,0,0,0,0,0,0,0,0,'','','','post',0);
INSERT INTO `wp_relevanssi` VALUES (55,'symptoms','smotpmys',1,0,0,0,0,0,0,0,0,0,0,'','','','post',0);
INSERT INTO `wp_relevanssi` VALUES (55,'talk','klat',1,0,0,0,0,0,0,0,0,0,0,'','','','post',0);
INSERT INTO `wp_relevanssi` VALUES (55,'team','maet',1,0,0,0,0,0,0,0,0,0,0,'','','','post',0);
INSERT INTO `wp_relevanssi` VALUES (55,'teams','smaet',1,0,0,0,0,0,0,0,0,0,0,'','','','post',0);
INSERT INTO `wp_relevanssi` VALUES (55,'tears','sraet',1,0,0,0,0,0,0,0,0,0,0,'','','','post',0);
INSERT INTO `wp_relevanssi` VALUES (55,'techniques','seuqinhcet',1,0,0,0,0,0,0,0,0,0,0,'','','','post',0);
INSERT INTO `wp_relevanssi` VALUES (55,'technology','ygolonhcet',2,0,0,0,0,0,0,0,0,0,0,'','','','post',0);
INSERT INTO `wp_relevanssi` VALUES (55,'tendon','nodnet',1,0,0,0,0,0,0,0,0,0,0,'','','','post',0);
INSERT INTO `wp_relevanssi` VALUES (55,'tendons','snodnet',1,0,0,0,0,0,0,0,0,0,0,'','','','post',0);
INSERT INTO `wp_relevanssi` VALUES (55,'therapists','stsipareht',1,0,0,0,0,0,0,0,0,0,0,'','','','post',0);
INSERT INTO `wp_relevanssi` VALUES (55,'therapy','ypareht',3,0,0,0,0,0,0,0,0,0,0,'','','','post',0);
INSERT INTO `wp_relevanssi` VALUES (55,'time','emit',2,0,0,0,0,0,0,0,0,0,0,'','','','post',0);
INSERT INTO `wp_relevanssi` VALUES (55,'tiny','ynit',1,0,0,0,0,0,0,0,0,0,0,'','','','post',0);
INSERT INTO `wp_relevanssi` VALUES (55,'tissue','eussit',1,0,0,0,0,0,0,0,0,0,0,'','','','post',0);
INSERT INTO `wp_relevanssi` VALUES (55,'torn','nrot',1,0,0,0,0,0,0,0,0,0,0,'','','','post',0);
INSERT INTO `wp_relevanssi` VALUES (55,'total','latot',3,0,0,0,0,0,0,0,0,0,0,'','','','post',0);
INSERT INTO `wp_relevanssi` VALUES (55,'tough','hguot',1,0,0,0,0,0,0,0,0,0,0,'','','','post',0);
INSERT INTO `wp_relevanssi` VALUES (55,'trauma','amuart',2,0,0,0,0,0,0,0,0,0,0,'','','','post',0);
INSERT INTO `wp_relevanssi` VALUES (55,'treat','taert',1,0,0,0,0,0,0,0,0,0,0,'','','','post',0);
INSERT INTO `wp_relevanssi` VALUES (55,'treatment','tnemtaert',4,0,0,0,0,0,0,0,0,0,0,'','','','post',0);
INSERT INTO `wp_relevanssi` VALUES (55,'type','epyt',1,0,0,0,0,0,0,0,0,0,0,'','','','post',0);
INSERT INTO `wp_relevanssi` VALUES (55,'types','sepyt',1,0,0,0,0,0,0,0,0,0,0,'','','','post',0);
INSERT INTO `wp_relevanssi` VALUES (55,'use','esu',3,0,0,0,0,0,0,0,0,0,0,'','','','post',0);
INSERT INTO `wp_relevanssi` VALUES (55,'used','desu',2,0,0,0,0,0,0,0,0,0,0,'','','','post',0);
INSERT INTO `wp_relevanssi` VALUES (55,'using','gnisu',2,0,0,0,0,0,0,0,0,0,0,'','','','post',0);
INSERT INTO `wp_relevanssi` VALUES (55,'usually','yllausu',1,0,0,0,0,0,0,0,0,0,0,'','','','post',0);
INSERT INTO `wp_relevanssi` VALUES (55,'valley','yellav',6,0,0,0,0,0,0,0,0,0,0,'','','','post',0);
INSERT INTO `wp_relevanssi` VALUES (55,'variety','yteirav',1,0,0,0,0,0,0,0,0,0,0,'','','','post',0);
INSERT INTO `wp_relevanssi` VALUES (55,'vary','yrav',1,0,0,0,0,0,0,0,0,0,0,'','','','post',0);
INSERT INTO `wp_relevanssi` VALUES (55,'video','oediv',1,0,0,0,0,0,0,0,0,0,0,'','','','post',0);
INSERT INTO `wp_relevanssi` VALUES (55,'view','weiv',1,0,0,0,0,0,0,0,0,0,0,'','','','post',0);
INSERT INTO `wp_relevanssi` VALUES (55,'walk','klaw',3,0,0,0,0,0,0,0,0,0,0,'','','','post',0);
INSERT INTO `wp_relevanssi` VALUES (55,'walker','reklaw',3,0,0,0,0,0,0,0,0,0,0,'','','','post',0);
INSERT INTO `wp_relevanssi` VALUES (55,'walking','gniklaw',2,0,0,0,0,0,0,0,0,0,0,'','','','post',0);
INSERT INTO `wp_relevanssi` VALUES (55,'way','yaw',1,0,0,0,0,0,0,0,0,0,0,'','','','post',0);
INSERT INTO `wp_relevanssi` VALUES (55,'weeks','skeew',1,0,0,0,0,0,0,0,0,0,0,'','','','post',0);
INSERT INTO `wp_relevanssi` VALUES (55,'work','krow',1,0,0,0,0,0,0,0,0,0,0,'','','','post',0);
INSERT INTO `wp_relevanssi` VALUES (55,'year','raey',1,0,0,0,0,0,0,0,0,0,0,'','','','post',0);
INSERT INTO `wp_relevanssi` VALUES (55,'years','sraey',1,0,0,0,0,0,0,0,0,0,0,'','','','post',0);
INSERT INTO `wp_relevanssi` VALUES (72,'formatting','gnittamrof',0,1,0,0,0,0,0,0,0,0,0,'','','','post',0);
/*!40000 ALTER TABLE `wp_relevanssi` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `wp_relevanssi_log`
--

DROP TABLE IF EXISTS `wp_relevanssi_log`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
 SET character_set_client = utf8mb4 ;
CREATE TABLE `wp_relevanssi_log` (
  `id` bigint(9) NOT NULL AUTO_INCREMENT,
  `query` varchar(200) COLLATE utf8mb4_unicode_520_ci NOT NULL,
  `hits` mediumint(9) NOT NULL DEFAULT '0',
  `time` timestamp NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `user_id` bigint(20) NOT NULL DEFAULT '0',
  `ip` varchar(40) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  PRIMARY KEY (`id`),
  KEY `query` (`query`(190))
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_520_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `wp_relevanssi_log`
--

LOCK TABLES `wp_relevanssi_log` WRITE;
/*!40000 ALTER TABLE `wp_relevanssi_log` DISABLE KEYS */;
/*!40000 ALTER TABLE `wp_relevanssi_log` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `wp_relevanssi_stopwords`
--

DROP TABLE IF EXISTS `wp_relevanssi_stopwords`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
 SET character_set_client = utf8mb4 ;
CREATE TABLE `wp_relevanssi_stopwords` (
  `stopword` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NOT NULL,
  PRIMARY KEY (`stopword`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_520_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `wp_relevanssi_stopwords`
--

LOCK TABLES `wp_relevanssi_stopwords` WRITE;
/*!40000 ALTER TABLE `wp_relevanssi_stopwords` DISABLE KEYS */;
/*!40000 ALTER TABLE `wp_relevanssi_stopwords` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `wp_term_relationships`
--

DROP TABLE IF EXISTS `wp_term_relationships`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
 SET character_set_client = utf8mb4 ;
CREATE TABLE `wp_term_relationships` (
  `object_id` bigint(20) unsigned NOT NULL DEFAULT '0',
  `term_taxonomy_id` bigint(20) unsigned NOT NULL DEFAULT '0',
  `term_order` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`object_id`,`term_taxonomy_id`),
  KEY `term_taxonomy_id` (`term_taxonomy_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_520_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `wp_term_relationships`
--

LOCK TABLES `wp_term_relationships` WRITE;
/*!40000 ALTER TABLE `wp_term_relationships` DISABLE KEYS */;
INSERT INTO `wp_term_relationships` VALUES (1,1,0);
INSERT INTO `wp_term_relationships` VALUES (16,2,0);
INSERT INTO `wp_term_relationships` VALUES (17,2,0);
INSERT INTO `wp_term_relationships` VALUES (18,2,0);
INSERT INTO `wp_term_relationships` VALUES (20,4,0);
INSERT INTO `wp_term_relationships` VALUES (21,4,0);
INSERT INTO `wp_term_relationships` VALUES (22,4,0);
INSERT INTO `wp_term_relationships` VALUES (23,4,0);
INSERT INTO `wp_term_relationships` VALUES (24,5,0);
INSERT INTO `wp_term_relationships` VALUES (25,5,0);
INSERT INTO `wp_term_relationships` VALUES (26,5,0);
INSERT INTO `wp_term_relationships` VALUES (27,5,0);
INSERT INTO `wp_term_relationships` VALUES (28,5,0);
INSERT INTO `wp_term_relationships` VALUES (70,2,0);
/*!40000 ALTER TABLE `wp_term_relationships` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `wp_term_taxonomy`
--

DROP TABLE IF EXISTS `wp_term_taxonomy`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
 SET character_set_client = utf8mb4 ;
CREATE TABLE `wp_term_taxonomy` (
  `term_taxonomy_id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `term_id` bigint(20) unsigned NOT NULL DEFAULT '0',
  `taxonomy` varchar(32) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `description` longtext COLLATE utf8mb4_unicode_520_ci NOT NULL,
  `parent` bigint(20) unsigned NOT NULL DEFAULT '0',
  `count` bigint(20) NOT NULL DEFAULT '0',
  PRIMARY KEY (`term_taxonomy_id`),
  UNIQUE KEY `term_id_taxonomy` (`term_id`,`taxonomy`),
  KEY `taxonomy` (`taxonomy`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_520_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `wp_term_taxonomy`
--

LOCK TABLES `wp_term_taxonomy` WRITE;
/*!40000 ALTER TABLE `wp_term_taxonomy` DISABLE KEYS */;
INSERT INTO `wp_term_taxonomy` VALUES (1,1,'category','',0,1);
INSERT INTO `wp_term_taxonomy` VALUES (2,2,'nav_menu','',0,4);
INSERT INTO `wp_term_taxonomy` VALUES (3,3,'nav_menu','',0,0);
INSERT INTO `wp_term_taxonomy` VALUES (4,4,'nav_menu','',0,4);
INSERT INTO `wp_term_taxonomy` VALUES (5,5,'nav_menu','',0,5);
/*!40000 ALTER TABLE `wp_term_taxonomy` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `wp_termmeta`
--

DROP TABLE IF EXISTS `wp_termmeta`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
 SET character_set_client = utf8mb4 ;
CREATE TABLE `wp_termmeta` (
  `meta_id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `term_id` bigint(20) unsigned NOT NULL DEFAULT '0',
  `meta_key` varchar(255) COLLATE utf8mb4_unicode_520_ci DEFAULT NULL,
  `meta_value` longtext COLLATE utf8mb4_unicode_520_ci,
  PRIMARY KEY (`meta_id`),
  KEY `term_id` (`term_id`),
  KEY `meta_key` (`meta_key`(191))
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_520_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `wp_termmeta`
--

LOCK TABLES `wp_termmeta` WRITE;
/*!40000 ALTER TABLE `wp_termmeta` DISABLE KEYS */;
/*!40000 ALTER TABLE `wp_termmeta` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `wp_terms`
--

DROP TABLE IF EXISTS `wp_terms`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
 SET character_set_client = utf8mb4 ;
CREATE TABLE `wp_terms` (
  `term_id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(200) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `slug` varchar(200) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `term_group` bigint(10) NOT NULL DEFAULT '0',
  PRIMARY KEY (`term_id`),
  KEY `slug` (`slug`(191)),
  KEY `name` (`name`(191))
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_520_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `wp_terms`
--

LOCK TABLES `wp_terms` WRITE;
/*!40000 ALTER TABLE `wp_terms` DISABLE KEYS */;
INSERT INTO `wp_terms` VALUES (1,'Uncategorized','uncategorized',0);
INSERT INTO `wp_terms` VALUES (2,'Main Menu','main-menu',0);
INSERT INTO `wp_terms` VALUES (3,'User Menu','user-menu',0);
INSERT INTO `wp_terms` VALUES (4,'Footer Menu','footer-menu',0);
INSERT INTO `wp_terms` VALUES (5,'Footer Links','footer-links',0);
/*!40000 ALTER TABLE `wp_terms` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `wp_usermeta`
--

DROP TABLE IF EXISTS `wp_usermeta`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
 SET character_set_client = utf8mb4 ;
CREATE TABLE `wp_usermeta` (
  `umeta_id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `user_id` bigint(20) unsigned NOT NULL DEFAULT '0',
  `meta_key` varchar(255) COLLATE utf8mb4_unicode_520_ci DEFAULT NULL,
  `meta_value` longtext COLLATE utf8mb4_unicode_520_ci,
  PRIMARY KEY (`umeta_id`),
  KEY `user_id` (`user_id`),
  KEY `meta_key` (`meta_key`(191))
) ENGINE=InnoDB AUTO_INCREMENT=26 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_520_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `wp_usermeta`
--

LOCK TABLES `wp_usermeta` WRITE;
/*!40000 ALTER TABLE `wp_usermeta` DISABLE KEYS */;
INSERT INTO `wp_usermeta` VALUES (1,1,'nickname','kauland.buchanan');
INSERT INTO `wp_usermeta` VALUES (2,1,'first_name','');
INSERT INTO `wp_usermeta` VALUES (3,1,'last_name','');
INSERT INTO `wp_usermeta` VALUES (4,1,'description','');
INSERT INTO `wp_usermeta` VALUES (5,1,'rich_editing','true');
INSERT INTO `wp_usermeta` VALUES (6,1,'syntax_highlighting','true');
INSERT INTO `wp_usermeta` VALUES (7,1,'comment_shortcuts','false');
INSERT INTO `wp_usermeta` VALUES (8,1,'admin_color','fresh');
INSERT INTO `wp_usermeta` VALUES (9,1,'use_ssl','0');
INSERT INTO `wp_usermeta` VALUES (10,1,'show_admin_bar_front','true');
INSERT INTO `wp_usermeta` VALUES (11,1,'locale','');
INSERT INTO `wp_usermeta` VALUES (12,1,'wp_capabilities','a:1:{s:13:\"administrator\";b:1;}');
INSERT INTO `wp_usermeta` VALUES (13,1,'wp_user_level','10');
INSERT INTO `wp_usermeta` VALUES (14,1,'dismissed_wp_pointers','');
INSERT INTO `wp_usermeta` VALUES (15,1,'show_welcome_panel','1');
INSERT INTO `wp_usermeta` VALUES (16,1,'session_tokens','a:1:{s:64:\"54c626163fa706975ac8a40488be25f86ca08ba11f5a9e972aec802d7ab283ab\";a:4:{s:10:\"expiration\";i:1635434908;s:2:\"ip\";s:9:\"127.0.0.1\";s:2:\"ua\";s:120:\"Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/94.0.4606.81 Safari/537.36\";s:5:\"login\";i:1635262108;}}');
INSERT INTO `wp_usermeta` VALUES (17,1,'wp_dashboard_quick_press_last_post_id','75');
INSERT INTO `wp_usermeta` VALUES (18,1,'wp_user-settings','libraryContent=browse&editor=html');
INSERT INTO `wp_usermeta` VALUES (19,1,'wp_user-settings-time','1619015610');
INSERT INTO `wp_usermeta` VALUES (20,1,'managenav-menuscolumnshidden','a:3:{i:0;s:15:\"title-attribute\";i:1;s:3:\"xfn\";i:2;s:11:\"description\";}');
INSERT INTO `wp_usermeta` VALUES (21,1,'metaboxhidden_nav-menus','a:5:{i:0;s:27:\"add-post-type-sl_staff_cpts\";i:1;s:33:\"add-post-type-sl_testimonial_cpts\";i:2;s:26:\"add-post-type-sl_faqs_cpts\";i:3;s:12:\"add-post_tag\";i:4;s:18:\"add-faq_categories\";}');
INSERT INTO `wp_usermeta` VALUES (22,1,'meta-box-order_page','a:4:{s:15:\"acf_after_title\";s:0:\"\";s:4:\"side\";s:57:\"submitdiv,pageparentdiv,postimagediv,acf-group_map_header\";s:6:\"normal\";s:30:\"revisionsdiv,slugdiv,authordiv\";s:8:\"advanced\";s:0:\"\";}');
INSERT INTO `wp_usermeta` VALUES (23,1,'screen_layout_page','2');
INSERT INTO `wp_usermeta` VALUES (24,1,'nav_menu_recently_edited','2');
INSERT INTO `wp_usermeta` VALUES (25,1,'gform_recent_forms','a:1:{i:0;s:1:\"1\";}');
/*!40000 ALTER TABLE `wp_usermeta` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `wp_users`
--

DROP TABLE IF EXISTS `wp_users`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
 SET character_set_client = utf8mb4 ;
CREATE TABLE `wp_users` (
  `ID` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `user_login` varchar(60) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `user_pass` varchar(255) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `user_nicename` varchar(50) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `user_email` varchar(100) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `user_url` varchar(100) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `user_registered` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `user_activation_key` varchar(255) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `user_status` int(11) NOT NULL DEFAULT '0',
  `display_name` varchar(250) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  PRIMARY KEY (`ID`),
  KEY `user_login_key` (`user_login`),
  KEY `user_nicename` (`user_nicename`),
  KEY `user_email` (`user_email`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_520_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `wp_users`
--

LOCK TABLES `wp_users` WRITE;
/*!40000 ALTER TABLE `wp_users` DISABLE KEYS */;
INSERT INTO `wp_users` VALUES (1,'kauland.buchanan','$P$BWjNdpkFfIxF9edJ8grAjKtg54Yfn6.','kauland-buchanan','kauland.buchanan@uhsinc.com','http://valleyhealth.local','2021-04-19 17:39:35','',0,'kauland.buchanan');
/*!40000 ALTER TABLE `wp_users` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2021-12-13 14:04:37
