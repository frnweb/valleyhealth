<?php

namespace App;

use StoutLogic\AcfBuilder\FieldsBuilder;

$config = (object) [
	'ui' => 1,
	'wrapper' => ['width' => 100],
];

$news = new FieldsBuilder('news');

$news
	->addTab('settings', ['placement' => 'left'])
		->addFields(get_field_partial('partials.add_class'))
		->addFields(get_field_partial('partials.module_title'))
        ->addFields(get_field_partial('partials.grid_options'));

$news
    ->addTab('content', ['placement' => 'left'])

    // Header
    ->addText('header', [
        'label' => 'Header',
        'ui' => $config->ui
    ])
    ->setInstructions('This is the Header for the News Module')

    ->addWysiwyg('description', [
        'label' => 'Description',
    ])
        ->setInstructions('Text that appears below header')
    
    // Post Relationship Field
	->addRelationship('article', [
	    'label' => 'News Picker',
        'post_type' => 'sl_news_cpts',
	    'ui' => $config->ui,
    ])

    //Button
    ->addFields(get_field_partial('modules.button'));
    
return $news;