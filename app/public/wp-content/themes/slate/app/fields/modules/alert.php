<?php

namespace App;

use StoutLogic\AcfBuilder\FieldsBuilder;

$config = (object) [
	'ui' => 1,
	'wrapper' => ['width' => 100],
];


$alert = new FieldsBuilder('alert');

$alert
	->addTab('settings', ['placement' => 'left'])
		->addFields(get_field_partial('partials.add_class'))
		->addFields(get_field_partial('partials.module_title'));

$alert
	->addTab('content', ['placement' => 'left'])

	  	// Text
		->setInstructions('Optional wysiwyg for the card')
		->addText('text', [
			'wrapper' => ['width' => 70]
		])

		//Button
		->addLink('button', [
			'wrapper' => ['width' => 30]
		]);
	

return $alert;

