jQuery(function($) {
  jQuery(document).ready(function() {

    $(".hamburger").on("click", function(e) {
      $(".hamburger").toggleClass("is-active");
    });

    $('.js-off-canvas-overlay').on("click", function(e) {
      $(".hamburger").toggleClass("is-active")
    });

  });
});