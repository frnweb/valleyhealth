<?php
/**
 * Plugin Name: News CPTS Plugin
 * Description: This is the Custom Post Type for News.
 * Author: M.M.
 * License: GPL2
*/

// Register Custom Post Type
function sl_news_cpts() {

	$labels = array(
		'name'                  => _x( 'News', 'Post Type General Name', 'sl_news_cpts' ),
		'singular_name'         => _x( 'News', 'Post Type Singular Name', 'sl_news_cpts' ),
		'menu_name'             => __( 'News', 'sl_news_cpts' ),
		'name_admin_bar'        => __( 'News', 'sl_news_cpts' ),
		'archives'              => __( 'News Archives', 'sl_news_cpts' ),
		'attributes'            => __( 'News Attributes', 'sl_news_cpts' ),
		'parent_item_colon'     => __( 'Parent News:', 'sl_news_cpts' ),
		'all_items'             => __( 'All News', 'sl_news_cpts' ),
		'add_new_item'          => __( 'Add New News', 'sl_news_cpts' ),
		'add_new'               => __( 'Add New', 'sl_news_cpts' ),
		'new_item'              => __( 'New News', 'sl_news_cpts' ),
		'edit_item'             => __( 'Edit News', 'sl_news_cpts' ),
		'update_item'           => __( 'Update News', 'sl_news_cpts' ),
		'view_item'             => __( 'View News', 'sl_news_cpts' ),
		'view_items'            => __( 'View News', 'sl_news_cpts' ),
		'search_items'          => __( 'Search News', 'sl_news_cpts' ),
		'not_found'             => __( 'Not found', 'sl_news_cpts' ),
		'not_found_in_trash'    => __( 'Not found in Trash', 'sl_news_cpts' ),
		'featured_image'        => __( 'Featured Image', 'sl_news_cpts' ),
		'set_featured_image'    => __( 'Set featured image', 'sl_news_cpts' ),
		'remove_featured_image' => __( 'Remove featured image', 'sl_news_cpts' ),
		'use_featured_image'    => __( 'Use as featured image', 'sl_news_cpts' ),
		'insert_into_item'      => __( 'Insert into News', 'sl_news_cpts' ),
		'uploaded_to_this_item' => __( 'Uploaded to this News', 'sl_news_cpts' ),
		'items_list'            => __( 'News list', 'sl_news_cpts' ),
		'items_list_navigation' => __( 'News list navigation', 'sl_news_cpts' ),
		'filter_items_list'     => __( 'Filter News list', 'sl_news_cpts' ),
	);
	$args = array(
		'label'                 => __( 'News', 'sl_news_cpts' ),
		'description'           => __( 'Custom Post Type for News', 'sl_news_cpts' ),
		'labels'                => $labels,
		'supports'              => array( 'title', 'editor', 'thumbnail', 'revisions', 'custom-fields' ),
		'hierarchical'          => true,
		'public'                => true,
		'show_ui'               => true,
		'show_in_menu'          => true,
		'rewrite' => array('slug' => 'news','with_front' => false),
		'menu_position'         => 5,
		'menu_icon'             => 'dashicons-format-aside',
		'show_in_admin_bar'     => true,
		'show_in_nav_menus'     => true,
		'can_export'            => true,
		'has_archive'           => true,
		'exclude_from_search'   => false,
		'publicly_queryable'    => true,
		'capability_type'       => 'page',
	);
	register_post_type( 'sl_news_cpts', $args );

}
add_action( 'init', 'sl_news_cpts', 0 );
?>