<?php

namespace App;

use StoutLogic\AcfBuilder\FieldsBuilder;

$config = (object) [
	'ui' => 1,
	'wrapper' => ['width' => 100],
];

$optionsglobal = new FieldsBuilder('global_options');

$optionsglobal
    ->setLocation('options_page', '==', 'theme-general-settings')

    //Address Fields
    ->addRepeater('address', [
      'min' => 1,
      'max' => 10,
      'button_label' => 'Add Address',
      'layout' => 'block',
      'wrapper' => [
          'width' => '50',
          'class' => 'deck',
        ],
    ])
        ->addGroup('address', [
    ])
            ->addText('street_address', [
                'label' => 'Street Address',
                'ui' => $config->ui
            ])
            ->setInstructions('The street address for the facility. This will be used throughout the Site.')

            ->addText('city', [
                'label' => 'City',
                'ui' => $config->ui
            ])
            ->setInstructions('Put the City i.e. Nashville')

            ->addText('state', [
                'label' => 'State',
                'ui' => $config->ui
            ])
            ->setInstructions('Put the State here i.e. TN')

            ->addText('zip_code', [
                'label' => 'Zip Code',
                'ui' => $config->ui
            ])
            ->setInstructions('The Zip Code of the facility')
            
            //Google Map
            ->addGoogleMap('google_map')
        ->endGroup()
    ->endRepeater()

    //Phone Numbers
    ->addGroup('phone', [
        'wrapper' => ['width' => 50]
    ])
        ->addText('main', [
            'label' => 'Global Phone Number',
            'ui' => $config->ui
        ])
        ->addText('sms', [
            'label' => 'SMS number',
            'ui' => $config->ui
        ])
    ->setInstructions('Add the number for the SMS messenging.')
    ->endGroup()

    //Sidebar Fields
    ->addRepeater('sidebar', [
      'min' => 1,
      'max' => 10,
      'label' => 'Global Sidebar',
      'instructions' => 'Add items that will appear on the sidebar on every default page template',
      'button_label' => 'Add Item',
      'layout' => 'block',
      'wrapper' => [
          'width' => '50',
          'class' => 'deck',
        ],
    ])
        ->addGroup('item', [
    ])
            ->addText('title', [
                'label' => 'Section Title',
                'instructions' => 'Optional',
            ])

            // WYSIWYG
            ->addWysiwyg('content', [
                'label' => 'WYSIWYG',
            ])
        ->endGroup()
    ->endRepeater();

return $optionsglobal;