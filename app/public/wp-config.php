<?php
/**
 * The base configuration for WordPress
 *
 * The wp-config.php creation script uses this file during the
 * installation. You don't have to use the web site, you can
 * copy this file to "wp-config.php" and fill in the values.
 *
 * This file contains the following configurations:
 *
 * * MySQL settings
 * * Secret keys
 * * Database table prefix
 * * ABSPATH
 *
 * @link https://codex.wordpress.org/Editing_wp-config.php
 *
 * @package WordPress
 */

// ** MySQL settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define( 'DB_NAME', 'local' );

/** MySQL database username */
define( 'DB_USER', 'root' );

/** MySQL database password */
define( 'DB_PASSWORD', 'root' );

/** MySQL hostname */
define( 'DB_HOST', 'localhost' );

/** Database Charset to use in creating database tables. */
define( 'DB_CHARSET', 'utf8' );

/** The Database Collate type. Don't change this if in doubt. */
define( 'DB_COLLATE', '' );

/**
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define('AUTH_KEY',         't4AXuY6fS7103E2b7BNoFEOZyuzo9HnmXgUsnDvn/jeV2wlkwkjdCIB7n/BibWuU4rXaOoRw/imARFXSVkOyOA==');
define('SECURE_AUTH_KEY',  'xKkthMt8xtBII1m3e2+eJpeHCyAqwwYGZ7m9UC5qrd6ROYGthFG2CrRAGjEZrfrtFZDrWw4JqrEMmt03lkAKOA==');
define('LOGGED_IN_KEY',    'b1gskX9mqXfEgJwtKAdATLK8sb8wSgyKQs3SoMCm0w5WZUHVbEyJXRavmQmxEDd7ZidXmClime6WE9pw6R5m1g==');
define('NONCE_KEY',        'g/fr0oej82eA0Q8J1F4YBGf1RX7w+MEq9hrHhMTAMO1ieYDmdXp9UIjiGNqB02dn7WPcRAXPBto1um/pVOMb4w==');
define('AUTH_SALT',        'GqwKqU5v2TiCzgqob68npBzDSakWwu0TL1FcVVw7oCjKD+QaWW9cz+w2M7BrrQqXT6OM3FCpTyF2GLAoEjvM5Q==');
define('SECURE_AUTH_SALT', 'wcCq+BxPY7PdlAyOjUcSp95OqKqvmFFm5jTcurYniAFA5D/FnYeo8tLxlTf3yxWEnAFvoA80nIbVHjbKWld1mA==');
define('LOGGED_IN_SALT',   'HUbLD9OyuQPOEpr90iLSfq/dir+7V5/ibAzIlUyDuIUV//kZ2uubkenjF97tajakzUQifR0EjjGO9p40oaYMIw==');
define('NONCE_SALT',       '7f/a1+dpgshMcDT35+sU8naHp+en7F4QejqA4ABltnQkzjASndEpGf5JnSB8TAaJubCdr7+pPxOI13ytgA4R9w==');

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each
 * a unique prefix. Only numbers, letters, and underscores please!
 */
$table_prefix = 'wp_';


// This enables debugging.
define( 'WP_DEBUG', true );



/* That's all, stop editing! Happy publishing. */

/** Absolute path to the WordPress directory. */
if ( ! defined( 'ABSPATH' ) ) {
	define( 'ABSPATH', dirname( __FILE__ ) . '/' );
}

/** Sets up WordPress vars and included files. */
require_once ABSPATH . 'wp-settings.php';
